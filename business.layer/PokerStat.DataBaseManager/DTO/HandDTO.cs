﻿using System;
using System.Collections.Generic;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class HandDTO:IHand
    {

        public HandDTO(IHand hand)
        {
            Id = Guid.NewGuid();
            Identifier = hand.Identifier;
            Date = hand.Date;
            StageHands = hand.StageHands;
        }

        public string Identifier { get; set; }

        public DateTime Date { get; set; }

        public ITable Table { get; set; }

        public IEnumerable<IHandStage> StageHands { get; set; }

        public Guid Id { get; set; }
        
    }
}
