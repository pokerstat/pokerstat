﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class PlayerHandDTO:IPlayerHand
    {
        public PlayerHandDTO(IPlayerHand playerHand)
        {
            CountChips = playerHand.CountChips;
            Position = playerHand.Position;
            Id = Guid.NewGuid();
            HandCard = playerHand.HandCard;
        }
        public PlayerHandDTO()
        {
            HandCard=new Collection<ICard>();
        }
        public Position Position { get; set; }

        public decimal CountChips { get; set; }

        public IPlayer Player { get; set; }

        public ICollection<ICard> HandCard { get; } 

        public Guid Id { get; set; }
    }
}
