﻿using System;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class TableDTO:ITable
    {
        public string Identifier { get; set; }

        public ITournament Tournament { get; set; }
        public TypeOfCurrency TypeOfCurrency { get; set; }
        public TypeOfGame TypeOfGame { get; set; }
        public IPokerRoom PokerRoom { get; set; }

        public Guid Id { get; set; }
    }
}
