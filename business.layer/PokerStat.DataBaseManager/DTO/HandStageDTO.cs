﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class HandStageDTO : IHandStage
    {
        public HandStageDTO(IHandStage handStage)
        {
            Id = Guid.NewGuid();
            Stage = handStage.Stage;
            Actions = handStage.Actions;
        }
        public HandStageDTO()
        {

        }
        public Stages Stage { get; set; }

        public ICollection<ICard> Cards { get; } = new Collection<ICard>();

        public IEnumerable<IStageAction> Actions { get; set; }

        public Guid Id { get; set;}
    }
}
