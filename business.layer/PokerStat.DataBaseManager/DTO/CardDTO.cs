﻿
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class CardDTO:ICard
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public char ShortName { get; set; }

        public char SuitCode { get; set; }

        public string SuitName { get; set;}
        
        public Color Color { get; set; }
        
        public Color AlternativeColor { get; set; }
    }
}
