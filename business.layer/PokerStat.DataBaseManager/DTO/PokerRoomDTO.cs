﻿
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class PokerRoomDTO:IPokerRoom
    {
        public short Id { get; set; }

        public string Name { get; set; }
    }
}
