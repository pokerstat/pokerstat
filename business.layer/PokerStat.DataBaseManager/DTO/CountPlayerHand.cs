using System;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class CountPlayerHand
    {
        public Guid PlayerId { get; set; }
        public string Nick { get; set; }
        public long CountCard { get; set; }
    }
}