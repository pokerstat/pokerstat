﻿using System;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class StageActionDTO:IStageAction
    {
        public StageActionDTO(IStageAction stageAction)
        {
            Action = stageAction.Action;
            Count = stageAction.Count;
            Id=stageAction.Id;
            Order = stageAction.Order;
        }

        public StageActionDTO()
        {
            
        }

        public IPlayerHand PlayerHand { get; set; }

        public Model.Action Action { get; set; }
        public byte Order { get; set; }

        public decimal? Count { get; set; }

        public Guid Id { get; set; }
    }
}
