﻿using System;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.DTO
{
    internal sealed class TournamentDTO:ITournament
    {
        public string Identifier { get; set; }
        public decimal Buy_in { get; set; }

        public Guid Id { get; set; }
    }
}
