﻿
using System.Text;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Managers
{
    internal static class ProviderHelper
    {
        public static string CreateHashCode(this IHand hand)
        {
            using (System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(hand.Identifier + hand.Table.PokerRoom.Name +
                                                                        hand.Table.Identifier + (byte) hand.Table.TypeOfGame
                                                                        + hand.Table.Tournament?.Identifier);
                var data = sha1.ComputeHash(inputBytes);
                
                var sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }
    }
}