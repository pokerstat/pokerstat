﻿using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using PokerStat.DataBaseManager.DTO;
using PokerStat.Model;
using System;
using System.Collections.ObjectModel;
using PokerStat.DataBaseHelpers;
using PokerStat.DataBaseManager.Interface;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class PlayerHandProvider<T> where T : class, IDbConnection, new()
    {
        private readonly CardProvider<T> _cardProvider;
        private readonly IPlayerProvider _playerProvider;

        public PlayerHandProvider(CardProvider<T> cardProvider,
            IPlayerProvider playerProvider)
        {
            _cardProvider = cardProvider;
            _playerProvider = playerProvider;
        }

        internal async Task<IDictionary<string, IPlayerHand>> SaveAsync(IHand hand, IDbTransaction transaction)
        {
            IDictionary<string, IPlayerHand> _phs = new Dictionary<string, IPlayerHand>(StringComparer.InvariantCulture);

            foreach (var playerHand in hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand))
            {
                if (_phs.ContainsKey(playerHand.Player.Nick))
                    continue;
                var _player = new PlayerHandDTO(playerHand);
                _player.Player = _playerProvider.Save(playerHand.Player);
                await transaction.ExecuteNonQueryAsync("save_player_hand", CreateParameters(_player, hand));
                await _cardProvider.SaveModelCardAsync(playerHand.HandCard, _player, "save_hand_card", transaction);
                _phs.Add(_player.Player.Nick, _player);
            }

            return _phs;
        }

        private PokerStatDataBaseParameter[] CreateParameters(IPlayerHand player, IModel hand)
        {
            var _ishaveCard = player.HandCard != null && player.HandCard.Any();
            var dp = new PokerStatDataBaseParameter[5];
            dp[0]=PokerStatDataBaseParameter.Create("_"+nameof(player.Id), player.Id);
            dp[1]=PokerStatDataBaseParameter.Create("_"+nameof(player.CountChips), player.CountChips);
            dp[2]=PokerStatDataBaseParameter.Create("_"+nameof(player.Position), (byte)player.Position);
            dp[3]=PokerStatDataBaseParameter.Create("_Player_id", player.Player.Id);
            dp[4] = PokerStatDataBaseParameter.Create("_Hand_id", hand.Id);
            return dp;
        }

        internal async Task CheckAndWritePlayerHandAsync(IHand hand, IDbConnection dbConnection)
        {
            var phandstsk = Task.Run(()=>hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand)
                .Where(x => x.HandCard != null && x.HandCard.Count > 0)); //только карты с руками
            ICollection<CountPlayerHand> phs = new Collection<CountPlayerHand>();
            var hashcode = hand.CreateHashCode();
            using (var reader =
                await dbConnection.ExecuteReaderAsync("get_count_of_hand_card",
                    new[] {PokerStatDataBaseParameter.Create("code", hashcode)})) //долгий запрос
            {
                while (reader.Read())
                {
                    var cph = new CountPlayerHand
                    {
                        PlayerId = reader.GetGuid(1),
                        Nick = reader.GetString(0),
                        CountCard = reader.GetInt32(2)
                    };
                    phs.Add(cph);
                }

                reader.Close();
            }

            foreach (var phands in phandstsk.Result)
            {
                var p = phs.FirstOrDefault(x => String.Equals(x.Nick, phands.Player.Nick, StringComparison.CurrentCultureIgnoreCase));
                if (p==null || p.CountCard>=phands.HandCard.Count)
                    continue;
                //если в БД нет записей по данной руке
                {
                    var _player = new PlayerHandDTO();
                    var dp = new PokerStatDataBaseParameter[2];
                    dp[0]=PokerStatDataBaseParameter.Create("code",hashcode);
                    dp[1]=PokerStatDataBaseParameter.Create("Player_id", p.PlayerId);
                    _player.Id = await dbConnection.ExecuteScalarAsync<Guid>("get_player_hand_id", dp);
                    using (var tran = dbConnection.BeginTransaction())
                    {
                        await _cardProvider.DeleteHandCard(_player, tran);
                        await _cardProvider.SaveModelCardAsync(phands.HandCard, _player, "save_hand_card", tran);
                        tran.Commit();
                    }
                }
            }
        }
    }
}