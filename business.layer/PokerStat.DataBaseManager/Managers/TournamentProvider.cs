﻿using System;
using System.Collections.Generic;
using System.Data;
using PokerStat.DataBaseHelpers;
using PokerStat.DataBaseManager.DTO;
using PokerStat.Model;
using PokerStat.Model.Comparers;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class TournamentProvider
    {
        private readonly HashSet<ITournament> _tournaments;
        private static readonly object _obj = new object();

        internal TournamentProvider()
        {
            _tournaments = new HashSet<ITournament>(new TournamentComparer());
        }

        public ITournament Save(ITournament tournament,IDbTransaction dbTransaction)
        {
            if (_tournaments.TryGetValue(tournament, out ITournament _tournament))
                return _tournament;
            var dp = new PokerStatDataBaseParameter[3];
            dp[0]=PokerStatDataBaseParameter.Create("_"+nameof(tournament.Id),Guid.NewGuid());
            dp[1]=PokerStatDataBaseParameter.Create("_"+nameof(tournament.Identifier), tournament.Identifier);
            dp[2]=PokerStatDataBaseParameter.Create("_"+nameof(tournament.Buy_in), tournament.Buy_in);


            var dto= new TournamentDTO
            {
                Id =   dbTransaction.ExecuteScalar<Guid>("save_tournament", dp),
                Identifier = tournament.Identifier,
                Buy_in = tournament.Buy_in
            };
            lock (_obj)
            {
                 _tournaments.Add(tournament);
            }
            return dto;
        }
    }
}
