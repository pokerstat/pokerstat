﻿using System;
using System.Data;
using PokerStat.Model;
using PokerStat.DataBaseManager.Interface;
using System.Threading.Tasks;
using PokerStat.DataBaseManager.DTO;
using System.Collections.Generic;
using System.Linq;
using PokerStat.DataBaseHelpers;


namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class HandProvider<T> : IHandProvider where T : class, IDbConnection, new()
    {
        private readonly CardProvider<T> _cardProvider;
        private readonly IPlayerProvider _playerProvider;
        private readonly string _dbConnection;
        private static readonly object _obj = new object();
        private readonly TableProvider _tableprovider;

        internal HandProvider(string connection, TableProvider tableProvider, CardProvider<T> cardProvider, IPlayerProvider playerProvider)
        {
            _dbConnection = connection;
            _cardProvider = cardProvider;
            _playerProvider = playerProvider;
            _tableprovider = tableProvider;
        }

        public async Task SaveAsync(IHand hand)
        {
            var _playerprovider = new PlayerHandProvider<T>( _cardProvider, _playerProvider);
            using (var con = new T {ConnectionString = _dbConnection})
            {
                con.Open();

                if (await CheckExistHandAsync(hand, con, _playerprovider))
                {
                    return;
                }
                using (var tran = con.BeginTransaction())
                {
                    HandDTO _hand;
                    try
                    {
                        
                         _hand = new HandDTO(hand)
                        {
                            Table = _tableprovider.Save(hand.Table, tran)
                        };
                        await tran.ExecuteNonQueryAsync("save_hand", Createparameters(_hand));
                        var _ph = await _playerprovider.SaveAsync(_hand, tran);
                        await GetFullStageHandAsync(_ph, _hand, tran);
                        lock (_obj) // для согласованности данных
                        {
                            using (var con2 = new T {ConnectionString = _dbConnection}) // из-за отсутвия поддержки параллельных транзакций, пришлось создать новый конекшн
                            {
                                con2.Open();
                                if (CheckExistHandAsync(hand, con2, _playerprovider).Result)
                                {
                                    tran.Rollback();
                                }
                            } 
                            tran.Commit();
                        }

                       
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }
        }

        #region private

        private async Task<bool> CheckExistHandAsync(IHand hand, IDbConnection connection,
            PlayerHandProvider<T> _playerprovider)
        {
            var hashcode = hand.CreateHashCode();
            var countstage = 0;
            if (hand.StageHands != null && hand.StageHands.Any())
                countstage = hand.StageHands.Count();
            short _countst = await CheckHandAsync(hashcode, connection);
            if (_countst >= 2 && _countst > countstage)
            {
                return true;
            }

            if (_countst == countstage)
            {
                await _playerprovider.CheckAndWritePlayerHandAsync(hand, connection);
                return true;
            }

            if (_countst < countstage)
            {
                await SetHandToRemovedAsync(hashcode, connection);
            }

            return false;
        }
        
        private async Task<short> CheckHandAsync(string hashcode, IDbConnection dbConnection)
        {
            return await dbConnection.ExecuteScalarAsync<short>("сheck_exists_hand",
                new[] {PokerStatDataBaseParameter.Create("code", hashcode)});
        }

        private async Task SetHandToRemovedAsync(string hashcode, IDbConnection con)
        {
            await con.ExecuteNonQueryAsync("set_hand_to_removed",
                new[] {PokerStatDataBaseParameter.Create("code", hashcode)});
        }

        private async Task GetFullStageHandAsync(IDictionary<string, IPlayerHand> playerHands, IHand hand,
            IDbTransaction tran)
        {
            var stageHandProvider = new StageHandProvider<T>(_cardProvider);
            foreach (var hs in hand.StageHands)
            {
                await stageHandProvider.SaveAsync(hs, playerHands, hand, tran);
            }
        }

        private PokerStatDataBaseParameter[] Createparameters(IHand hand)
        {
            var dp = new PokerStatDataBaseParameter[5];
            dp[0] = PokerStatDataBaseParameter.Create("_"+nameof(hand.Id), hand.Id);
            dp[1] = PokerStatDataBaseParameter.Create("_"+nameof(hand.Identifier), hand.Identifier);
            dp[2] = PokerStatDataBaseParameter.Create("_"+nameof(hand.Date), hand.Date);
            dp[3] = PokerStatDataBaseParameter.Create("_hashcode", hand.CreateHashCode());
            dp[4] = PokerStatDataBaseParameter.Create("_table_id", hand.Table.Id);
            return dp;
        }

        #endregion
    }
}