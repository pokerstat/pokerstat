﻿using System;
using System.Collections.Generic;
using System.Data;
using PokerStat.DataBaseHelpers;
using PokerStat.Model;
using PokerStat.DataBaseManager.DTO;
using PokerStat.DataBaseManager.Interface;
using PokerStat.Model.Comparers;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class TableProvider
    {
        private readonly HashSet<ITable> _tables;
        private readonly IPokerRoomProvider _pokerRoomProvider;
        private static readonly object _obj = new object();

        internal TableProvider(IPokerRoomProvider pokerRoomProvider)
        {
            _tables = new HashSet<ITable>(new TableComparer());
            _pokerRoomProvider = pokerRoomProvider;
        }

        internal ITable Save(ITable table, IDbTransaction dbTransaction)
        {
            if (_tables.TryGetValue(table, out ITable _table))
                return _table;

            var dto = new TableDTO
            {
                PokerRoom = _pokerRoomProvider.Save(table.PokerRoom),
                Identifier = table.Identifier,
                TypeOfCurrency = table.TypeOfCurrency,
                TypeOfGame = table.TypeOfGame,
            };
            var dp = new PokerStatDataBaseParameter[table.Tournament != null ? 6 : 5];
            dp[0] = PokerStatDataBaseParameter.Create("_" + nameof(table.Id), Guid.NewGuid());
            dp[1] = PokerStatDataBaseParameter.Create("_" + nameof(table.Identifier), table.Identifier);
            dp[2] = PokerStatDataBaseParameter.Create("_" + nameof(table.TypeOfCurrency), (byte) table.TypeOfCurrency);
            dp[3] = PokerStatDataBaseParameter.Create("_type_of_game", (byte) table.TypeOfGame);
            dp[4] = PokerStatDataBaseParameter.Create("_poker_room_id", table.PokerRoom.Id);


            ITournament tournament = null;
            if (table.Tournament != null)
            {
                var tp = new TournamentProvider();
                tournament = tp.Save(table.Tournament, dbTransaction);
                dp[5] = PokerStatDataBaseParameter.Create("_tournament_id", tournament.Id);
            }

            dto.Tournament = tournament; 
            dto.Id =  dbTransaction.ExecuteScalar<Guid>("save_table", dp);
            lock (_obj)
            {
                _tables.Add(dto);
            }

            return dto;
        }
    }
}