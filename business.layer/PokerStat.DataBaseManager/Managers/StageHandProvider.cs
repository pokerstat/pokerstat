﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using PokerStat.DataBaseHelpers;
using PokerStat.DataBaseManager.DTO;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class StageHandProvider<T> where T : class, IDbConnection, new()
    {
        private readonly CardProvider<T> _cardProvider;

        public StageHandProvider(CardProvider<T> cardProvider)
        {
            _cardProvider = cardProvider;
        }

        public async Task SaveAsync(IHandStage stage, IDictionary<string, IPlayerHand> playerHands, IModel hand,
            IDbTransaction transaction)
        {
            var hs = new HandStageDTO(stage);

            await transaction.ExecuteNonQueryAsync("save_stage_hand", CreateParameters(hs, hand));

            if (hs.Stage == Stages.Flop || hs.Stage == Stages.Turn || hs.Stage == Stages.River)
            {
                await _cardProvider.SaveModelCardAsync(stage.Cards, hs, "save_stage_card", transaction);
            }

            var actprovider = new ActionProvider();

            foreach (var act in stage.Actions)
            {
                var actdto = new StageActionDTO(act);
                actdto.PlayerHand = playerHands[act.PlayerHand.Player.Nick];
                await actprovider.SaveAsync(actdto, hs, transaction);
            }
        }

        private PokerStatDataBaseParameter[] CreateParameters(IHandStage handStage, IModel hand)
        {
            var dp = new PokerStatDataBaseParameter[3];
            dp[0]=PokerStatDataBaseParameter.Create("_"+nameof(handStage.Id), handStage.Id);
            dp[1]=PokerStatDataBaseParameter.Create("_"+nameof(handStage.Stage), (byte) handStage.Stage);
            dp[2]=PokerStatDataBaseParameter.Create("_Hand_id", hand.Id);
            return dp;
        }
    }
}