﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using PokerStat.DataBaseHelpers;
using PokerStat.DataBaseManager.DTO;
using PokerStat.DataBaseManager.Interface;
using PokerStat.Model;
using PokerStat.Model.Comparers;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class PlayerProvider<T> : IPlayerProvider where T : class, IDbConnection, new()
    {
        private readonly string _dbConnection;
        private readonly IPokerRoomProvider _pokerRoomProvider;
        private static readonly object _obj = new object();
        private readonly HashSet<IPlayer> _players;

        public PlayerProvider(string connection, IPokerRoomProvider pokerRoomProvider)
        {
            _dbConnection = connection;
            _pokerRoomProvider = pokerRoomProvider;
            _players = new HashSet<IPlayer>(new PlayerComparer());
        }

        public IPlayer Save(IPlayer player)
        {
            if (_players.TryGetValue(player, out IPlayer _player))
                return _player;
            using (var con = new T {ConnectionString = _dbConnection})
            {
                con.Open();
                var _playerDto = new PlayerDTO(player);

                _playerDto.PokerRoom = _pokerRoomProvider.Save(player.PokerRoom);
                _playerDto.Id =  con.ExecuteScalar<Guid>("save_player", CreateParameters(_playerDto));
                lock (_obj)
                {
                    _players.Add(_playerDto);
                }
                return _playerDto;
            }
        }

        public async Task<IEnumerable<IPlayer>> GetPlayers(string nick)
        {
            var dp = new PokerStatDataBaseParameter[1];
            dp[0] = PokerStatDataBaseParameter.Create("_" + nameof(nick), nick);

            ICollection<IPlayer> _players = new Collection<IPlayer>();

            using (var con = new T {ConnectionString = _dbConnection})
            {
                con.Open();
                using (var reader =
                    await con.ExecuteReaderAsync("get_players", dp))
                {
                    while (reader.Read())
                    {
                        var dtop = new PlayerDTO
                        {
                            Id = reader.GetGuid(0), Nick = reader.GetString(1), PokerRoom = _pokerRoomProvider.PokerRooms.Single(x => x.Id == reader.GetByte(2))
                        };
                        _players.Add(dtop);
                    }

                    reader.Close();
                }
            }

            return _players;
        }
        private PokerStatDataBaseParameter[] CreateParameters(IPlayer player)
        {
            var dp = new PokerStatDataBaseParameter[3];
            dp[0] = PokerStatDataBaseParameter.Create("_" + nameof(player.Id), player.Id);
            dp[1] = PokerStatDataBaseParameter.Create("_" + nameof(player.Nick), player.Nick);
            dp[2] = PokerStatDataBaseParameter.Create("_PokerRoom_id", player.PokerRoom.Id);
            return dp;
        }
    }
}