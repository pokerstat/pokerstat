using System;
using System.Data;
using PokerStat.DataBaseManager.DTO;
using PokerStat.DataBaseManager.Interface;
using PokerStat.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PokerStat.DataBaseHelpers;
using PokerStat.Model.Comparers;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class CardProvider<T> : ICardProvider where T : class, IDbConnection, new()
    {
        private readonly Lazy<IEnumerable<ICard>> _lazycard;
        private readonly string _dbConnection;

        public CardProvider(string connection)
        {
            _dbConnection = connection;
            _lazycard = new Lazy<IEnumerable<ICard>>(GetAll, true);
        }

        public IEnumerable<ICard> Cards => _lazycard.Value;

        private IEnumerable<ICard> GetAll()
        {
            var _cards =new HashSet<ICard>(new CardComparer());
            using (var con = new T {ConnectionString = _dbConnection})
            {
                con.Open();
                using (var reader = con.ExecuteReader("get_card"))
                {
                    while (reader.Read())
                    {
                        var dtocard = new CardDTO
                        {
                            Id = reader.GetByte(0),
                            Name = reader.GetString(1),
                            Color = (Color) reader.GetInt32(5),
                            AlternativeColor = (Color) reader.GetInt32(6)
                        };
                        if (!reader.IsDBNull(2))
                        {
                            dtocard.ShortName = reader.GetString(2)[0];
                        }

                        if (!reader.IsDBNull(3))
                        {
                            dtocard.SuitCode = reader.GetString(3)[0];
                        }

                        if (!reader.IsDBNull(4))
                        {
                            dtocard.SuitName = reader.GetString(4);
                        }

                        _cards.Add(dtocard);
                    }
                    reader.Close();
                }
            }
            return _cards;
        }

        internal async Task SaveModelCardAsync(IEnumerable<IShortCard> cards, IModel model, string nameprocedure, IDbTransaction transaction)
        {
            if (cards == null)
                return;
            foreach (var c in cards)
            {
                var dp = new PokerStatDataBaseParameter[3];
                dp[0]=PokerStatDataBaseParameter.Create("_id",Guid.NewGuid());
                dp[1]=PokerStatDataBaseParameter.Create("_model_id", model.Id);
                ICard card = Cards.First(x => x.ShortName == c.ShortName && x.SuitCode == c.SuitCode);
                dp[2]=PokerStatDataBaseParameter.Create("_card_id", card.Id);
                await transaction.ExecuteNonQueryAsync(nameprocedure, dp);
            }
        }

        internal async Task DeleteHandCard(IModel player, IDbTransaction transaction)
        {
            await transaction.ExecuteNonQueryAsync("delete_hand_card", new [] {PokerStatDataBaseParameter.Create("_model_id", player.Id)});
        }
    }
}