﻿using System;
using System.Data;
using System.Threading.Tasks;
using PokerStat.DataBaseHelpers;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class ActionProvider
    {
        internal async Task SaveAsync(IStageAction stageAction, IHandStage hs, IDbTransaction transaction)
        {
            var dp = new PokerStatDataBaseParameter[stageAction.Count.HasValue?6:5];
            dp[0]=PokerStatDataBaseParameter.Create("_"+nameof(stageAction.Id), Guid.NewGuid());
            dp[1]=PokerStatDataBaseParameter.Create("_"+nameof(stageAction.Action), (short) stageAction.Action);
            dp[2]=PokerStatDataBaseParameter.Create("_playerhand_id", stageAction.PlayerHand.Id);
            dp[3]=PokerStatDataBaseParameter.Create("_Handstage_id", hs.Id);
            dp[4]=PokerStatDataBaseParameter.Create("_"+nameof(stageAction.Order), stageAction.Order);
            if (stageAction.Count.HasValue)
                dp[5]=PokerStatDataBaseParameter.Create("_"+nameof(stageAction.Count), stageAction.Count.Value);

            await transaction.ExecuteNonQueryAsync("save_stage_action", dp);
            
        }
    }
}