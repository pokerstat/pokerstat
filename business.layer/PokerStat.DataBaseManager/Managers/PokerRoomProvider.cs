using System.Collections.Generic;
using System.Data;
using PokerStat.DataBaseHelpers;
using PokerStat.DataBaseManager.DTO;
using PokerStat.DataBaseManager.Interface;
using PokerStat.Model;
using PokerStat.Model.Comparers;

namespace PokerStat.DataBaseManager.Managers
{
    internal sealed class PokerRoomProvider<T> : IPokerRoomProvider where T : class, IDbConnection, new()
    {
        private readonly HashSet<IPokerRoom> _pokerroom;
        private readonly string _connection;
        
        private readonly static object _obj = new object();

        public PokerRoomProvider(string connection)
        {
            _pokerroom = new HashSet<IPokerRoom>(new PokerRoomComparer());
            _connection = connection;
            using (var con = new T {ConnectionString = _connection})
            {
                con.Open();
                using (var reader = con.ExecuteReader("get_poker_rooms"))
                {
                    while (reader.Read())
                    {
                        var dtopr = new PokerRoomDTO()
                        {
                            Id = reader.GetInt16(0),
                            Name = reader.GetString(1)
                        };
                        _pokerroom.Add(dtopr);
                    }

                    reader.Close();
                }
            }
        }

        public IPokerRoom Save(IPokerRoom pokerRoom)
        {
            if (_pokerroom.TryGetValue(pokerRoom, out IPokerRoom _pokerRoom))
                return _pokerRoom;
            using (var con = new T {ConnectionString = _connection})
            {
                con.Open();
                var dto = new PokerRoomDTO
                {
                    Name = pokerRoom.Name,
                    Id = con.ExecuteScalar<short>("save_poker_room",
                        new[] {PokerStatDataBaseParameter.Create("_"+nameof(pokerRoom.Name), pokerRoom.Name)})
                };
                lock (_obj)
                {
                     _pokerroom.Add(dto);
                }
                return dto;
            }
        }

        public IEnumerable<IPokerRoom> PokerRooms => _pokerroom;
    }
}