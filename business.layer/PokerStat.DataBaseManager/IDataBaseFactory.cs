﻿using PokerStat.DataBaseManager.Interface;

namespace PokerStat.DataBaseManager
{
    public interface IDataBaseFactory
    {
        ICardProvider CardProvider { get; }

        IPlayerProvider PlayerProvider { get; }

        IHandProvider HandProvider { get; }

        IPokerRoomProvider PokerRoomProvider { get; }

        void RefreshCache();
    }
}