﻿using System.Threading.Tasks;
using PokerStat.Model;
namespace PokerStat.DataBaseManager.Interface
{
    public interface IHandProvider
    {
        Task SaveAsync(IHand hand);
    }
}
