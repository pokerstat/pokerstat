﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Interface
{
    public interface IPlayerProvider
    {
        IPlayer Save(IPlayer player);
        /// <summary>
        /// Выгружает пользователей, у которых схожий ник
        /// </summary>
        /// <param name="nick"></param>
        /// <returns></returns>
        Task<IEnumerable<IPlayer>> GetPlayers(string nick);
    }
}
