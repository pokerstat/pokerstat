using System.Collections.Generic;
using PokerStat.Model;
namespace PokerStat.DataBaseManager.Interface
{
    public interface ICardProvider
    {
        IEnumerable<ICard> Cards{get;}
    }
}