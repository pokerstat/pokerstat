using System.Collections.Generic;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Interface
{
    public interface IPokerRoomProvider
    {
        IEnumerable<IPokerRoom> PokerRooms{get;}
        IPokerRoom Save(IPokerRoom pokerRoom);
    }
}