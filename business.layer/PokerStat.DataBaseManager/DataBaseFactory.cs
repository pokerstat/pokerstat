﻿using System;
using System.Data;
using PokerStat.DataBaseManager.Interface;
using PokerStat.DataBaseManager.Managers;

namespace PokerStat.DataBaseManager
{
    public sealed class DataBaseFactory<T> : IDataBaseFactory where T : class, IDbConnection, new()
    {
        private readonly string _connection;

        private Lazy<IHandProvider> _lazyhand;
        private readonly Lazy<CardProvider<T>> _lazycard;
        private Lazy<IPlayerProvider> _lazyplayer;
        private readonly Lazy<IPokerRoomProvider> _lazypokerroom;

        public DataBaseFactory(string connection)
        {
            _connection = connection;
            _lazycard = new Lazy<CardProvider<T>>(() => new CardProvider<T>(_connection), true);
            _lazypokerroom = new Lazy<IPokerRoomProvider>(() => new PokerRoomProvider<T>(_connection), true);
            _lazyplayer = new Lazy<IPlayerProvider>(() => new PlayerProvider<T>(_connection, _lazypokerroom.Value), true);
            _lazyhand = new Lazy<IHandProvider>(() => new HandProvider<T>(_connection, new TableProvider(_lazypokerroom.Value), _lazycard.Value, _lazyplayer.Value), true);
        }

        public ICardProvider CardProvider => _lazycard.Value;

        public IPlayerProvider PlayerProvider => _lazyplayer.Value;

        public IHandProvider HandProvider => _lazyhand.Value;

        public IPokerRoomProvider PokerRoomProvider => _lazypokerroom.Value;

        public void RefreshCache()
        {
            _lazyplayer = new Lazy<IPlayerProvider>(() => new PlayerProvider<T>(_connection, _lazypokerroom.Value), true);
            _lazyhand = new Lazy<IHandProvider>(() => new HandProvider<T>(_connection, new TableProvider(_lazypokerroom.Value), _lazycard.Value, _lazyplayer.Value), true);
        }
    }
}