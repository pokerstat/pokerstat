﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using PokerStat.Model;
using PokerStat.Model.Comparers;
using UnitTestPlayerComparer.DTO;
using UnitTestPokerRoomComparer.DTO;

namespace UnitTestPlayerComparer
{
    [TestFixture]
    public class UnitTestPlayerComparer
    {
        IPlayer c1;
        IPlayer c2;
        IPlayer c3;
        IEqualityComparer<IPlayer> comparer;

        [SetUp]
        public void Setup()
        {
            c1 = new PlayerDTO {Id = Guid.Parse("2CFBDB76-3EB4-4AAC-8379-43659DEE6769"), Nick="test",PokerRoom=new PokerRoomDTO() {  Name="ps"} };
            c2 = new PlayerDTO {Id = Guid.Parse("2CFBDB76-3EB4-4AAC-8379-43659DEE6769"), Nick = "test", PokerRoom = new PokerRoomDTO() { Name = "ps" } };
            c3 = new PlayerDTO {Id = Guid.Parse("2CFBDB76-3EB4-4AAC-8379-43659DEE6789"), Nick = "test", PokerRoom = new PokerRoomDTO() { Name = "889" } };
            comparer = new PlayerComparer();
        }

        [Test]
        public void Comparer()
        {
            Assert.Multiple(() =>
            {
                Assert.IsFalse(comparer.Equals(c1, null));
                Assert.IsFalse(comparer.Equals(null, c1));
                Assert.IsFalse(comparer.Equals(null, null));
                Assert.IsFalse(comparer.Equals(c1, c3));
                Assert.IsTrue(comparer.Equals(c1, c2));
                Assert.IsTrue(comparer.Equals(c1, c1));
            });
        }

        [Test]
        public void GetHashCodeTest()
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(comparer.GetHashCode(c1), comparer.GetHashCode(c2));
                Assert.AreNotEqual(comparer.GetHashCode(c3), comparer.GetHashCode(c1));
            });
        }
    }
}