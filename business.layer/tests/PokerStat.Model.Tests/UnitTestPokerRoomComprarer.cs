﻿using NUnit.Framework;
using System.Collections.Generic;
using PokerStat.Model;
using PokerStat.Model.Comparers;
using UnitTestPokerRoomComparer.DTO;

namespace UnitTestPokerRoomComparer
{
    [TestFixture]
    public class UnitTestPokerRoomComprarer
    {
        IPokerRoom c1;
        IPokerRoom c2;
        IPokerRoom c3;
        IEqualityComparer<IPokerRoom> comparer;

        [SetUp]
        public void Setup()
        {
            c1 = new PokerRoomDTO {Name = "ps"};
            c2 = new PokerRoomDTO {Name = "ps"};
            c3 = new PokerRoomDTO {Name = "888"};
            comparer = new PokerRoomComparer();
        }

        [Test]
        public void Comparer()
        {
            Assert.IsFalse(comparer.Equals(c1, null));
            Assert.IsFalse(comparer.Equals(null, c1));
            Assert.IsFalse(comparer.Equals(null, null));
            Assert.IsFalse(comparer.Equals(c1, c3));
            Assert.IsTrue(comparer.Equals(c1, c2));
            Assert.IsTrue(comparer.Equals(c1, c1));
        }

        [Test]
        public void GetHashCodeTest()
        {
            Assert.AreEqual(comparer.GetHashCode(c1), comparer.GetHashCode(c2));
            Assert.AreNotEqual(comparer.GetHashCode(c3), comparer.GetHashCode(c1));
        }
    }
}