﻿using PokerStat.Model;

namespace UnitTestCardComparer.DTO
{
    internal sealed class CardDTO : ICard
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public Color Color { get; set; }

        public Color AlternativeColor { get; }

        public char ShortName { get; set; }

        public char SuitCode { get; set; }

        public string SuitName { get; set; }
    }
}