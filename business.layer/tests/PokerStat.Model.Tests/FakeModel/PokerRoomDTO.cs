﻿using PokerStat.Model;

namespace UnitTestPokerRoomComparer.DTO
{
    internal sealed class PokerRoomDTO : IPokerRoom
    {
        public short Id { get; set; }

        public string Name { get; set; }
    }
}