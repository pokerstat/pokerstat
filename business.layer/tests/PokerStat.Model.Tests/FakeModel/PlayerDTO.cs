﻿using System;
using PokerStat.Model;

namespace UnitTestPlayerComparer.DTO
{
    internal sealed class PlayerDTO : IPlayer
    {
        public PlayerDTO(IPlayer player)
        {
            Nick = player.Nick;
            PokerRoom = player.PokerRoom;
            Id = Guid.NewGuid();
        }

        public PlayerDTO()
        {
        }

        public string Nick { get; set; }

        public IPokerRoom PokerRoom { get; set; }

        public Guid Id { get; set; }
    }
}