using NUnit.Framework;
using System.Collections.Generic;
using PokerStat.Model;
using PokerStat.Model.Comparers;
using UnitTestCardComparer.DTO;

namespace UnitTestCardComparer
{
    [TestFixture]
    public class UnitTestCardComparer
    {
        ICard c1;
        ICard c2;
        ICard c3;
        IEqualityComparer<ICard> comparer;

        [SetUp]
        public void Setup()
        {
            c1 = new CardDTO {Id = 1, ShortName = 'A', SuitCode = 's'};
            c2 = new CardDTO {Id = 1, ShortName = '9', SuitCode = 'd'};
            c3 = new CardDTO {Id = 2, ShortName = 'A', SuitCode = 's'};
            comparer = new CardComparer();
        }

        [Test]
        public void Comparer()
        {
            Assert.Multiple(() =>
            {
                Assert.IsFalse(comparer.Equals(c1, null));
                Assert.IsFalse(comparer.Equals(null, c1));
                Assert.IsFalse(comparer.Equals(null, null));
                Assert.IsFalse(comparer.Equals(c1, c3));
                Assert.IsTrue(comparer.Equals(c1, c2));
                Assert.IsTrue(comparer.Equals(c1, c1));
            });
        }

        [Test]
        public void GetHashCodeTest()
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(comparer.GetHashCode(c1), 1);
                Assert.AreEqual(comparer.GetHashCode(c3), 2);
            });
        }
    }
}