using System;
using System.Collections;
using System.Data.Common;

namespace PokerStat.DataBaseManager.Tests.Fake
{
    public class FakePokerRoomReader:DbDataReader
    {
        readonly string[,] pokerroom = { { "1",  "PokerRoom",}};
        short index = -1;
        public override bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public override byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public override long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public override char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public override long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public override string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public override decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public override double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public override IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public override float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public override Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public override short GetInt16(int i)
        {
            return Convert.ToInt16(pokerroom[index, i]);
        }

        public override int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        public override long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public override string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public override string GetString(int i)
        {
            return pokerroom[index, i];
        }

        public override object GetValue(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public override bool IsDBNull(int i)
        {
            throw new NotImplementedException();
        }

        public override int FieldCount { get; }
        public override bool HasRows { get; }

        public override object this[int i] => throw new NotImplementedException();

        public override object this[string name] => throw new NotImplementedException();

        public override bool NextResult()
        {
            throw new NotImplementedException();
        }

        public override bool Read()
        {
            index++;
            return index < 1;
        }

        public override int Depth { get; }
        public override bool IsClosed { get;  }
        public override int RecordsAffected { get;  }
    }
}