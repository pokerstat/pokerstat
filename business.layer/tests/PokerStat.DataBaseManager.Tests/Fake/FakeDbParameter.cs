﻿using System;
using System.Data;
using System.Data.Common;

namespace PokerStat.DataBaseManager.Tests.Fake
{
    public class FakeDbParameter: DbParameter,IDbDataParameter
    {
        public FakeDbParameter(string name, object value, DbType dbType)
        {
            ParameterName = name;
            Value = value;
            DbType = dbType;
        }
        public FakeDbParameter(string name, object value, DbType dbType,int size):this(name,value,dbType)
        {
            Size = size;
        }
        public FakeDbParameter()
        {

        }

        
        public override byte Precision { get ; set; }
        public override byte Scale { get; set ; }

        public override int Size { get; set; }

        //public int Size { get; set ; }
        public override void ResetDbType()
        {
            throw new NotImplementedException();
        }

        public override DbType DbType { get; set ; }
        public override ParameterDirection Direction { get ; set ; }

        public override bool IsNullable
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public override string ParameterName { get; set ; }
        public override string SourceColumn { get; set; }
        public override bool SourceColumnNullMapping { get; set; }
        public override DataRowVersion SourceVersion { get; set ; }
        public override object Value { get ; set; }
    }
}
