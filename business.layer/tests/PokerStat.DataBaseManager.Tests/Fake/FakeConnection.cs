using System.Data;
using System.Data.Common;
using PokerStat.DataBaseManager.Test.Fake;

namespace PokerStat.DataBaseManager.Tests.Fake
{
    public class FakeConnection:DbConnection
    {
        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
        {
            return new FakeTransaction();
        }
        
        public override void ChangeDatabase(string databaseName)
        {
           
        }

        public override void Close()
        {
            
        }
        
        protected override DbCommand CreateDbCommand()
        {
            return  new FakeDbCommand();
        }

        public override void Open()
        {
            
        }

        public override string ConnectionString { get; set; }
   
        public override string Database { get; }
        public override string DataSource { get; }

        public override string ServerVersion { get; }
        public override ConnectionState State { get;  }
    }
}