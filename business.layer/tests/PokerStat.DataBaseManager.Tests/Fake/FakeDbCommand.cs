using System;
using System.Data;
using System.Data.Common;
using PokerStat.DataBaseManager.Tests.Fake.FakeModel;
using PokerStat.DataBaseManager.Test.Fake;

namespace PokerStat.DataBaseManager.Tests.Fake
{
    public class FakeDbCommand : DbCommand
    {
        public override string CommandText { get; set; }
        public override int CommandTimeout { get; set; }
        public override CommandType CommandType { get; set; }
       // public DbConnection Connection { get; set; }
        protected override DbConnection DbConnection { get; set; }
        protected override DbParameterCollection DbParameterCollection => new FakeDataParameterCollection();
        protected override DbTransaction DbTransaction { get; set; }
        public override bool DesignTimeVisible { get; set; }
        
        public override UpdateRowSource UpdatedRowSource { get; set; }

        public override void Cancel()
        {
        }

        protected override DbParameter CreateDbParameter()
        {
            return new FakeDbParameter();
        }

        protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
        {
            if (CommandText == "get_card" || CommandText == "get_hand_card"|| CommandText=="get_stage_card")
            {
                return new FakeCardReader();
            }
            if (CommandText == "get_poker_rooms")
            {
                return new FakePokerRoomReader();
            }
            if (CommandText == "get_player_hand")
            {
                return new FakePlayersHandReader();
            }
            if (CommandText == "get_hand_stages" )
            {
                return new FakeHandStage();
            }
            if (CommandText == "get_stage_action")
            {
                return new FakeStageActionReadercs();
            }
            if (CommandText == "get_players" )
            {
                return new FakePlayerReader();
            }
            throw new NotImplementedException(CommandText);
        }

        public override int ExecuteNonQuery()
        {
            if (CommandText == "save_player_hand")
            {
                return 1;
            }

            if (CommandText == "save_hand")
            {
                return 1;
            }

            if (CommandText == "save_stage_action")
            {
                return 1;
            }

            if (CommandText == "save_stage_card")
            {
                return 1;
            }

            if (CommandText == "save_hand_card")
            {
                return 1;
            }
            if (CommandText == "set_hand_to_removed")
            {
                return 1;
            }
            if (CommandText == "save_stage_hand")
            {
                return 1;
            }

            throw new NotImplementedException(CommandText);
        }

        public override object ExecuteScalar()
        {
            if (CommandText == "сheck_exists_hand")
            {
                return 0;
            }

            if (CommandText == "save_player")
            {
                return Guid.NewGuid();
            }

            if (CommandText == "save_poker_room")
            {
                return 1;
            }

            if (CommandText == "save_table")
            {
                return Guid.NewGuid();
            }

            if (CommandText == "save_tournament")
            {
                return Guid.NewGuid();
            }
            throw new NotImplementedException(CommandText);
        }

        public override void Prepare()
        {
        }

        protected override void Dispose(bool disposing)
        {
            
        }
    }
}