using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace PokerStat.DataBaseManager.Test.Fake
{
    public class FakeDataParameterCollection:DbParameterCollection
    {
         readonly IList<IDataParameter> _list;
        public FakeDataParameterCollection()
        {
            _list = new List<IDataParameter>();
        }
        protected override void SetParameter(string parameterName, DbParameter value)
        {
            throw new NotImplementedException();
        }

        public override int Count => _list.Count;
        

        public override object SyncRoot => throw new NotImplementedException();

        public override int Add(object value)
        {
            var param = (IDataParameter) value;
            if (string.IsNullOrEmpty(param.ParameterName) || param.Value == null )
                throw new ArgumentException("Ошибка в параметрах->" + param.ParameterName);
            _list.Add((IDataParameter)value);
          return  _list.Count - 1;
        }

        public override void AddRange(Array values)
        {
            foreach (var v in values)
            {
                Add(v);
            }
        }

        public override void Clear()
        {
            _list.Clear();
        }

        public override bool Contains(string parameterName)
        {
           return _list.Any(x => x.ParameterName == parameterName);
        }

        public override bool Contains(object value)
        {
           return _list.Any(x => x.ParameterName == ((IDataParameter) value).ParameterName);
        }

        public override void CopyTo(Array array, int index)
        {
           // _list.CopyTo(array, index);
        }

        public override IEnumerator GetEnumerator()
        {
           return _list.GetEnumerator();
        }

        protected override DbParameter GetParameter(int index)
        {
            throw new NotImplementedException();
        }

        protected override DbParameter GetParameter(string parameterName)
        {
            throw new NotImplementedException();
        }

        public override int IndexOf(string parameterName)
        {
           return _list.IndexOf(_list.FirstOrDefault(x => x.ParameterName == parameterName));
        }

        public override int IndexOf(object value)
        {
            return _list.IndexOf((IDataParameter)value);
        }

        public override void Insert(int index, object value)
        {

        }

        public override void Remove(object value)
        {

        }

        public override void RemoveAt(string parameterName)
        {

        }

        protected override void SetParameter(int index, DbParameter value)
        {
            throw new NotImplementedException();
        }

        public override void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }
    }
}