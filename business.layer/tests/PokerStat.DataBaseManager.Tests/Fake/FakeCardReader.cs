using System;
using System.Collections;
using System.Data.Common;

namespace PokerStat.DataBaseManager.Test.Fake
{
    public class FakeCardReader: DbDataReader
    {
        readonly string[,] Cards = { { "3",  "Ace", "A", "d", "diamond",  "-65536", "-16776961"},
                                     { "4",  "Ace",  "A", "h","hearts",   "-65536", "-65536"},
                                     { "5",  "Ace",  "A", "s", "sprade",  "-16777216", "-16776961"},
                                     { "6",  "Ace",  "A", "c", "peaks",  "-16744448", "-16776961"},
                                     { "7",  "9", "9",  "d", "diamond",  "-65536", "-16776961"},
                                     { "8",  "9", "9", "h","hearts",   "-65536", "-65536"},
                                     { "9",  "9", "9", "s", "sprade",  "-16777216", "-16776961"},
                                     { "10",  "9", "9", "c", "peaks",  "-16744448", "-16776961"},
};
        short index = -1;
        public FakeCardReader()
        {
           
        }

        public override object this[int i] => throw new NotImplementedException();

        public override object this[string name] => throw new NotImplementedException();

        public override int Depth => throw new NotImplementedException();

        public override bool HasRows { get; }
        public override bool IsClosed => true;

        public override int RecordsAffected => throw new NotImplementedException();

        public override int FieldCount => throw new NotImplementedException();

        

        public override bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public override byte GetByte(int i)
        {
            return Convert.ToByte(Cards[index, i]);
        }

        public override long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public override char GetChar(int i)
        {
            return Cards[index, i][0];
        }

        public override long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }
        public override string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public override decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public override double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public override IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public override float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public override Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public override short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetInt32(int i)
        {
            return Convert.ToInt32(Cards[index, i]);
        }

        public override long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public override string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public override string GetString(int i)
        {
            return Cards[index, i];
        }

        public override object GetValue(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public override bool IsDBNull(int i)
        {
            return string.IsNullOrEmpty(Cards[index, i]);
        }

        public override bool NextResult()
        {
            throw new NotImplementedException();
        }

        public override bool Read()
        {
            index++;
            return index < 8;
        }
    }
}