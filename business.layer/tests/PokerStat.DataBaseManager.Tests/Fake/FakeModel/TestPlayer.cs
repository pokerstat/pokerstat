﻿using PokerStat.Model;
using System;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestPlayer:IPlayer
    {
      public Guid Id { get; set; }
      public string Nick { get; set; }
      public IPokerRoom PokerRoom { get; set; }
    }
}
