using PokerStat.Model;
using System;
using System.Collections.Generic;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestHand:IHand
    {
        public string Identifier { get; set; }

        public DateTime Date { get; set; }
        
        public ITable Table { get; set; }

        public IEnumerable<IHandStage> StageHands { get; set; }

        public Guid Id { get; set; }
    }
}
