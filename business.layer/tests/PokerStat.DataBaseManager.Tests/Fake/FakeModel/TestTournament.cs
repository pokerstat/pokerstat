using PokerStat.Model;
using System;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestTournament:ITournament
    { 
        public string Identifier{get;set;}
        public decimal Buy_in { get; set; }
        public TypeOfCurrency TypeOfCurrency { get; set; }

        public Guid Id { get; set; }
    }
}