using PokerStat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestHandPlayer:IPlayerHand
    {
      public Position Position { get; set; }

        public decimal CountChips { get; } = 500;

        public IPlayer Player { get; } = new TestPlayer{Nick="test", PokerRoom = new TestPokerRoom{Name="TestPokerRoom"}};

        public ICollection<ICard> HandCard { get; } = new Collection<ICard>{new TestCard('A','d'),new TestCard('9','d')};

        public Guid Id { get; set; }
    }
}
