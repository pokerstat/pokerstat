using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestPokerRoom:IPokerRoom
    {
      public short Id { get; set; }
      public string Name { get; set; }
    }
}