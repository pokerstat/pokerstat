using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestCard:ICard
    { 
        public TestCard(char shortName, char suitCode)
        {
            ShortName=shortName;
            SuitCode=suitCode;
        }
       public byte Id { get; set; }

        public string Name { get; set; }

        public char ShortName { get; set; }

        public char SuitCode { get; set; }

       public string SuitName { get; set;}
       public Color Color { get; set; }
       public Color AlternativeColor { get; set; }
    }
}