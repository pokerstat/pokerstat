using PokerStat.Model;
using System;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestTable:ITable
    { 
        public string Identifier{get;set;}

        public ITournament Tournament {get;set;}
        public TypeOfCurrency TypeOfCurrency { get; set; }
        public TypeOfGame TypeOfGame { get; set; }
        public IPokerRoom PokerRoom { get; set; }

        public Guid Id { get; set; }
    }
}