﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestHandStage: IHandStage
    {
        public Stages Stage { get; set; }

        public ICollection<ICard> Cards { get; set; } = new Collection<ICard> { new TestCard('A', 'd'), new TestCard('9', 'd') };

        public IEnumerable<IStageAction> Actions { get; } =new Collection<IStageAction> {new TestStageAction{Action=Model.Action.Raises }, new TestStageAction{Action=Model.Action.Calls}};

        public Guid Id { get; set; }
    }
}
