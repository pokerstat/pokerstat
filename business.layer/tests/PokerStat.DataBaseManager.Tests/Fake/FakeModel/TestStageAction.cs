using PokerStat.Model;
using System;


namespace PokerStat.DataBaseManager.Tests.Fake.FakeModel
{
    public class TestStageAction:IStageAction
    {
       public Model.Action Action { get; set; }
       public byte Order { get; set; }

       public decimal? Count { get; set; } = 150;

        public Guid Id { get; set; } =Guid.NewGuid();

        public IPlayerHand PlayerHand { get; set; } = new TestHandPlayer();
    }
}
