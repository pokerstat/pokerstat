using System;
using System.Collections;
using System.Data.Common;

namespace PokerStat.DataBaseManager.Test.Fake
{
    public class FakeHandReader : DbDataReader
    {
        readonly string[,] hands =
        {
            {
                Guid.NewGuid().ToString(), "Ace", DateTime.Now.ToString(), "1", "1", "TestPokerRoom",
                Guid.NewGuid().ToString(), "testtable", "1", Guid.NewGuid().ToString(), "testtournamnet", "123.121", "1"
            },
            {
                Guid.NewGuid().ToString(), "Ace", DateTime.Now.ToString(), "1", "1", "TestPokerRoom",
                Guid.NewGuid().ToString(), "testtable", "1", Guid.NewGuid().ToString(), "testtournamnet", "123.122", "1"
            },
            {
                Guid.NewGuid().ToString(), "Ace", DateTime.Now.ToString(), "1", "1", "TestPokerRoom",
                Guid.NewGuid().ToString(), "testtable", "1", Guid.NewGuid().ToString(), "testtournamnet", "123.123", "1"
            }
        };

        short index = -1;

        public FakeHandReader()
        {
        }

        public override object this[int i] => throw new NotImplementedException();

        public override object this[string name] => throw new NotImplementedException();

        public override int Depth => throw new NotImplementedException();

        public override bool HasRows { get; }
        public override bool IsClosed => true;

        public override int RecordsAffected => throw new NotImplementedException();

        public override int FieldCount => throw new NotImplementedException();


        public override bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public override byte GetByte(int i)
        {
            return Convert.ToByte(hands[index, i]);
        }

        public override long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public override char GetChar(int i)
        {
            return hands[index, i][0];
        }

        public override long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public override string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetDateTime(int i)
        {
          return DateTime.Parse(hands[index, i]);
        }

        public override decimal GetDecimal(int i)
        {
           return Decimal.Parse(hands[index, i]);
        }

        public override double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public override IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public override float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public override Guid GetGuid(int i)
        {
           return Guid.Parse(hands[index, i]);
        }

        public override short GetInt16(int i)
        {
            return Convert.ToInt16(hands[index, i]);
        }

        public override int GetInt32(int i)
        {
            return Convert.ToInt32(hands[index, i]);
        }

        public override long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public override string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public override string GetString(int i)
        {
            return hands[index, i];
        }

        public override object GetValue(int i)
        {
            throw new NotImplementedException();
        }

        public override int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public override bool IsDBNull(int i)
        {
            return string.IsNullOrEmpty(hands[index, i]);
        }

        public override bool NextResult()
        {
            throw new NotImplementedException();
        }

        public override bool Read()
        {
            index++;
            return index < 3;
        }
    }
}