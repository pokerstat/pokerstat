using System.Data;
using System.Data.Common;
using PokerStat.DataBaseManager.Tests.Fake;

namespace PokerStat.DataBaseManager.Test.Fake
{
    public class FakeTransaction:DbTransaction
    {

        public override void Commit()
        {
            
        }

        public override void Rollback()
        {
            
        }
        
        protected override DbConnection DbConnection => new FakeConnection();
        public override IsolationLevel IsolationLevel { get; }
    }
}