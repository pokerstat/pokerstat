using System.Linq;
using NUnit.Framework;
using PokerStat.DataBaseManager.Tests.Fake;

namespace PokerStat.DataBaseManager.Tests
{
    [TestFixture]
    public class UnitTestPokerRoomProvider
    {
        private IDataBaseFactory factory;

        [SetUp]
        public void SetUp()
        {
            factory = new DataBaseFactory<FakeConnection>("test");
        }
        [Test]
        public void TestGetPokerRoom()
        {
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(factory.PokerRoomProvider.PokerRooms);
                Assert.AreEqual(factory.PokerRoomProvider.PokerRooms.Count(),1);
                Assert.AreEqual(factory.PokerRoomProvider.PokerRooms.First().Id,1);
            });
        }
    }
}