﻿using System.Linq;
using NUnit.Framework;
using PokerStat.DataBaseManager.Tests.Fake;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests
{
    [TestFixture]
    public class UnitTestCardProvider
    {
        IDataBaseFactory _factory;


        [SetUp]
        public void SetUp()
        {
            _factory = new DataBaseFactory<FakeConnection>("test");
        }


        [Test]
        public void TestGetCard()
        {
            Assert.Multiple(() =>
            {
                Assert.IsTrue(_factory.CardProvider.Cards.Count() == 8);
                Assert.IsTrue(_factory.CardProvider.Cards.Count(x => x.ShortName == 'A') == 4);
                Assert.IsTrue(_factory.CardProvider.Cards.Count(x => x.ShortName == '9') == 4);
                Assert.IsFalse(_factory.CardProvider.Cards.Any(x => x.ShortName == '3'));
                Assert.IsTrue(_factory.CardProvider.Cards.Any(x => x.Color == Color.Black));
            });
        }
    }
}