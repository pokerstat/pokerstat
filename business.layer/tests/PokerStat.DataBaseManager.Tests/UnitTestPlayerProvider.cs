using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using PokerStat.DataBaseManager.Tests.Fake;
using PokerStat.DataBaseManager.Tests.Fake.FakeModel;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests
{
    [TestFixture]
    public class UnitTestPlayerProvider
    {
        private IDataBaseFactory factory;

        [SetUp]
        public void SetUp()
        {
            factory = new DataBaseFactory<FakeConnection>("test");
        }

        [Test]
        public void TestSavePlayer()
        {
            IPlayer testplayer = new TestPlayer {Nick = "test", PokerRoom = new TestPokerRoom {Name = "TestPokerRoom"}};

            testplayer = factory.PlayerProvider.Save(testplayer);
            Assert.Multiple(() =>
            {
                Assert.IsFalse(testplayer.Id == Guid.Empty);
                Assert.IsFalse(testplayer.PokerRoom.Id == default(short));
                Assert.IsFalse(string.IsNullOrEmpty(testplayer.Nick));
                Assert.IsFalse(string.IsNullOrEmpty(testplayer.PokerRoom.Name));
            });
        }

        [Test]
        public async Task TestGetPlayer()
        {
            IEnumerable<IPlayer> testplayer = await factory.PlayerProvider.GetPlayers("1");
            Assert.Multiple(() =>
            {
                Assert.IsFalse(testplayer.First().Id == Guid.Empty);
                Assert.IsFalse(testplayer.First().PokerRoom.Id == default(short));
                Assert.AreEqual(testplayer.First().Nick, "test player");
            });
        }
    }
}