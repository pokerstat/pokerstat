using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using NUnit.Framework;
using PokerStat.DataBaseManager.Tests.Fake;
using PokerStat.DataBaseManager.Tests.Fake.FakeModel;
using PokerStat.Model;

namespace PokerStat.DataBaseManager.Tests
{
    [TestFixture]
    public class UnitTestHandProvider
    {
        TestHand testhand;
        IDataBaseFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new DataBaseFactory<FakeConnection>("test");
            testhand = new TestHand
            {
                Identifier = "test",
                Table = new TestTable
                {
                    Identifier = "test_table",
                    PokerRoom = new TestPokerRoom {Name = "TestPokerRoom"},
                    TypeOfGame = TypeOfGame.Cash,
                    TypeOfCurrency = TypeOfCurrency.USD,
                    Tournament = new TestTournament {Identifier = "test_tournament"}
                },
                Date = DateTime.UtcNow
            };
        }

        [Test]
        public async Task TestSaveHand()
        {
            testhand.StageHands = new Collection<IHandStage>
                {new TestHandStage {Stage = Stages.Ante, Cards = null}, new TestHandStage {Stage = Stages.Flop}};
            await _factory.HandProvider.SaveAsync(testhand);
        }
    }
}