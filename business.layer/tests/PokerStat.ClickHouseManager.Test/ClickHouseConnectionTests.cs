using NUnit.Framework;
using PokerStat.ClickHouse.Helper;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [TestCase("localhost", null, null, false, null, "http://localhost")]
        [TestCase("localhost", "test", null, false, null, "http://test@localhost")]
        [TestCase("localhost", "test", null, false, 43, "http://test@localhost:43")]
        [TestCase("localhost", "test", "teeet", false, 43, "http://test:teeet@localhost:43")]
        [TestCase("localhost", "test", "teeet", true, 43, "https://test:teeet@localhost:43")]
        public void Should_FormatConnectionString(string server, string user, string pass, bool isSsl, int port, string excepted)
        {
            var chcon = new ClickHouseConnection();
            chcon.Server = server;
            chcon.User = user;
            chcon.Password = pass;
            chcon.IsSSLMode = isSsl;
            chcon.Port = port;
            Assert.AreEqual(excepted, chcon.ToString());
        }
    }
}