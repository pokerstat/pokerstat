﻿namespace PokerStat.Model
{
    public enum Position:byte
    {
        UnderTheGun=1,
        UnderTheGun1 = 2,
        UnderTheGun2 = 3,
        MiddlePosition1=4,
        MiddlePosition2 = 5,
        MiddlePosition3 = 6,
        CutOff=7,
        Button=8,
        SmallBlind=9,
        BigBlind=10,
    }
}
