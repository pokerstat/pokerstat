﻿using System;
using System.Collections.Generic;

namespace PokerStat.Model.Comparers
{
    public sealed class PokerRoomComparer : IEqualityComparer<IPokerRoom>
    {
        public bool Equals(IPokerRoom x, IPokerRoom y)
        {
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (ReferenceEquals(x, y)) return true;
            return String.Equals(x.Name, y.Name, StringComparison.CurrentCultureIgnoreCase);
        }

        public int GetHashCode(IPokerRoom obj)
        {
            return obj.Name.GetHashCode() * 13;
        }
    }
}