﻿using System;
using System.Collections.Generic;

namespace PokerStat.Model.Comparers
{
    public sealed class PlayerComparer : IEqualityComparer<IPlayer>
    {
        public bool Equals(IPlayer x, IPlayer y)
        {
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (ReferenceEquals(x, y)) return true;
            return String.Equals(x.Nick, y.Nick, StringComparison.CurrentCultureIgnoreCase) && new PokerRoomComparer().Equals(x.PokerRoom,y.PokerRoom);
        }

        public int GetHashCode(IPlayer obj)
        {
            return obj.Nick.GetHashCode() * 41 + (obj.PokerRoom==null?0: new PokerRoomComparer().GetHashCode(obj.PokerRoom));
        }
    }
}