using System;
using System.Collections.Generic;

namespace PokerStat.Model.Comparers
{
    public class TournamentComparer:IEqualityComparer<ITournament>
    {
        public bool Equals(ITournament x, ITournament y)
        {
            if (x == null && y == null) return true;
            if (ReferenceEquals(x, y)) return true;
            return String.Equals(x.Identifier, y.Identifier, StringComparison.CurrentCultureIgnoreCase) && x.Buy_in==y.Buy_in;
        }

        public int GetHashCode(ITournament obj)
        {
            return obj.Identifier.GetHashCode() * 7 + obj.Buy_in.GetHashCode()*13;
        }
    }
}