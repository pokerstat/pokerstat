﻿using System.Collections.Generic;

namespace PokerStat.Model.Comparers
{
    public sealed class CardComparer : IEqualityComparer<ICard>
    {
        public bool Equals(ICard x, ICard y)
        {
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (ReferenceEquals(x, y)) return true;
            return x.Id == y.Id;
        }

        public int GetHashCode(ICard obj)
        {
            return obj.Id;
        }
    }
}