using System;
using System.Collections.Generic;

namespace PokerStat.Model.Comparers
{
    public class TableComparer:IEqualityComparer<ITable>
    {
        public bool Equals(ITable x, ITable y)
        {
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (ReferenceEquals(x, y)) return true;
            return String.Equals(x.Identifier, y.Identifier, StringComparison.CurrentCultureIgnoreCase) && new PokerRoomComparer().Equals(x.PokerRoom,y.PokerRoom)&& new TournamentComparer().Equals(x.Tournament,y.Tournament)
                && x.TypeOfCurrency==y.TypeOfCurrency && x.TypeOfGame==y.TypeOfGame;
        }

        public int GetHashCode(ITable obj)
        {
            return obj.Identifier.GetHashCode() * 13 + obj.TypeOfCurrency.GetHashCode() * 3 + obj.TypeOfGame.GetHashCode() * 17 + new PokerRoomComparer().GetHashCode(obj.PokerRoom)
                + (obj.Tournament==null ?0: new TournamentComparer().GetHashCode(obj.Tournament));
        }
    }
}