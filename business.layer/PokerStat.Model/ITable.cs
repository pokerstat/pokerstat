﻿namespace PokerStat.Model
{
    public interface ITable:IModel
    {
        string Identifier { get; }
        ITournament Tournament { get; }
        TypeOfCurrency TypeOfCurrency { get; }
        TypeOfGame TypeOfGame { get; }
        IPokerRoom PokerRoom { get; }
    }
}
