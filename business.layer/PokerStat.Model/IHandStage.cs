
using System.Collections.Generic;

namespace PokerStat.Model
{
    /// <summary>
    /// Стадия разадчи
    /// </summary>
    public interface IHandStage : IModel
    {
        /// <summary>
        /// Получения типа стадии
        /// </summary>
        /// <value>The stage.</value>
        Stages Stage { get; }
        /// <summary>
        /// Получение карт разадачи
        /// </summary>
        /// <value>The cards.</value>
        ICollection<ICard> Cards {get;}
        /// <summary>
        /// Получение активностей игроков стадии
        /// </summary>
        /// <value>The actions.</value>
        IEnumerable<IStageAction> Actions { get; }
    }
}
