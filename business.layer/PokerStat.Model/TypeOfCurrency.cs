namespace PokerStat.Model
{
    public enum TypeOfCurrency:byte
    {
        Chips=0,
        USD=1,
        Euro=2
    }
}