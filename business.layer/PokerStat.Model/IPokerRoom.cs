

namespace PokerStat.Model
{
    public interface IPokerRoom
    {
        short Id { get;  }
        string Name { get; }
    }
}
