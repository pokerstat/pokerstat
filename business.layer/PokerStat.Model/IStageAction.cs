﻿
namespace PokerStat.Model
{
    public interface IStageAction:IModel
    {
        IPlayerHand PlayerHand { get; }
        Action Action { get; }
        byte Order { get; }
        decimal? Count { get; }
    }
}
