
namespace PokerStat.Model
{
   
    public enum Stages : byte 
    {
        Ante=1,
        PreFlop=2,
        Flop=3,
        Turn=4,
        River=5,
        ShowDown=6
     }

}
