using System;
using System.Collections.Generic;

namespace PokerStat.Model
{
    /// <summary>
    /// Раздача
    /// </summary>
    public interface IHand : IModel
    {
        /// <summary>
        /// Идентификатор раздачи
        /// </summary>
        /// <value>The identifier.</value>
        string Identifier { get; }
        /// <summary>
        /// Время раздачи
        /// </summary>
        /// <value>The date.</value>
        DateTime Date { get; }
        /// <summary>
        /// Стол, где осуществилась разадача
        /// </summary>
        /// <value>The table.</value>
        ITable Table { get; }
        /// <summary>
        /// Список стадий разадачи
        /// </summary>
        /// <value>The stage hands.</value>
        IEnumerable<IHandStage> StageHands { get; set; }
    }
}
