﻿
namespace PokerStat.Model
{
    public interface ITournament : IModel
    {
        string Identifier { get; }
        decimal Buy_in { get; }
    }
}
