﻿

namespace PokerStat.Model
{
    public enum Color:int
    {
        Blue= -16776961,
        Black= -16777216,
        Red= -65536,
        Green= -16744448
    }
}