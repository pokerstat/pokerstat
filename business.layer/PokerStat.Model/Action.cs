
using System;

namespace PokerStat.Model
{
    /// <summary>
    /// Перечисление действий игрока за столом
    /// </summary>
    [Flags]
    public enum Action : short
    {
        NotFound = 1,
        Raises = 2,
        Folds = 4,
        Checks = 8,
        Calls = 16,
        Posts = 32,
        SmallBlind = 64,
        BigBlind = 128,
        Ante = 256,
        All_In = 512,
        Collected = 1024
    }
}
