﻿namespace PokerStat.Model
{
    public interface IPlayer:IModel
    {
        string Nick { get; }
        IPokerRoom PokerRoom { get; }
    }
}
