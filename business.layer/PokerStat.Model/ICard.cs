﻿namespace PokerStat.Model
{
    /*
     * Бубны — Diamonds — d
       Пики — Spades — s
       Черви — Hearts — h
       Трефы (крести) — Clubs — c
    *//// <summary>
    /// Короткое представление карт
    /// </summary>
      public interface IShortCard 
      {
        /// <summary>
        /// Полочение короткого имя карты
        /// </summary>
        /// <value>The short name.</value>
        char ShortName { get; }
        /// <summary>
        /// Получение короткого иммя масти
        /// </summary>
        /// <value>The suit code.</value>
        char SuitCode { get; }
    }
    /// <summary>
    /// Полное представление карт
    /// </summary>
    public interface ICard:IShortCard
    {
        byte Id { get; }
        string Name { get; }
        string SuitName { get; }
        Color Color { get; }
        Color AlternativeColor { get; }
    }
}
