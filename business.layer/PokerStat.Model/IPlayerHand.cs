﻿
using System.Collections.Generic;

namespace PokerStat.Model
{
    public interface IPlayerHand:IModel
    {
        Position Position { get;  }
        decimal CountChips { get; }
        IPlayer Player { get; }
        ICollection<ICard> HandCard { get; }
    }
}