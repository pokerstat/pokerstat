﻿using System;
namespace PokerStat.Model
{
    public interface IModel
    {
        Guid Id { get; }
    }
}
