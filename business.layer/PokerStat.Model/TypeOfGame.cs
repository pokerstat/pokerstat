﻿namespace PokerStat.Model
{
    /// <summary>
    /// Тип игры!
    /// </summary>
    public enum TypeOfGame:byte
    {
        Zoom=0,
        Cash=1
    }
}
