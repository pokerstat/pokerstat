using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PokerStat.ClickHouseManager
{
    public interface IClickHouseManager
    {
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем int</returns>
        Task<int> GetIntAsync(string query);
        
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем decimal</returns>
        Task<decimal> GetDecimalAsync(string query);
        
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем string</returns>
        Task<string> GetStringAsync(string query);
        
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем список целочисленных занчений</returns>
        Task<int[]> GetIntsAsync(string query);
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем список строчек</returns>
        Task<string[]> GetStringsAsync(string query);
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем список id</returns>
        Task<Guid[]> GetGuidsAsync(string query);
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем id</returns>
        Task<Guid> GetGuidAsync(string query);
        /// <summary>
        /// Возвращает результат выполнения запроса
        /// </summary>
        /// <param name="query">sql запрос</param>
        /// <returns>возвращаем список значений с плавающей запятой</returns>
        Task<decimal[]> GetDecimalsAsync(string query);
    }
}