using System.Text;

namespace PokerStat.ClickHouse.Helper
{
    public sealed class ClickHouseConnection
    {
        public string User { get; set; }
        public string Password { get; set; }
        public bool IsSSLMode { get; set; }
        
        public string Server { get; set; }

        public int? Port { get; set; }
        public override string ToString()
        {
            Server = Server.Trim().ToLower();
            var sb = new StringBuilder(IsSSLMode ? @"https://" : @"http://");
            if (!string.IsNullOrEmpty(User))
            {
                sb.Append(User);
                if (!string.IsNullOrEmpty(Password))
                    sb.Append(':').Append(Password);
                sb.Append('@');
            }

            sb.Append(Server);
            if (Port.HasValue && Port!=0)
                sb.Append(':').Append(Port);


            return sb.ToString();
        }
    }
}