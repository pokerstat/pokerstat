using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using PokerStat.ClickHouse.Helper;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.ClickHouseManager
{
    public class ClickHouseManager : IClickHouseManager
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ClickHouseConnection _url;
        public ClickHouseManager(IHttpClientFactory clientFactory, ClickHouseConnection connection)
        {
            _clientFactory = clientFactory;
            _url = connection;
        }
        
        private HttpRequestMessage CreateQuery(Dictionary<string, string> query)
        {
            if (!query.Any())
                throw new ArgumentException("query is empty", nameof(query));
            
            return new HttpRequestMessage
            {
                RequestUri = new Uri(QueryHelpers.AddQueryString(_url.ToString(), query)),
                Method = HttpMethod.Get
            };
        }

        private async Task<HttpContent> ExecuteAsync(HttpRequestMessage requestMessage)
        {
            var httpClient = _clientFactory.CreateClient();

            var responseMessage = await httpClient.SendAsync(requestMessage);


            if (responseMessage.IsSuccessStatusCode)
            {
                return responseMessage.Content;
            }

            var pokerStatError = new PokerStatError(PokerStatErrorCodes.ClickHouse);
            pokerStatError.Messages = new[]
            {
                new ErrorMessage
                {
                    Key = responseMessage.StatusCode.ToString(),
                    Value = responseMessage.ReasonPhrase
                }
            };
            throw new PokerStatException(pokerStatError);
        }

        public async Task<int> GetIntAsync(string query)
        {
            var result = await GetStringAsync(query);
            return int.TryParse(result, out int i) ? i : 0;
        }

        public async Task<decimal> GetDecimalAsync(string query)
        {
            var result = await GetStringAsync(query);
            return decimal.TryParse(result, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal i) ? i : 0;
        }

        public async Task<string> GetStringAsync(string query)
        {
            var quedic = new Dictionary<string, string>()
            {
                {"query", query}
            };

            var content = await ExecuteAsync(CreateQuery(quedic));
            var res_string = await content.ReadAsStringAsync();
            return string.IsNullOrEmpty(res_string) ? string.Empty : res_string;
        }

        public async Task<int[]> GetIntsAsync(string query)
        {
            var massive = await GetStringsAsync(query);

            // фиг знает сколько может быть возвращено элементов, поэтому создаем массив, а не используем динамические коллекции (экономия памяти и быстродейстродействия)
            var result_massive = new int[massive.Length]; 

            for (long i = 0; i < massive.LongLength; i++)
            {
                if (int.TryParse(massive[i], out int res))
                    result_massive[i] = res;
                else
                {
                    result_massive[i] = 0;
                }
            }
            return result_massive;
        }

        public async Task<string[]> GetStringsAsync(string query)
        {
            var quedic = new Dictionary<string, string>()
            {
                {"query", query}
            };

            var content = await ExecuteAsync(CreateQuery(quedic));
            var res_string = await content.ReadAsStringAsync();
            return string.IsNullOrEmpty(res_string) ? new string[0] : (res_string).Split(Environment.NewLine);
        }

        public async Task<Guid> GetGuidAsync(string query)
        {
            var result = await GetStringAsync(query);
            return Guid.TryParse(result, out Guid guid) ? guid : Guid.Empty;
        }
        public async Task<Guid[]> GetGuidsAsync(string query)
        {
            var massive = await GetStringsAsync(query);
            
            // фиг знает сколько может быть возвращено элементов, поэтому создаем массив, а не используем динамические коллекции (экономия памяти и быстродейстродействия)
            var result_massive = new Guid[massive.Length]; 

            for (long i = 0; i < massive.LongLength; i++)
            {
                if (Guid.TryParse(massive[i], out Guid res))
                    result_massive[i] = res;
                else
                {
                    result_massive[i] = Guid.Empty;
                }
            }
            return result_massive;
        }
        public async Task<decimal[]> GetDecimalsAsync(string query)
        {
            var massive = await GetStringsAsync(query);

            // фиг знает сколько может быть возвращено элементов, поэтому создаем массив, а не используем динамические коллекции (экономия памяти и быстродейстродействия)
            var result_massive = new decimal[massive.Length]; 

            for (long i = 0; i < massive.LongLength; i++)
            {
                if (decimal.TryParse(massive[i], NumberStyles.Any, CultureInfo.InvariantCulture, out decimal res))
                    result_massive[i] = res;
                else
                {
                    result_massive[i] = 0;
                }
            }

            return result_massive;
            
        }
    }
}