using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PokerStat.CloudStorageManager.Interface;

namespace PokerStat.CloudStorageManager.Managers
{
    public class YandexCloudStorageManager : IFileManager
    {
        private readonly IConfiguration _config;

        public YandexCloudStorageManager(IConfiguration config, ILogger<object> logger)
        {
            _config = config;
        }

        public Task<IEnumerable<string>> GetNamesOfBlobsAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task DeleteAsync(string name)
        {
            throw new System.NotImplementedException();
        }

        public Task MoveToErrorAsync(Stream stream, string name)
        {
            throw new System.NotImplementedException();
        }

        public Task DownloadToStreamAsync(string name, Stream stream)
        {
            throw new System.NotImplementedException();
        }
    }
}