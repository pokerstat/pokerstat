using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PokerStat.CloudStorageManager.Interface
{
    public interface IFileManager
    {
        /// <summary>
        /// Gets all files from Azure Storage
        /// </summary>
        /// <returns>List of files</returns>
        Task<IEnumerable<string>> GetNamesOfBlobsAsync();
        Task DeleteAsync(string name);
        Task MoveToErrorAsync(Stream stream, string name);
        Task DownloadToStreamAsync(string name, Stream stream);
    }
}