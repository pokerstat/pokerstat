# Install

## NodeJS
 
1. Необходимо установить nodejs, для этого его необходимо [скачать](https://nodejs.org/en/download/). Также можно это сделать через терминал:
	### macOS
	
	```bash
	curl "https://nodejs.org/dist/latest/node-${VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\.pkg</a>.*|\1|p')}.pkg"  >  "$HOME/Downloads/node-latest.pkg"  && sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"
	```
	### windows
	```
	Только средствами Chocolatey
	```
2. Проверить установленные версии, для этого в терминале поочередно ввести команды `node -v` и `npm -v`
	```bash
	ia_samoylov@DESKTOP-ON4KTSC:/mnt/d/$ node -v
	v10.15.3 #последняя версия на текущий момент
	ia_samoylov@DESKTOP-ON4KTSC:/mnt/d/Projects/My/PokerStat$ npm -v
	6.9.0 #последняя версия на текущий момент
	```
## Angular
1. Установить глобально пакет Angular CLI
	```bash
	npm install -g @angular/cli
	```
2. Проверить установленную версию командой `ng v`
	```bash
	ia_samoylov@DESKTOP-ON4KTSC:/mnt/d/$ ng v
	Angular CLI: 7.3.8
	Node: 10.15.3
	OS: linux x64	    
	```
## Подготовка проекта к запуску	
0. **Рекомендация** Открыть проект в VS Code и с использование встроенного термина работать с проектом.
	![vs code](vscode.png)
1. Перейти в каталог с frontend частью проекта `{путь до проекта}/PokerStat/PokerStat`
2. Открыть термина и выполнить команду `npm install`