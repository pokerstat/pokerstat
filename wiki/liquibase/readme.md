# Liquibase]
## Установка

1. Скачать и установить [Java](https://bell-sw.com/pages/java-11.0.3), главное не качать с сайта Oracle, т.к. у них поменялась лицензия и теперь их образ платный.
2. Скачать [liquibase](https://download.liquibase.org/download/?frm=n) 
3. `Не обязательный` Можно задать path в environment системы для liquibase 

## Полезные ресурсы 

1. Доступные команды liquibase: [here](https://www.liquibase.org/documentation/changes/)
2. [Доступные типы](liquibase-types.md)  

## Стуктура проекта

Стуктура проекта liquibase имеет следующий вид:
* Liquibase
	* liquibase.local.properties - конфигурационный файл для локального развертывания
	* liquibase.dev.azure.properties - конфигурационный файл для облачного развертывания
	* drivers - драйвера для liquibase
	* changelog - директория с изменениями базы данных
		* changelog.yaml - точка входа, в него подключаются наши фичи
	
			```yaml	
			databaseChangeLog:
				- include:
					file: 0000-zero-configuration/changelog.yaml
					relativeToChangelogFile: true
			```
		*  {номер задачи}-{имя задачи}
			* data - все изменений данных заносим в эту директорию
			* procedures - все изменений процедур заносим в эту директорию
			* view  - все изменений представлений заносим в эту директорию
			* table - все изменений таблиц заносим в эту директорию
			* changelog.yaml - корневой файл фичи

	![image](folders.png)

## Написание скриптов

Основное требование к написанию миграций, это наличией тега для возможности сделать rollback, в пределах фичи достаточно установки одного тега. установка тега происходит в корневом файл фичи:

![root](root.png)

```yaml
databaseChangeLog:   

  - changeSet:
        id: set rollback tag empty-data-base
        author: IA.Samoylov
        changes:
        - tagDatabase:
            tag: empty-data-base

  - include:
      file: table/changelog.yaml
      relativeToChangelogFile: true

  - include:
      file: procedure/changelog.yaml
      relativeToChangelogFile: true

  - include:
      file: data/changelog.yaml
      relativeToChangelogFile: true

  - changeSet:
        id: set rollback tag 0000
        author: IA.Samoylov
        changes:
        - tagDatabase:
            tag: '0000'
```

- [ ] Также в дальнейшем у нас появятся теги обзаначающие релизы, это
    	будет описано позже


## Запуск миграции

> Важно сначала проверить скрипты на локально базе данных

1. Перейти в директорию с liquibase `{путь до проекта}/PokerStat/Liquibase`
2. Чтобы выполнить обновление базы данных, надо запустить команду:

	```bash
	D:\Projects\My\liquibase-3.6.3-bin\liquibase --defaultsFile={конфигурация} --password={пароль} update
	```
	* {конфигурация} - ./liquibase.local.properties или liquibase.dev.azure.properties
	* {пароль} - пароль пользователь от которого запускается обновление, пользователя можно посмотреть внутри файлов liquibase.local.properties

3. В случае если в рамкой своей фичи, что-то забыли добавить или решили просто поменять. То в первую очередь необходимо сделать rollback скриптов иначе в будет изменена checksum файла, и скрипты перестанут работать без прямого вмешательства в логи liquibase. Обычно хватает сделать rollback по тегу `rollback <tag>` также можно `rollbackCount <value>`. С тегами можно ознакомится в таблицу [liquibase].[DATABASECHANGELOG] в колонке [TAG]

	![image](rollback.png)

	```bash
	sudo bash D:\Projects\My\liquibase-3.6.3-bin\liquibase --defaultsFile={конфигурация} --password={пароль} rollback 0000
	```

	> Фича не должна быть мигрирована на базу в облаке!
4. Также для отладки можно использовать флаг --logLevel=debug 