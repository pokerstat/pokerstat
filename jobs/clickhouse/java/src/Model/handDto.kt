
package Model

import java.math.BigDecimal
import java.util.*

 class handDto {

     lateinit var id: UUID
     lateinit var player_id: UUID
     var stage_id:Short = 0
     var action:Short = 0
     var order:Short = 0
     var position_id:Short = 0
     var typeOfGame:Short = 0
     var typeOfCurrency:Short = 0
     var pokerRoom_id:Short = 0
     var buy_in:BigDecimal? = null
     var is_tournament:Int = 0
     lateinit var date:Date
     var count: BigDecimal? = null
     var card:Int?=null


    override fun hashCode(): Int {
        var hash = id.hashCode()*13+
                player_id.hashCode()*7+
                stage_id.hashCode()*473+
                action.hashCode()*17+
                order.hashCode()*43+
                position_id.hashCode()*13+
                typeOfGame.hashCode()*71+
                typeOfCurrency.hashCode()*5+
                pokerRoom_id.hashCode()*91+
                is_tournament.hashCode()*11+
                date.hashCode()*23
        if (buy_in!=null)
            hash+=buy_in!!.hashCode()*83
        if (count!=null)
            hash+=count!!.hashCode()*111
        if (card!=null)
            hash+=card!!.hashCode()*111
            return hash
    }

    override fun equals(other: Any?): Boolean {
        if (other==null)
            return false
        if (other is handDto)
        {
            return other.hashCode()==hashCode()
        }
        return false
    }
  }