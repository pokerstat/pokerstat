package Providers.Interface

import Model.handDto
import java.util.*

interface IPGProvider {
    fun getHands (is_removed:Boolean, size:Int):Iterable<handDto>
    fun setCompleteMigrate (hand_ids:Iterable<UUID>)
    fun resetCompleteMigrate (hand_ids:Iterable<UUID>)
}
