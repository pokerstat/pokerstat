package Providers.Interface

import Model.handDto

interface ICHProvider {

    fun Add(hands: Iterable<handDto>)

    fun Delete(hands:Iterable<handDto>)

}