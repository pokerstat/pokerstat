package Providers

import Model.handDto
import java.util.*
import kotlin.collections.HashSet


fun getUUIDs(hands: Iterable<handDto>)
            : List<UUID> {
        val dd = HashSet<UUID>()
    hands.forEach{
        if (!dd.contains(it.id))
            dd.add(it.id)
    }
        return dd.toList()
    }

