package Providers

import Model.handDto
import Providers.Interface.ICHProvider
import java.lang.StringBuilder
import java.sql.DriverManager
import java.text.SimpleDateFormat


class CHProvider(connection: String) : ICHProvider {
   private val _connection = connection
    override fun Delete(hands: Iterable<handDto>) {
        if (hands.none()) {
            return
        }
       // throw Throwable()
        val MAX_DELETE_COUNT = 5000 //предел для клик хаус, слишком большая строчка на удаление получается
        val uuids = getUUIDs(hands)
        val sb = StringBuilder()
        for (i in 0 until (uuids.count() / MAX_DELETE_COUNT)+1) {
            sb.clear()
            sb.append("alter table PokerStat_Analyzer.Data delete where hand_id in (")
            if (uuids.count() > MAX_DELETE_COUNT * (i + 1)) {
                for (j in MAX_DELETE_COUNT * i until MAX_DELETE_COUNT * (i + 1)) {
                    sb.append("'").append(uuids[j]).append("'").append(", ")
                }
            } else {
                if (MAX_DELETE_COUNT * i == uuids.count()) {
                    return
                }
                for (j in MAX_DELETE_COUNT * i until uuids.count()) {
                    sb.append("'").append(uuids[j]).append("'").append(", ")
                }
            }
            sb.delete(sb.length - 2, sb.length)
            sb.append(")")
            Execute(sb)
            Thread.sleep(1000)
        }
    }

    override fun Add(hands: Iterable<handDto>) {
        if (hands.none()) return
        Delete(hands)
        val sb =
            StringBuilder("insert into PokerStat_Analyzer.Data (hand_id, player_id, stage_id, \"action\", ordr, position_id, typeofgame, typeofcurrency, pokerroom, buy_in, is_tournamnet, date_action, \"count\", card) values ")
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        hands.forEach {
            sb.appendln()
            sb.append("(")
                .append("'").append(it.id).append("'").append(", ")
                .append("'").append(it.player_id).append("'").append(", ")
                .append(it.stage_id).append(", ")
                .append(it.action).append(", ")
                .append(it.order).append(", ")
                .append(it.position_id).append(", ")
                .append(it.typeOfGame).append(", ")
                .append(it.typeOfCurrency).append(", ")
                .append(it.pokerRoom_id).append(", ")
                .append(if (it.buy_in != null) it.buy_in!! else "null")
                .append(", ")
                .append(it.is_tournament).append(", ")
                .append("'").append(dateFormat.format(it.date)).append("'").append(", ")
                .append(if (it.count != null) it.count!! else "null")
                .append(", ")
                .append(if (it.card != null) it.card!! else "null")
                .append(") ")
        }
        sb.append(";")
        Thread.sleep(1000) // для кликхауса
        Execute(sb)
    }

    private fun Execute(sb: StringBuilder) {
        Class.forName("ru.yandex.clickhouse.ClickHouseDriver")
        val c = DriverManager
            .getConnection(_connection)
        c.autoCommit = true
        val stmt = c.createStatement()
        stmt.execute(sb.toString())
        c.close()
    }
}