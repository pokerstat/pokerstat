package Providers

import Model.handDto
import Providers.Interface.IPGProvider

import java.lang.StringBuilder
import java.math.BigDecimal
import java.sql.DriverManager
import java.util.*
import kotlin.collections.HashSet


class PGProvider(connectinon: String, user: String, password: String):IPGProvider {


    private val _connection = connectinon
    private val _user = user
    private val _password=password

    override  fun getHands(is_removed: Boolean, size:Int):Iterable<handDto> {
        Class.forName("org.postgresql.Driver")
        val c = DriverManager
            .getConnection(_connection,_user, _password)
        c.autoCommit=true

        val stmt = c.createStatement()


        val rs = stmt.executeQuery("SELECT * FROM get_hand(_is_removed:="+is_removed+", _size:="+size+");")
        val list= HashSet<handDto>()
        while (rs.next())
        {
            val hand = handDto()
            hand.id = UUID.fromString(rs.getString(1))
            hand.player_id= UUID.fromString(rs.getString(2))
            hand.stage_id = rs.getShort(3)
            hand.action = rs.getShort(4)
            hand.order = rs.getShort(5)
            hand.position_id = rs.getShort(6)
            hand.typeOfGame = rs.getShort(7)
            hand.typeOfCurrency = rs.getShort(8)
            hand.pokerRoom_id = rs.getShort(9)
            var null_object = rs.getObject(10)
            if (null_object!=null) {
                hand.buy_in=null_object as BigDecimal
            }
            hand.is_tournament=rs.getInt(11)
            hand.date=rs.getDate(12)
            null_object = rs.getObject(13)
            if (null_object!=null) {
                hand.count=null_object as BigDecimal
            }
            null_object = rs.getObject(14)
            if (null_object!=null) {
                hand.card=null_object as Int
            }

            list.add(hand)
        }
        rs.close()
        stmt.close()
        c.close()
        return list

    }

    override fun resetCompleteMigrate(hand_ids: Iterable<UUID>) {
        if (hand_ids.none())
            return
        val sb = StringBuilder("do \$\$ begin perform reset_date_migrate('{")
        hand_ids.forEach { sb.append(" \"").append(it.toString()).append("\",") }
        sb.delete(sb.length-1,sb.length)
        sb.append("}'); end \$\$")
        executeQuery(sb.toString())
    }

     override fun setCompleteMigrate(hand_ids: Iterable<UUID>) {
        if (hand_ids.none())
            return
        val sb = StringBuilder("do \$\$ begin perform set_date_migrate('{")
        hand_ids.forEach { sb.append(" \"").append(it.toString()).append("\",") }
        sb.delete(sb.length-1,sb.length)
        sb.append("}'); end \$\$")
        executeQuery(sb.toString())
    }

    private fun executeQuery(query:String){
        Class.forName("org.postgresql.Driver")
        val c=DriverManager
            .getConnection(_connection,_user, _password)
        c.autoCommit=true
        val stmt = c.createStatement()
        stmt.execute(query)
        stmt.close()
        c.close()
    }
}