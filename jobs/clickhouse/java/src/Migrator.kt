import Model.handDto
import Providers.Interface.*
import Providers.getUUIDs


class Migrator(pgProvider: IPGProvider, chProvider:ICHProvider):IMigrator {
    private val _pgProvider = pgProvider
    private val _chProvider = chProvider

    override fun Run(count:Int) {
       migrate(false,count)
        Thread.sleep(1000)//для клик хаус
        migrate(true,count)
    }

    private fun migrate(is_removed:Boolean, count: Int)
    {
        val hands = _pgProvider.getHands(is_removed,count)
            SaveToCh(is_removed, hands)
            _pgProvider.setCompleteMigrate(getUUIDs(hands))

    }

    private fun SaveToCh(is_removed: Boolean, hands: Iterable<handDto>) {
        try {
            if (is_removed)
                _chProvider.Delete(hands)
            else _chProvider.Add(hands)
        } catch (t: Throwable) {
            _pgProvider.resetCompleteMigrate(getUUIDs(hands))
            throw t
        }
    }
}