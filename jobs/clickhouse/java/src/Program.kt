import Providers.CHProvider
import Providers.Interface.*
import Providers.PGProvider


fun main(args: Array<String>) {
    System.out.println("начали")

    val pgProvider: IPGProvider = PGProvider("jdbc:postgresql://localhost:32771/pokerstat", "appsql", "Sw1233X>22")
    val chProvider: ICHProvider = CHProvider("jdbc:clickhouse://localhost:32770/")
    val migrator: IMigrator = Migrator(pgProvider, chProvider)
    try {
        do {
            migrator.Run(180000)
            Thread.sleep(60000)
        } while (true)
    } catch (t: Throwable) {
        System.err.println(t.message)
    }


    System.out.println("закончили")
}
