create function get_hand(_is_removed boolean, _size int) returns table (id uuid, player_id uuid, stage_id smallint, action smallint, ordr smallint
                                                            , position_id smallint, typeofgame smallint, typeofcurrency smallint
                                                            ,pokerroom smallint, buy_in numeric, is_tournament int, date timestamp, count numeric
                                                            , card smallint
)
    security definer
    RETURNS NULL ON NULL INPUT
    language plpgsql
as
$$
begin
return query
WITH update_id (id) AS
         (
             UPDATE hand_update
                 SET date_begin_migrate=now()
                 where hand_id in(select hand_id from hand_update where is_hand_removed=_is_removed and date_begin_migrate is null and date_migrate is null LIMIT _size)
                 RETURNING hand_id
         )
select h.id,
       poh.player_id,
       hs.stage_id,
       sa.action,
       sa."order",
       poh.position_id,
       pt.type_of_game_id,
       pt.type_of_currency_id,
       pt.poker_room_id,
       t.buy_in,
       case
           when t.id is null then 0
           else 1
           end as is_tournamnet,
       h.date,
       sa.count,
       cpoh.card_id
FROM hand h
         JOIN hand_stage hs ON h.Id = hs.Hand_id and h.is_removed=false
         join poker_table pt on h.table_id = pt.id
         left join tournament t on pt.tournament_id = t.id
         JOIN stage_action sa ON hs.Id = sa.hand_stage_id AND sa.is_removed=false
         JOIN player_of_hand poh ON sa.player_of_hand_id = poh.Id
         left join card_player_of_hand cpoh on poh.id = cpoh.player_of_hand_id
         join update_id hu on h.id = hu.id
         order by h.id,hs.stage_id,sa."order";
end
$$;