GRANT EXECUTE ON FUNCTION public.get_hand (boolean, int) TO appsql;
GRANT EXECUTE on FUNCTION save_hand (uuid,text,timestamp,varchar,uuid) TO appsql;
GRANT EXECUTE ON FUNCTION public.set_hand_to_removed (varchar) TO appsql;
GRANT EXECUTE ON FUNCTION public.set_date_migrate (ids uuid[]) TO appsql;
GRANT EXECUTE ON FUNCTION public.reset_date_migrate (ids uuid[]) TO appsql;
