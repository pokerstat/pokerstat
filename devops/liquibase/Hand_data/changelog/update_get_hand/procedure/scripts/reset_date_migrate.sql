create or replace function reset_date_migrate (ids uuid[]) returns void
as 
$$

        update hand_update
        set date_migrate = null,
        date_begin_migrate =null
        where hand_id = Any(ids);
     $$ LANGUAGE sql  RETURNS NULL ON NULL INPUT  SECURITY DEFINER; 