create or replace function public.set_hand_to_removed
   (code varchar(40)) RETURNS void
AS $$ begin
    WITH deleted_id (id) AS
     (
        UPDATE hand
        SET is_removed=true
        WHERE hashcode=code
        RETURNING id
     )
    INSERT INTO hand_update (hand_id,date_create,is_hand_removed)
    select id,  now(), true
    from deleted_id 
    ON CONFLICT (hand_id) DO UPDATE SET is_hand_removed = true, date_create=now(), date_begin_migrate=null,date_migrate=null;
    END $$ LANGUAGE plpgsql  RETURNS NULL ON NULL INPUT  SECURITY DEFINER;  
    