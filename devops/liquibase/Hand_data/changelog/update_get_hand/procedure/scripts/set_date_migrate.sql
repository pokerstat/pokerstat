create or replace function set_date_migrate (ids uuid[]) returns void
as 
$$

        update hand_update
        set date_migrate = now()
        where hand_id = Any(ids) and date_begin_migrate is not null;
     $$ LANGUAGE sql  RETURNS NULL ON NULL INPUT  SECURITY DEFINER; 