CREATE function public.save_stage_action(
    _id uuid,
    _action smallint,
    _playerhand_id uuid,
    _handstage_id uuid,
    _order smallint, 
    _count numeric(17,2) = null) RETURNS void
AS $$ begin
    insert into stage_action (id, player_of_hand_id,hand_stage_id,"action",count,"order")
    VALUES                  (_id,_playerhand_id,_handstage_id,_action,_count,_order);
 end $$ LANGUAGE plpgsql   SECURITY DEFINER; 
 