CREATE function public.get_stage_card
    (model_id uuid) RETURNS table(id SMALLINT)
AS $$
    begin
        return query select card_id
        from hand_stage_card
        where hand_stage_id=model_id;
    END $$ LANGUAGE plpgsql
    RETURNS NULL ON NULL INPUT
      SECURITY DEFINER;  
    