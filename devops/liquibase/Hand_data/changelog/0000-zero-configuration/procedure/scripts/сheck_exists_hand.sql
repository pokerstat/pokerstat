CREATE function public.сheck_exists_hand ( code varchar(40)) returns bigint
AS  
 $$
    declare _count bigint;
begin
    if code is null then
        return 0;
    END IF;
    select Count(*) into _count
    from hand h
    join hand_stage hs on hs.hand_id=h.id
    where h.hashcode=code and h.is_removed=false;
    return _count;
end
 $$ LANGUAGE plpgsql
 SECURITY DEFINER; 
 