CREATE function public.save_stage_hand (
    _id uuid,
    _stage smallint,
    _hand_id uuid) RETURNS void
AS $$ begin
    insert into hand_stage (id, hand_id,stage_id)
    VALUES                 (_id,_hand_id,_stage);
 end $$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT SECURITY DEFINER; 
 