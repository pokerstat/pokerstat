CREATE function public.get_stage_actions
    (_hand_id uuid) returns setof stage_action
AS
  $$
  begin
      return query select sa.id,sa.action,sa.count,sa.player_of_hand_id
        from stage_action sa
        join hand_stage hs on hs.id=sa.hand_stage_id
        where sa.is_removed=0 and hs.hand_id=_hand_id;
    END $$ LANGUAGE plpgsql
    RETURNS NULL ON NULL INPUT
      SECURITY DEFINER;  
    