CREATE OR replace function public.save_tournament
    (_id uuid,
    _identifier text,
    _buy_in numeric(17,2)
    ) returns uuid
AS $$
    declare __id uuid;
    declare _lower text:=lower(_identifier);
BEGIN

    insert into tournament (id,identifier, identifier_lower,buy_in)
    VALUES                 (_id, _identifier,_lower,_buy_in)
    ON CONFLICT (identifier_lower, buy_in) do NOTHING;

    SELECT id into __id
    from tournament
    where identifier_lower=_lower AND buy_in = _buy_in
    limit 1;
    return __id; 
END $$ LANGUAGE plpgsql   SECURITY DEFINER;  
