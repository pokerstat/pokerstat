CREATE function public.save_player
    (_id uuid,
    _nick text,
    _pokerroom_id SMALLINT) returns uuid
AS $$ 
    declare __id uuid;
    declare _lower text:=lower(_nick);
BEGIN
    insert into player (id,nick, nick_lower, poker_room_id)
    VALUES             (_id, _nick,_lower,_pokerroom_id)
    ON CONFLICT (nick_lower, poker_room_id) do NOTHING;

    SELECT id into __id
    from player
    where nick_lower=_lower and poker_room_id=_pokerroom_id
    limit 1;
    return __id;
END $$ LANGUAGE plpgsql  SECURITY DEFINER;  
