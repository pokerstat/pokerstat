CREATE function public.get_card () returns TABLE (id smallint, c_name varchar(5), c_short_name char, s_short_name char, s_name VARCHAR(8), color INTEGER, alt_color INTEGER) 
AS $$
    begin
       return query  SELECT c.id,
                cn.name,
                cn.short_name,
                s.short_name,
                s.name, 
                col.color,
                col.alternative_color
         from card c
         join card_name cn on cn.id=c.card_name_id
         join color col on c.color_id=col.id
         left join suit s on s.id=c.suit_id;
  end $$ LANGUAGE plpgsql
  IMMUTABLE 
  SECURITY DEFINER; 
  