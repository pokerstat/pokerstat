
CREATE function public.get_hand_card 
    (model_id uuid) returns table (id uuid)
AS
   $$ 
    begin
    return query
        select card_id
        from card_player_of_hand
        where player_of_hand_id=model_id;
end $$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT
  SECURITY DEFINER; 
 