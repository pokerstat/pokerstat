CREATE function public.get_count_of_hand_card
    (code text) returns  table (nick text, id uuid, count bigint)
AS
$$ 
    begin    
   return query SELECT p.nick_lower,p.id, Count(*)
    FROM player_of_hand poh
    join player p on p.id=poh.player_id
    join card_player_of_hand c on c.player_of_hand_id=poh.id
    JOIN hand h on h.id=poh.hand_id
    where h.is_removed=false and h.hashcode=code
    GROUP by p.nick,p.id;
end $$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT
  SECURITY DEFINER; 
