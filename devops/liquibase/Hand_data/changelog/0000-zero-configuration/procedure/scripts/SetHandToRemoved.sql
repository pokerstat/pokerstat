CREATE function public.set_hand_to_removed
    (code varchar(40)) RETURNS void
AS $$ begin
        UPDATE hand
        SET is_removed=true
        WHERE hashcode=code;
    END $$ LANGUAGE plpgsql  RETURNS NULL ON NULL INPUT SECURITY DEFINER; 
    