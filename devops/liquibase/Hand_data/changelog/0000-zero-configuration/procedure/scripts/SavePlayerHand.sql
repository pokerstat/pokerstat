CREATE function public.save_player_hand(
    _id uuid,
    _countchips numeric(17,2),
    _position smallint,
    _player_id uuid,
    _hand_id uuid) RETURNS void
AS $$ 

    insert into player_of_hand (id, player_id,hand_id,position_id,count)
    VALUES                   (_id,_player_id,_hand_id,_position,_countchips);
 $$ LANGUAGE sql  SECURITY DEFINER; 
 