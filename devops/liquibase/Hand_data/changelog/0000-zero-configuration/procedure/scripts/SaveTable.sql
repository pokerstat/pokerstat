
CREATE OR replace function public.save_table
    (_id uuid,
    _identifier text,
    _typeofcurrency smallint,
    _type_of_game SMALLINT,
    _poker_room_id SMALLINT,
    _tournament_id uuid=NULL) RETURNS uuid
AS $$
    declare __id uuid;
    declare _lower text:=lower(_identifier);
BEGIN
    insert into poker_table (id, identifier,identifier_lower,tournament_id,type_of_currency_id,type_of_game_id,poker_room_id)
    VALUES             (_id,_identifier,_lower,_tournament_id,_typeofcurrency,_type_of_game,_poker_room_id)
    ON CONFLICT (identifier_lower, COALESCE(tournament_id, '00000000-0000-0000-0000-000000000000'::uuid), poker_room_id, type_of_game_id) DO NOTHING;

    sELECT id into __id
    from poker_table
    where identifier_lower=_lower and coalesce(tournament_id,'00000000-0000-0000-0000-000000000000') = coalesce(_tournament_id,'00000000-0000-0000-0000-000000000000')
    and poker_room_id = _poker_room_id and type_of_game_id=_type_of_game
    limit 1;
    return __id;
END $$ LANGUAGE plpgsql   SECURITY DEFINER; 
