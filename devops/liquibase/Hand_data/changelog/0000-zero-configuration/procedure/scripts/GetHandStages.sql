CREATE function public.get_hand_stages
    (hand_id uuid) returns setof hand_stage
AS $$
    begin
    return query
        SELECT hs.id,hs.stage_id
    from hand_stage hs
    join hand h on h.id=hs.hand_id
    where h.is_removed=0 and h.id=hand_id;
end $$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT
  SECURITY DEFINER;  
