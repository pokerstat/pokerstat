CREATE function public.save_hand 
    (_id uuid,
    _identifier text,
    _date timestamp,
    _hashcode VARCHAR(40),
    _table_id uuid)
    RETURNS void
AS $$ begin
    INSERT into hand (id, identifier, identifier_lower, date, table_id, hashcode)
    VALUES      (_id, _identifier,lower(_identifier),_date,_table_id,_hashcode);
 end $$ LANGUAGE plpgsql
 RETURNS NULL ON NULL INPUT
   SECURITY DEFINER;  
 