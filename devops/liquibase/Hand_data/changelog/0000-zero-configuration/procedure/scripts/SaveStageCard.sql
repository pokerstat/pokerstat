CREATE function public.save_stage_card
    (_id uuid,
    _model_id uuid,
    _card_id smallint) RETURNS void
AS $$ begin
    insert into hand_stage_card (id, hand_stage_id,card_id)
    VALUES                    (_id,_model_id,_card_id);
 end $$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT SECURITY DEFINER;  
 