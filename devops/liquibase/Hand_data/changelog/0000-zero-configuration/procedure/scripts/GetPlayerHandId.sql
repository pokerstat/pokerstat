CREATE function public.get_player_hand_id
    (code varchar(40),
    player_id uuid) RETURNS uuid
AS
$$ 
    declare result uuid; 
    begin
   SELECT  poh.id into  result
    from player_of_hand  poh
        JOIN hand h on h.id=poh.hand_id
    where poh.player_id=player_id and h.is_removed=0 and h.hashcode=code
    limit 1;
return result;
end $$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT
  SECURITY DEFINER; 
