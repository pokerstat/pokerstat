
GRANT EXECUTE on function сheck_exists_hand (varchar) TO appsql;

GRANT EXECUTE on function get_card() TO appsql;

GRANT EXECUTE on function get_count_of_hand_card (text)  TO appsql;

GRANT EXECUTE on function get_hand_card (uuid) TO appsql;

GRANT EXECUTE on function get_hand_stages (uuid) TO appsql;

GRANT EXECUTE on function get_player_hand_id(varchar,uuid) TO appsql;

GRANT EXECUTE on function get_stage_actions(uuid) TO appsql;

GRANT EXECUTE on function get_stage_card (uuid) TO appsql;

GRANT EXECUTE on FUNCTION save_hand  (uuid,text,timestamp,varchar,uuid)TO appsql;

GRANT EXECUTE on FUNCTION save_hand_card (uuid,uuid,SMALLINT) TO appsql;

GRANT EXECUTE on function save_player (uuid,text,SMALLINT) TO appsql;

GRANT EXECUTE on FUNCTION save_player_hand(uuid,numeric,smallint,uuid,uuid) TO appsql;

GRANT EXECUTE on function save_poker_room (text)TO appsql;

GRANT EXECUTE on FUNCTION save_stage_action (uuid,smallint,uuid,uuid,smallint, numeric) TO appsql;

GRANT EXECUTE on FUNCTION save_stage_card(uuid,uuid,SMALLINT) TO appsql;

GRANT EXECUTE on FUNCTION save_stage_hand(uuid,SMALLINT,uuid) TO appsql;

GRANT EXECUTE on function save_table (uuid,text,smallint,SMALLINT,SMALLINT,uuid) TO appsql;

GRANT EXECUTE on function save_tournament (uuid,text,numeric) TO appsql;

GRANT EXECUTE on FUNCTION set_hand_to_removed(varchar) TO appsql;

 