
CREATE function public.save_poker_room 
    (_name text) returns smallint
AS $$
    declare _id smallint;
BEGIN
    insert into poker_room (name)
    VALUES             (_name)
    ON CONFLICT (name) do NOTHING;
    select  setval('poker_room_id_seq', (select max(id)from poker_room)+1,false);
    SELECT id into  _id
    from poker_room
    where name=_name
    limit 1;
    return _id;
END $$ LANGUAGE plpgsql  RETURNS NULL ON NULL INPUT SECURITY DEFINER; 
