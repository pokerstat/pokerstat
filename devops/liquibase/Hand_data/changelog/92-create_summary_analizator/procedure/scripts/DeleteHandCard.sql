CREATE or replace function public.delete_hand_card 
    (_model_id uuid) RETURNS void
AS $$ begin
    delete 
    from card_player_of_hand
    where player_of_hand_id=_model_id;
END $$ LANGUAGE plpgsql   RETURNS NULL ON NULL INPUT SECURITY DEFINER;  
