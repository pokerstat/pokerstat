CREATE function public.get_poker_rooms () returns  SETOF poker_room
     AS
     $$
    begin
    return query select id,
                name
       from poker_room;
    end;
   $$ LANGUAGE plpgsql   SECURITY DEFINER; 

