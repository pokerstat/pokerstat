create or replace function public.save_hand
     (_id uuid,
    _identifier text,
    _date timestamp,
    _hashcode VARCHAR(40),
    _table_id uuid)
    RETURNS void
AS $$ begin

    INSERT into hand (id, identifier,identifier_lower, date, table_id, hashcode)
    VALUES      (_id, _identifier,lower(_identifier),_date,_table_id,_hashcode);
    INSERT INTO hand_update (hand_id,date_create,is_hand_removed)
    VALUES ( _id,  now(), false)
    ON CONFLICT (hand_id) DO UPDATE SET is_hand_removed = false, date_create=now(), date_migrate = null;
    end
 $$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT SECURITY DEFINER;  
 