create function get_hand(_is_removed boolean) returns table (hand_id uuid, player_id uuid, action smallint, ordr smallint
                                                            , position_id smallint, typeofgame smallint, typeofcurrency smallint
                                                            ,pokerroom smallint, buy_in numeric, date timestamp, count numeric
                                                            , card smallint
)
    security definer
    language plpgsql
as
$$
begin
return query
select h.id,
       poh.player_id, 
       hs.stage_id, 
       sa.action, 
       sa."order", 
       poh.position_id, 
       pt.type_of_game_id, 
       pt.type_of_currency_id,
       pt.poker_room_id, 
       t.buy_in,h.date, 
       sa.count,
       cpoh.card_id
FROM hand h 
JOIN hand_stage hs ON h.Id = hs.Hand_id and h.is_removed=false 
join poker_table pt on h.table_id = pt.id
left join tournament t on pt.tournament_id = t.id
JOIN stage_action sa ON hs.Id = sa.hand_stage_id AND sa.is_removed=false 
JOIN player_of_hand poh ON sa.player_of_hand_id = poh.Id
left join card_player_of_hand cpoh on poh.id = cpoh.player_of_hand_id
join hand_update hu on h.id = hu.hand_id
where is_hand_removed=_is_removed and date_migrate is null;
end
$$;