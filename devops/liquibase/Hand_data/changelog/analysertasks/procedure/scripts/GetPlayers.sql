﻿CREATE function public.get_players 
  (_nick text) returns table (id uuid, nick text, poker_room smallint)
AS $$ 
Begin
return query SELECT p.id
      ,p.nick
      ,p.poker_room_id
FROM player p
WHERE p.nick_lower like '%'|| lower(rtrim(ltrim(_nick)))|| '%';
end $$ LANGUAGE plpgsql   RETURNS NULL ON NULL INPUT SECURITY DEFINER; 

