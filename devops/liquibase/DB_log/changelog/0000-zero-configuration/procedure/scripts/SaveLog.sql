
CREATE OR replace function public.save_log
    (_id uuid,
    _log_type smallint,
    _module_name text,
    _text_log text,
    _date_log timestamp,
    _stack_trace text = NULL,
    _class_name text=NULL,
    _number_of_thread smallint = null
    ) RETURNS void
AS $$
    declare _class_id uuid=null;
    declare _module_id uuid;
BEGIN
--  сохраняем новый модуль
    WITH module_ids (id) AS
    (
        insert into log_module (name)
        VALUES             (_module_name)
        ON CONFLICT (name) DO NOTHING
        RETURNING id
    )
    sELECT id into _module_id -- записываем в переменную
    from module_ids
    limit 1;

    select  setval('log_module_id_seq', (select max(id)from poker_room)+1,false); --востанавливаем счетчик

    if _class_name is not null and _class_name<>'' then
        WITH class_ids (id) AS
        (
            insert into logging_class (name)
            VALUES             (_class_name)
            ON CONFLICT (name) DO NOTHING
            RETURNING id
        )
        sELECT id into _class_id -- записываем в переменную
        from class_ids
        limit 1;

        select  setval('logging_class_id_seq', (select max(id)from poker_room)+1,false); --востанавливаем счетчик

    end if;

    insert into log (id, log_type_id, number_of_thread, class_id, module_id, date_log, text_log, stack_trace)
    values          (_id,_log_type,   _number_of_thread,_class_id,_module_id,_date_log,_text_log, _stack_trace);
END $$ LANGUAGE plpgsql   SECURITY DEFINER; 
