#!/bin/bash

while getopts p:i:d: option
do
case "${option}" 
in 
 	p) p=${OPTARG};; # sudo password
    i) ip=${OPTARG};; # adress
    d) dir=${OPTARG};;
esac
done
# Write your commands here

echo "Debug arguments:"
echo "------------------------"
echo "IP: $ip"  
echo "Directory: $dir"  
echo "Password: $p" | sed -r 's/(\s|\d|\S|\D){6}$/******/g'
echo "------------------------"

ssh pf@$ip << EOF
	echo "$p" | sudo -S systemctl stop parsefile
	cd parsefile
	rm -rf *
EOF
scp -r $dir/* pf@$ip:~/parsefile/
ssh pf@$ip
echo "$p" | sudo -S systemctl start parsefile
exit

# Use the environment variables input below to pass secret variables to this script