﻿#!/bin/bash
while getopts v: option
do
case "${option}"
in v) v=${OPTARG};;
esac
done

echo "Debug arguments:"
echo "------------------------"
echo "Version:$v" # 7.2.2
echo "------------------------"


version="mssql-jdbc-$v.jre8.jar"
relese="v$v"
destination_directory_root="./../../Liquibase/soft/"
destination_directory_sql_driver="sql_driver/"
destination_directory="$destination_directory_root$destination_directory_sql_driver" 

echo "check if a driver [$destination_directory$version] doesn't exsist"

if [ ! -e "$destination_directory$version" ]; then

	echo "check if a directory [$destination_directory] doesn't exsist"
	if [ ! -d "$destination_directory" ]; then
		echo "Creating directory [$destination_directory]"
		mkdir -p "$destination_directory"
	fi

	echo "Downloading $version to $destination_directory"
	wget "https://github.com/Microsoft/mssql-jdbc/releases/download/$relese/$version" -P "$destination_directory"
else
	echo "a driver [$version] exists, downloading is skipped"
fi