#!/bin/bash
while getopts v: option
do
case "${option}" 
in v) v=${OPTARG};;
esac
done

echo "Debug arguments:"
echo "------------------------"
echo "Version: $v" #3.6.3
echo "------------------------"

version="liquibase-$v-bin.tar.gz"
release="liquibase-parent-$v"
unzip_directory_root="./../../Liquibase/soft/"
unzip_directory_liquibase="liquibase/"
unzip_directory="$unzip_directory_root$unzip_directory_liquibase"

echo "Downloading $version"
wget "https://github.com/liquibase/liquibase/releases/download/$release/$version"

echo "check if a directory [$unzip_directory] doesn't exsist"
if [ ! -d "$unzip_directory" ]; then
	echo "Creating directory [$unzip_directory]"
	mkdir -p "$unzip_directory"
fi

echo "Unzip $version to $unzip_directory"
tar -xf "$version" --directory "$unzip_directory"
rm -f "$version"