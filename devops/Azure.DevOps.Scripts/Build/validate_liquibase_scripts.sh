﻿#!/bin/bash
while getopts v:d:p:s: option
do
case "${option}" 
in 
	v) v=${OPTARG};;
 	d) db=${OPTARG};;
 	p) p=${OPTARG};;
 	s) s=${OPTARG};;
esac
done

echo "Debug arguments:"
echo "------------------------"
echo "Version: $v" #7.2.2
echo "Server: $s"  
echo "Database: $db"  
echo "Password: $p" | sed -r 's/(\s|\d|\S|\D){6}$/******/g'
echo "------------------------"

classpath="./soft/sql_driver/mssql-jdbc-$v.jre8.jar"
driver="com.microsoft.sqlserver.jdbc.SQLServerDriver"
url="jdbc:sqlserver://$s;database=$db"
changeLogFile="changelog/changelog.yaml"
username="liquibaseuser"
defaultSchemaName="liquibase"

./soft/liquibase/liquibase \
      --driver=$driver \
      --classpath=$classpath \
      --changeLogFile=$changeLogFile \
      --url=$url \
      --username=$username \
      --password=$p \
      validate

rm -rf ./soft