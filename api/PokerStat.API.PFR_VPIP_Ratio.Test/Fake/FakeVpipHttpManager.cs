﻿
using System.Threading.Tasks;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.PFR_VPIP_Ratio.Test.Fake
{
    public class FakeVpipHttpManager : IHttpManager
    {
        public async Task<decimal> SendQueryAsync(AnalyzerWithDateRequest request)
        {
           return await Task.FromResult(10);
        }

        public async Task<decimal> SendQueryAsync(AnalyzerRequest request)
        {
            return await Task.FromResult(20);
        }
    }
}
