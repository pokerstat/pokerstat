﻿using System;
using System.Threading.Tasks;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.PFR_VPIP_Ratio.Test.Fake
{
    public class FakePFRHttpManager:IHttpManager
    {
        public async Task<decimal> SendQueryAsync(AnalyzerWithDateRequest request)
        {
            return await Task.FromResult(5);
        }

        public async Task<decimal> SendQueryAsync(AnalyzerRequest request)
        {
            return await Task.FromResult(4);
        }
    }
}
