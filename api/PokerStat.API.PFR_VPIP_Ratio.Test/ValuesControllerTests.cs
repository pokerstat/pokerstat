using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.AnalyzerHttpHelper.Validators;
using PokerStat.Api.PFR_VPIP_Ratio.Controllers;
using PokerStat.Api.PFR_VPIP_Ratio.Factorys;
using PokerStat.API.PFR_VPIP_Ratio.Test.Fake;

namespace Tests
{
    [TestFixture]
    public class ValuesControllerTests
    {
		private IAnalyzerController _analyzerController;
        private IHttpManagerFactory httpManagerFactory;
        AnalyzerWithDateRequest awdr;
		[SetUp]
		public void Setup()
		{
			awdr = new AnalyzerWithDateRequest { Player_id = Guid.NewGuid().ToString(), PokerRoom = "0", IsTournamnet = "1", TypeOfGame = "1", DateEnd = "2000-04-18 00:00:00", DateStart = "2000-02-18 00:00:00" };
            Mock<IHttpManagerFactory> stub_hmf = new Mock<IHttpManagerFactory>();
            stub_hmf.Setup(hmf => hmf.PFRManager)
                .Returns(new FakePFRHttpManager());
            stub_hmf.Setup(hmf => hmf.VPIPManager)
              .Returns(new FakeVpipHttpManager());
            httpManagerFactory = stub_hmf.Object;
        }

		[Test]
        public async Task Should_0_2()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerWithDateRequestValidator)))
                .Returns(new AnalyzerWithDateRequestValidator());
            _analyzerController = new ValuesController(stub_vf.Object, httpManagerFactory);
            var res = await _analyzerController.GetWithDateAsync(awdr);
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult)res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse)ok.Value;
                Assert.AreEqual((decimal)5/10, response.Data);
            });
        }
        [Test]
        public async Task Should_0_25()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerRequestValidator)))
                .Returns(new AnalyzerRequestValidator());
            _analyzerController = new ValuesController(stub_vf.Object, httpManagerFactory);
            var res = await _analyzerController.GetAsync(awdr);
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult)res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse)ok.Value;
                Assert.AreEqual((decimal)4 / 20, response.Data);
            });
        }
    }
}