using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.AnalyzerHttpHelper.Validators;
using PokerStat.API.ContBet.Controllers;
using PokerStat.API.ContBet.Test.Fake;

namespace Tests
{
    [TestFixture]
    public class ValuesControllerTests
    {
        private IAnalyzerController _analyzerController;
        AnalyzerWithDateRequest awdr;
        [SetUp]
        public void Setup()
        {
            awdr = new AnalyzerWithDateRequest { Player_id = "f6ec47f1-e391-41d1-89a0-7936ea7a2991", PokerRoom = "0", IsTournamnet = "1", TypeOfGame = "1", DateEnd = "2000-04-18 00:00:00", DateStart = "2000-02-18 00:00:00" };
        }

        [Test]
        public async Task Should_0_2()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerWithDateRequestValidator)))
                .Returns(new AnalyzerWithDateRequestValidator());
            _analyzerController = new ValuesController(stub_vf.Object, new FakeHttpManager(), new FakeClickHouse());
            var res = await _analyzerController.GetWithDateAsync(awdr);
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult)res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse)ok.Value;
                Assert.AreEqual((decimal)2 / 10, response.Data);
            });
        }
        [Test]
        public async Task Should_0_1()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerRequestValidator)))
                .Returns(new AnalyzerRequestValidator());
            _analyzerController = new ValuesController(stub_vf.Object, new FakeHttpManager(), new FakeClickHouse());
            var res = await _analyzerController.GetAsync(awdr);
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult)res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse)ok.Value;
                Assert.AreEqual((decimal)2 / 20, response.Data);
            });
        }
    }
}