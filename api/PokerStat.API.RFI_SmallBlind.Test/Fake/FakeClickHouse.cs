﻿using System;
using System.Threading.Tasks;
using PokerStat.ClickHouseManager;

namespace PokerStat.API.RFI_SmallBlind.Test.Fake
{
    public class FakeClickHouse : IClickHouseManager
    {
        public Task<decimal> GetDecimalAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<decimal[]> GetDecimalsAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> GetGuidAsync(string query)
        {
            throw new NotImplementedException();
        }

        public async Task<Guid[]> GetGuidsAsync(string query)
        {
            if (query ==
                "select distinct hand_id from PokerStat_Analyzer.Data where player_id='f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet=1 and typeofgame = 1 and pokerroom= 0 and position_id=9 and bitAnd(action,2)=2 and stage_id=2;"
                || query ==
                "select distinct hand_id from PokerStat_Analyzer.Data where player_id='f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet=1 and typeofgame = 1 and pokerroom= 0 and date_action BETWEEN toDate('2000-02-18 00:00:00') and toDate('2000-04-18 00:00:00') and position_id=9 and bitAnd(action,2)=2 and stage_id=2;"
            )
                return await Task.FromResult(new[] {new Guid("f6ec47f1-e391-41d1-89a0-7936ea7a2992"), new Guid("f6ec47f1-e391-41d1-89a0-7936ea7a2992")});
            throw new ArgumentException("wrong query", nameof(query));
        }

        public async Task<int> GetIntAsync(string query)
        {
            if (query == "select action from PokerStat_Analyzer.Data where hand_id = 'f6ec47f1-e391-41d1-89a0-7936ea7a2992' and stage_id =2 and player_id = 'f6ec47f1-e391-41d1-89a0-7936ea7a2991' order by ordr limit 1;")
                return await Task.FromResult(514);
            throw new ArgumentException("wrong query", nameof(query));
        }

        public Task<int[]> GetIntsAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetStringAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<string[]> GetStringsAsync(string query)
        {
            throw new NotImplementedException();
        }
    }
}