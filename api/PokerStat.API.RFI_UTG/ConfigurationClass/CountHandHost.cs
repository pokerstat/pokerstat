﻿using System;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.RFI_UTG.ConfigurationClass
{
    public class CountHandHost:IHttpHost
    {
        public string Host { get; set; }
    }
}
