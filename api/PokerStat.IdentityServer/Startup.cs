﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PokerStat.IdentityServer.Extensions;
using PokerStat.SerilogConfiguration.Middleware;

namespace PokerStat.IdentityServer
{
  public class Startup
  {
    private readonly IHostingEnvironment _env;
    private readonly IConfiguration _configuration;

    public Startup(IHostingEnvironment env)
    {
      _env = env;
      _configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
        .Build();
    }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1);
      services
        .RegisterDependencies(_env, _configuration)
        .AddIdentity(_env, _configuration);
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseDatabaseErrorPage();
      }


      app.UseStaticFiles();
      app.UseMiddleware<SerilogMiddleware>();
      app.UseIdentityServer();
      app.UseMvcWithDefaultRoute();
    }
  }
}
