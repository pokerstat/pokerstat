using System.Reflection;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PokerStat.Configuration.ConfigSection;
using PokerStat.Configuration.Extensions;
using PokerStat.IdentityServer.AuthContext;
using PokerStat.IdentityServer.AuthContext.DTO;

namespace PokerStat.IdentityServer.Extensions
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection RegisterDependencies(this IServiceCollection services, IHostingEnvironment env, IConfiguration configuration)
    {
      services
        .AddSingleton(env)
        .AddSingleton(configuration);

      return services;
    }

    public static IServiceCollection AddIdentity(this IServiceCollection services, IHostingEnvironment env, IConfiguration configuration)
    {
      var cn = configuration.BindTo<IdentityServerConfiguration>().ConnectionString;
      var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

      services.AddDbContext<AuthDataBaseContext>(options => options.UseNpgsql(cn, sql => sql.MigrationsAssembly(migrationsAssembly)));

      //https://stackoverflow.com/questions/42787120/how-to-use-identityserver4-with-custom-password-validation-with-asp-net-microsof
      //custom password validator
      services.AddIdentity<PokerStatUser, IdentityRole>(options =>
        {
          options.Password.RequireDigit = true;
          options.Password.RequireLowercase = true;
          options.Password.RequireNonAlphanumeric = false;
          options.Password.RequireUppercase = true;
          options.Password.RequiredLength = 8;
        })
        .AddEntityFrameworkStores<AuthDataBaseContext>()
        .AddDefaultTokenProviders();

      var builder = services.AddIdentityServer(options =>
        {
          options.Events.RaiseErrorEvents = true;
          options.Events.RaiseInformationEvents = true;
          options.Events.RaiseFailureEvents = true;
          options.Events.RaiseSuccessEvents = true;
        })
        .AddAspNetIdentity<PokerStatUser>()
        .AddConfigurationStore(options => { options.ConfigureDbContext = b => b.UseNpgsql(cn, sql => sql.MigrationsAssembly(migrationsAssembly)); })
        .AddOperationalStore(options => { options.ConfigureDbContext = b => b.UseNpgsql(cn, sql => sql.MigrationsAssembly(migrationsAssembly));
          // options.EnableTokenCleanup = true;
          // options.TokenCleanupInterval = 15; // frequency in seconds to cleanup stale grants. 15 is useful during debugging
        });

      builder.AddSigningCredential(new RsaSecurityKey(new RSACryptoServiceProvider(2048)));

      return services;
    }
  }
}
