using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PokerStat.Configuration.ConfigSection;
using PokerStat.Configuration.Extensions;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;
using PokerStat.IdentityServer.AuthContext;
using PokerStat.IdentityServer.AuthContext.DTO;

namespace PokerStat.IdentityServer.Extensions
{
  public static class WebHostExtensions
  {
    public static IWebHost Migrate(this IWebHost webHost)
    {
      using (var scope = webHost.Services.GetService<IServiceScopeFactory>().CreateScope())
      {
        var serviceProvider = scope.ServiceProvider;
        var configuration = serviceProvider.GetService<IConfiguration>();
        var authContext = serviceProvider.GetRequiredService<AuthDataBaseContext>();
        var configurationDbContext = serviceProvider.GetRequiredService<ConfigurationDbContext>();
        var persistedGrantDbContext = serviceProvider.GetRequiredService<PersistedGrantDbContext>();
        var identityServerConfiguration = configuration.BindTo<IdentityServerConfiguration>();

        if (identityServerConfiguration.ForceMigrate)
        {
          authContext.Database.EnsureDeleted();
        }

        authContext.Database.Migrate();
        configurationDbContext.Database.Migrate();
        persistedGrantDbContext.Database.Migrate();

        if (!EnumerableExtensions.Any(configurationDbContext.Clients) || identityServerConfiguration.ForceMigrate)
        {
          foreach (var client in IdentityServerConfig.GetClients(configuration))
          {
            configurationDbContext.Clients.Add(client.ToEntity());
          }

          configurationDbContext.SaveChanges();
        }

        if (!EnumerableExtensions.Any(configurationDbContext.IdentityResources) || identityServerConfiguration.ForceMigrate)
        {
          foreach (var resource in IdentityServerConfig.GetIdentityResources())
          {
            configurationDbContext.IdentityResources.Add(resource.ToEntity());
          }

          configurationDbContext.SaveChanges();
        }

        if (!EnumerableExtensions.Any(configurationDbContext.ApiResources) || identityServerConfiguration.ForceMigrate)
        {
          foreach (var resource in IdentityServerConfig.GetApiResources())
          {
            configurationDbContext.ApiResources.Add(resource.ToEntity());
          }

          configurationDbContext.SaveChanges();
        }


        var userManager = serviceProvider.GetRequiredService<UserManager<PokerStatUser>>();

        var admin = userManager.FindByNameAsync(identityServerConfiguration.AdminLogin).Result;

        if (admin != null)
          return webHost;

        admin = new PokerStatUser()
        {
          UserName = identityServerConfiguration.AdminLogin
        };

        var result = userManager.CreateAsync(admin, identityServerConfiguration.AdminPassword).Result;
        if (!result.Succeeded)
        {
          throw new PokerStatException(new PokerStatError(PokerStatErrorCodes.DataBaseInitializer)
          {
            Messages = result.Errors.Select(er => new ErrorMessage()
            {
              Key = er.Code,
              Value = er.Description
            }).ToArray()
          });
        }

        admin = userManager.FindByNameAsync(identityServerConfiguration.AdminLogin).Result;

        result = userManager.AddClaimsAsync(admin, new[]
        {
          new Claim(JwtClaimTypes.Name, "Admin"),
          new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean)
        }).Result;

        if (!result.Succeeded)
        {
          throw new PokerStatException(new PokerStatError(PokerStatErrorCodes.DataBaseInitializer)
          {
            Messages = result.Errors.Select(er => new ErrorMessage()
            {
              Key = er.Code,
              Value = er.Description
            }).ToArray()
          });
        }
      }

      return webHost;
    }
  }
}
