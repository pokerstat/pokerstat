﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using PokerStat.IdentityServer.Extensions;
using PokerStat.SerilogConfiguration.Extensions;

namespace PokerStat.IdentityServer
{
    public sealed class Program
    {
        public static void Main(string[] args) => CreateWebHostBuilder(args)
          .Build()
          .Migrate()
          .Run();

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureSerilog("PokerStat.IdentityServer");
    }
}
