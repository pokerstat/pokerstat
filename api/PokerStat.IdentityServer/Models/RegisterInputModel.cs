using System.ComponentModel.DataAnnotations;

namespace PokerStat.IdentityServer.Models
{
  public class RegisterInputModel
  {
    [Required] public string Username { get; set; }
    [Required] public string Password { get; set; }
    [Required]
    [Compare(nameof(Password))]
    public string AgainPassword { get; set; }
    [Required] public string Email { get; set; }

    public string ReturnUrl { get; set; }
  }
}
