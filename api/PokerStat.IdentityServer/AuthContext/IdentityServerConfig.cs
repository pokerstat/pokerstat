//TODO need to remove when we'll add admin UI

using System.Collections.Generic;
using System.Linq;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using PokerStat.Configuration.ConfigSection;
using PokerStat.Configuration.Extensions;

namespace PokerStat.IdentityServer.AuthContext
{
  public class IdentityServerConfig
  {
    public static IEnumerable<IdentityResource> GetIdentityResources()
    {
      return new List<IdentityResource>
      {
        new IdentityResources.OpenId(),
        new IdentityResources.Profile(),
        new IdentityResources.Email()
      };
    }

    public static IEnumerable<ApiResource> GetApiResources()
    {
      return new List<ApiResource>
      {
      };
    }

    public static IEnumerable<Client> GetClients(IConfiguration configuration)
    {
      IdentityServerConfiguration isConfig = configuration.BindTo<IdentityServerConfiguration>();

      return isConfig.Clients.Select(cl => new Client
      {
        ClientId = cl.Id,
        ClientName = cl.Id,
        AllowedGrantTypes = GrantTypes.Code,
        RequirePkce = true,
        RequireClientSecret = false,
        RedirectUris = cl.Uris.Select(url => $"{url}/callback.html").ToList(),
        PostLogoutRedirectUris = cl.Uris.Select(url => $"{url}/index.html").ToList(),
        AllowedCorsOrigins = cl.Uris,
        RequireConsent = false,
        AllowedScopes =
        {
          IdentityServerConstants.StandardScopes.OpenId,
          IdentityServerConstants.StandardScopes.Profile,
          IdentityServerConstants.StandardScopes.Email,
        }
      });
    }
  }
}
