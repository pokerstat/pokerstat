using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PokerStat.IdentityServer.AuthContext.DTO;

namespace PokerStat.IdentityServer.AuthContext
{
  public sealed class AuthDataBaseContext : IdentityDbContext<PokerStatUser>
  {
    public AuthDataBaseContext(DbContextOptions<AuthDataBaseContext> options)
      : base(options)
    {
    }
  }
}
