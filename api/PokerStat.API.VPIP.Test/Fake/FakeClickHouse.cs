﻿using System;
using System.Threading.Tasks;
using PokerStat.ClickHouseManager;

namespace PokerStat.API.VPIP.Test.Fake
{
    public class FakeClickHouse : IClickHouseManager
    {
        public Task<decimal> GetDecimalAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<decimal[]> GetDecimalsAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> GetGuidAsync(string query)
        {
            throw new NotImplementedException();
        }

        public async Task<Guid[]> GetGuidsAsync(string query)
        {
            throw new NotImplementedException();
        }

        public async Task<int> GetIntAsync(string query)
        {
            if (query ==
                "select count (*) from( select distinct hand_id from PokerStat_Analyzer.Data where player_id='f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet=1 and typeofgame = 1 and pokerroom= 0 and stage_id = 2 and count is not null);"
                || query ==
                "select count (*) from( select distinct hand_id from PokerStat_Analyzer.Data where player_id='f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet=1 and typeofgame = 1 and pokerroom= 0 and date_action BETWEEN toDate('2000-02-18 00:00:00') and toDate('2000-04-18 00:00:00') and stage_id = 2 and count is not null);"
            )
                return await Task.FromResult(5);
            throw new ArgumentException("wrong query", nameof(query));
        }

        public Task<int[]> GetIntsAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetStringAsync(string query)
        {
            throw new NotImplementedException();
        }

        public Task<string[]> GetStringsAsync(string query)
        {
            throw new NotImplementedException();
        }
    }
}