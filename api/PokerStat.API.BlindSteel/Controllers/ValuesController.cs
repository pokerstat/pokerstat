﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.ClickHouseManager;
using PokerStat.Model;
using Action = PokerStat.Model.Action;

namespace PokerStat.API.BlindSteel.Controllers
{
    public class ValuesController : AnalyzerController
    {
        private readonly IClickHouseManager _clickHouseManager;
        private readonly IHttpManager _httpManager;
        
        
        public ValuesController(IValidatorFactory validatorFactory, IHttpManager httpManager, IClickHouseManager clickHouseManager) : base(validatorFactory)
        {
            _httpManager = httpManager;
            _clickHouseManager = clickHouseManager;
        }

        private async Task<long> CalculateBlindSteelCount(string player_id, Guid[] guids)
        {
            //какое первое действие было совершено на префлопе
            const string sql_check_player = "select player_id, action from PokerStat_Analyzer.Data where hand_id = '{0}' and stage_id ={1} and count is not null order by ordr limit 1;";
            long count = 0;

            for (long i = 0; i < guids.LongLength; i++)
            {
                var results = (await _clickHouseManager.GetStringAsync(string.Format(sql_check_player, guids[i], (byte)Stages.PreFlop))).Split("\t"); //по символу табуляции разбиваем значеия
                if (results.Length == 2 && player_id==results[0].Trim()) //player_id, action
                {
                    if (Action.TryParse(results[1].Trim(), true, out Action act) && act.HasFlag(Action.Raises))
                        count++;
                }
            }

            return count;
        }
        
        protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            var count = await _httpManager.SendQueryAsync(request);
            const string sql = "select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and position_id in ({4},{5}) and stage_id={6} and bitAnd(action,{7})={7};";
            var guids = await _clickHouseManager.GetGuidsAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom,(byte)Position.CutOff,(byte)Position.Button, (byte)Stages.PreFlop, (byte)Action.Raises));
            return await CalculateBlindSteelCount(request.Player_id, guids) / count;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
        {
            var count = await _httpManager.SendQueryAsync(request);
            const string sql = "select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and date_action BETWEEN toDate('{4}') and toDate('{5}') and position_id in ({6},{7}) and stage_id={8} and bitAnd(action,{9})={9};";
            var guids = await _clickHouseManager.GetGuidsAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom, request.DateStart, request.DateEnd, (byte)Position.CutOff,(byte)Position.Button, (byte)Stages.PreFlop, (byte)Action.Raises));
            return await CalculateBlindSteelCount(request.Player_id, guids) / count;
        }
    }
}