﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.ClickHouseManager;
using PokerStat.Model;
using Action = PokerStat.Model.Action;

namespace PokerStat.API.PFR.Controllers
{

    public class ValuesController : AnalyzerController
    {
        private readonly IClickHouseManager _clickHouseManager;
        private readonly IHttpManager _httpManager;
        
        
        public ValuesController(IValidatorFactory validatorFactory, IHttpManager httpManager, IClickHouseManager clickHouseManager) : base(validatorFactory)
        {
            _httpManager = httpManager;
            _clickHouseManager = clickHouseManager;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            var count = await _httpManager.SendQueryAsync(request);
            //скрипт рук, где была зафиксировано повышение у игрока
            const string sql = "select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and bitAnd(action,{4})={4} and stage_id={5};";

            var guids = await _clickHouseManager.GetGuidsAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom, (short)Action.Raises, (byte)Stages.PreFlop));

            return await CalculateRaiseCount(request.Player_id, guids) / count;
        }

        private async Task<long> CalculateRaiseCount(string player_id, Guid[] guids)
        {
            //какое первое действие было совершено на префлопе
            const string sql_check_action = "select action from PokerStat_Analyzer.Data where hand_id = '{0}' and stage_id ={2} and player_id = '{1}' order by ordr limit 1;";
            long raisecount = 0;

            for (long i = 0; i < guids.LongLength; i++)
            {
                var action = (Action) await _clickHouseManager.GetIntAsync(string.Format(sql_check_action, guids[i], player_id,(byte)Stages.PreFlop));
                if (action.HasFlag(Action.Raises)) // есть ли повышение
                    raisecount++;
            }

            return raisecount;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
        {
            var count = await _httpManager.SendQueryAsync(request);
            //скрипт рук, где была зафиксировано повышение у игрока
            const string sql = "select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and date_action BETWEEN toDate('{4}') and toDate('{5}') and bitAnd(action,{6})={6} and stage_id={7};";

            var guids = await _clickHouseManager.GetGuidsAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom, request.DateStart, request.DateEnd, (short)Action.Raises, (byte)Stages.PreFlop));
            return await CalculateRaiseCount(request.Player_id, guids) / count;

        }
    }
}
