﻿using System;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.PFR.ConfigurationClass
{
    public class CountHandHost:IHttpHost
    {
        public string Host { get; set; }
    }
}
