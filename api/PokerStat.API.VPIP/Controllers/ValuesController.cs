﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.ClickHouseManager;
using PokerStat.Model;

namespace PokerStat.API.VPIP.Controllers
{
    public class ValuesController : AnalyzerController
    {
        private readonly IClickHouseManager _clickHouseManager;
        private readonly IHttpManager _httpManager;

        public ValuesController(IValidatorFactory validatorFactory, IHttpManager httpManager, IClickHouseManager clickHouseManager) : base(validatorFactory)
        {
            _httpManager = httpManager;
            _clickHouseManager = clickHouseManager;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            const string sql = "select count (*) from( select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and stage_id = {4} and count is not null);";
            var counthand = await _httpManager.SendQueryAsync(request);
            var count = await _clickHouseManager.GetIntAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom,(byte)Stages.PreFlop));
            return count / counthand;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
        {
            const string sql = "select count (*) from( select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and date_action BETWEEN toDate('{4}') and toDate('{5}') and stage_id = {6} and count is not null);";
            var counthand = await _httpManager.SendQueryAsync(request);
            var count = await _clickHouseManager.GetIntAsync(string.Format(sql, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom, request.DateStart, request.DateEnd,(byte)Stages.PreFlop));
            return count / counthand;
        }
    }
}