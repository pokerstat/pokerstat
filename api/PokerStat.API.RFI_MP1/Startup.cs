﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PokerStat.AnalyzerHttpHelper;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.API.RFI_MP1.ConfigurationClass;
using PokerStat.ClickHouse.Helper;
using PokerStat.ClickHouseManager;
using PokerStat.Configuration.Extensions;
using PokerStat.HttpContracts.Middleware;

namespace PokerStat.API.RFI_MP1
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _configuration;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .Build();
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvcCore()
                .AddJsonFormatters(options =>
                {
                    options.NullValueHandling = NullValueHandling.Ignore;
                })
                .AddFluentValidation(fv => fv.ValidatorFactoryType = typeof(AttributedValidatorFactory));
            services.AddHttpClient()
                .AddSingleton(_configuration.BindTo<ClickHouseConnection>())
                .AddSingleton<IHttpHost>(_configuration.BindTo<CountHandHost>())
                .AddSingleton<IHttpManager, HttpManager>()
                .AddSingleton<IClickHouseManager, ClickHouseManager.ClickHouseManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}