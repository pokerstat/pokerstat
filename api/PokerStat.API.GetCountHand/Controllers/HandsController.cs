using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;
using PokerStat.ClickHouseManager;

namespace PokerStat.API.GetCountHand.Controllers
{
    public class HandsController : AnalyzerController
    {
        private readonly IClickHouseManager _clickHouseManager;
        //                                                "select count (*)  from  (select distinct hand_id from PokerStat_Analyzer.Data where player_id = 'f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet = 0 and typeofgame = 1 and pokerroom = 0) "
        //"select count (*)  from  (select distinct hand_id from PokerStat_Analyzer.Data where player_id = 'f6ec47f1-e391-41d1-89a0-7936ea7a2991' and is_tournamnet = 1 and typeofgame = 1 and pokerroom = 0 and date_action BETWEEN toDate('2009-04-18') and toDate('2020-04-18')) "

        public HandsController(IValidatorFactory validatorFactory,IClickHouseManager clickHouseManager) : base(validatorFactory)
       {
           _clickHouseManager = clickHouseManager;
        }

       protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            const string GetHandsCountByIdQuery = "select count() from (select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3});";

            return await _clickHouseManager.GetIntAsync(string.Format(GetHandsCountByIdQuery,request.Player_id,request.IsTournamnet,request.TypeOfGame,request.PokerRoom));
        }

       

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
       {
            const string GetHandsCountByIdQuery = "select count() from (select distinct hand_id from PokerStat_Analyzer.Data where player_id='{0}' and is_tournamnet={1} and typeofgame = {2} and pokerroom= {3} and date_action BETWEEN toDate('{4}') and toDate('{5}'));";

            return await _clickHouseManager.GetIntAsync(string.Format(GetHandsCountByIdQuery, request.Player_id, request.IsTournamnet, request.TypeOfGame, request.PokerRoom,request.DateStart,request.DateEnd));
        }
       
    }
}