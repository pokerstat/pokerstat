﻿using System;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.RFI_UTG2.ConfigurationClass
{
    public class CountHandHost:IHttpHost
    {
        public string Host { get; set; }
    }
}
