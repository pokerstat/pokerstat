﻿using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;
using PokerStat.Api.PFR_VPIP_Ratio.Factorys;

namespace PokerStat.Api.PFR_VPIP_Ratio.Controllers
{
    public class ValuesController : AnalyzerController
    {
        readonly IHttpManagerFactory _httpManagerFactory;
        public ValuesController(IValidatorFactory validatorFactory, IHttpManagerFactory httpManagerFactory) : base(validatorFactory)
        {
            _httpManagerFactory = httpManagerFactory;
        }
        

        protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            var countpfr = await _httpManagerFactory.PFRManager.SendQueryAsync(request);
            var countvpip = await _httpManagerFactory.VPIPManager.SendQueryAsync(request);

            return countpfr / countvpip;
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
        {
            var countpfr = await _httpManagerFactory.PFRManager.SendQueryAsync(request);
            var countvpip = await _httpManagerFactory.VPIPManager.SendQueryAsync(request);

            return countpfr / countvpip;
        }
    }
}