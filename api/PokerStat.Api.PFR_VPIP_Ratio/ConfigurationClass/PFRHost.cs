﻿using System;
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.API.PFR_VPIP_Ratio.ConfigurationClass
{
    public class PFRHost:IHttpHost
    {
        public string Host { get; set; }
    }
}
