﻿
using PokerStat.AnalyzerHttpHelper.Interfaces;

namespace PokerStat.Api.PFR_VPIP_Ratio.Factorys
{
    public interface IHttpManagerFactory
    {
        IHttpManager VPIPManager { get; }
        IHttpManager PFRManager { get; }
    }
}
