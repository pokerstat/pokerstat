﻿using System.Net.Http;
using PokerStat.AnalyzerHttpHelper;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.API.PFR_VPIP_Ratio.ConfigurationClass;

namespace PokerStat.Api.PFR_VPIP_Ratio.Factorys
{
    public class HttpManagerFactory: IHttpManagerFactory
    {

        public HttpManagerFactory(VPIPHost vPIPHost, PFRHost pFRHost, IHttpClientFactory httpClientFactory)
        {
            VPIPManager = new HttpManager(httpClientFactory, vPIPHost);
            PFRManager = new HttpManager(httpClientFactory, pFRHost);
        }

        public IHttpManager VPIPManager { get; private set; }

        public IHttpManager PFRManager { get; private set; }
    }
}
