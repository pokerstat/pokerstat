﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using PokerStat.CloudStorageManager.Managers;
using PokerStat.Configuration.ConfigSection;
using PokerStat.Configuration.Extensions;
using PokerStat.DataBaseManager;
using PokerStat.Serialization;
using PokerStat.SerilogConfiguration.Extensions;
using Serilog;

namespace PokerStat.Console.ParseFile
{
    class Env : IHostingEnvironment
    {
        public string EnvironmentName { get; set; }
        public string ApplicationName { get; set; }
        public string WebRootPath { get; set; }
        public IFileProvider WebRootFileProvider { get; set; }
        public string ContentRootPath { get; set; }
        public IFileProvider ContentRootFileProvider { get; set; }
    }

    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                System.Console.WriteLine(Directory.GetCurrentDirectory());
                IConfiguration configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddCommandLine(args)
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .Build();

                Log.Logger = new LoggerConfiguration()
                    .RegisterSerilog(configuration, new Env
                    {
                        EnvironmentName = configuration.Contains("DEV_ENVIRONMENT") ? EnvironmentName.Development : null
                    }, "PokerStat.Console.ParseFile")
                    .CreateLogger();
                var lf = new LoggerFactory().AddSerilog(Log.Logger);


                var dbContext = new DataBaseFactory<SqlConnection>(configuration.BindTo<ConnectionStrings>().PokerStatDataBase);
                var fileManager = new YandexCloudStorageManager(configuration, lf.CreateLogger<object>());
                var factorySerialization = new FactorySerialization();
                var bgWorker = new BGWorker(dbContext.HandProvider, lf.CreateLogger<BGWorker>(), fileManager, factorySerialization);

                var st = new Stopwatch();
                do
                {
                    st.Restart();
                    bgWorker.Start();
                    st.Stop();
                    var delay = configuration.BindTo<Parser>().Delay - st.ElapsedMilliseconds;
                    if (delay > 0) //если парсинг выполнялся слишком долго, то не ждем запуск
                    {
                        Thread.Sleep((int) delay);
                    }
                } while (true);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Ошибка: " + ex.Message);
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}