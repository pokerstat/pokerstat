﻿using Microsoft.Extensions.Logging;
using PokerStat.DataBaseManager.Interface;
using PokerStat.CloudStorageManager.Interface;
using PokerStat.Serialization;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PokerStat.Console.ParseFile
{
    sealed class BGWorker
    {
        readonly ILogger<BGWorker> _logger;
        readonly IHandProvider _handProvider;
        readonly IFileManager _manager;
        readonly IFactorySerialization _factorySerialization;

        public BGWorker(IHandProvider handProvider, ILogger<BGWorker> logger, IFileManager fileManager, IFactorySerialization factorySerialization)
        {
            _logger = logger;
            _handProvider = handProvider;
            _manager = fileManager;
            _factorySerialization = factorySerialization;
        }

        public void Start()
        {
            _logger.LogInformation("Start BGWorker");
            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 100; //каждый поток подребляет 2 конекшена, пулл рассчитан на 200
            Parallel.ForEach(_manager.GetNamesOfBlobsAsync().Result, 
                po, 
                (f) => SerializationFileAsync(f).GetAwaiter().GetResult());
            _logger.LogInformation("Finish BGWorker");
        }

        private async Task SerializationFileAsync(string file)
        {
            ISerializationHistory ser;
            using (var stream = new MemoryStream())
            {
                try
                {
                    await _manager.DownloadToStreamAsync(file, stream);
                    ser = _factorySerialization.GetSerializator(stream);                 
                    if (ser == null)
                    {
                        _logger.LogWarning($"serializator not found for {file}");
                        await _manager.MoveToErrorAsync(stream, file);
                        return;
                    }
                    var hands = ser.GetHands();

                    var tables = hands.Select(x => x.Table);
                    
                    foreach (var h in hands)
                    {
                        await _handProvider.SaveAsync(h);
                    }
                    await _manager.DeleteAsync(file);
                }
                catch (Serialization.SerializationException se)
                {
                    _logger.LogError(se, $"error in serilization in {file}");
                    await _manager.MoveToErrorAsync(stream, file);
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, $"FATAL ERRROR {file}");
                }
            }
        }
    }
}