using System;
using PokerStat.Model;

namespace PokerStat.Console.Analyzer
{
    public class CPlayer:IPlayer
    {
        public CPlayer( string id)
        {
            Id=Guid.Parse(id);
        }
        public Guid Id { get; }
        public string Nick { get; set; }
        public IPokerRoom PokerRoom { get; set; }
    }
}