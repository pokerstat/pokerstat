﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PokerStat.CloudStorageManager.Managers;

namespace PokerStat.Console.FileManager
{
    public class Logger : ILogger
    {
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            System.Console.WriteLine(exception);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }
    }

    public class LoggerFactory : ILoggerFactory
    {
        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new Logger();
        }

        public void AddProvider(ILoggerProvider provider)
        {
        }
    }

    class Program
    {
        static async Task Main(string[] args)
        {
            var stopWatch = Stopwatch.StartNew();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var fileManager = new YandexCloudStorageManager(configuration, new LoggerFactory().CreateLogger<object>());
            var i = 1;

            foreach (var name in await fileManager.GetNamesOfBlobsAsync())
            {
                using (Stream stream = new MemoryStream())
                {
                    await fileManager.DownloadToStreamAsync(name, stream);
                    await fileManager.MoveToErrorAsync(stream, name);
                    System.Console.WriteLine($"{i++:000}) {name}, {stream.Length} bytes");
                }

                await fileManager.DeleteAsync(name);
            }
        }
    }
}