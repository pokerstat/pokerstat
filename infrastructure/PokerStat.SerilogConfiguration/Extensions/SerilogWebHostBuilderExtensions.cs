using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using PokerStat.Configuration.ConfigSection;
using PokerStat.Configuration.Extensions;
using PokerStat.SerilogConfiguration.Enrichers;
using Serilog;
using Serilog.Events;

namespace PokerStat.SerilogConfiguration.Extensions
{
    public static class SerilogWebHostBuilderExtensions
    {
        public static IWebHostBuilder ConfigureSerilog(this IWebHostBuilder builder, string fileName)
        {
            return builder.UseSerilog((context, loggerConfiguration) =>
                loggerConfiguration.RegisterSerilog(context.Configuration, context.HostingEnvironment, fileName));
        }

        public static LoggerConfiguration RegisterSerilog(this LoggerConfiguration loggerConfiguration, IConfiguration configuration, IHostingEnvironment env, string fileName)
        {
            loggerConfiguration
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.Is(configuration.BindTo<Logging>().Level)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext();

            if (env.IsDevelopment())
            {
                loggerConfiguration
                    .WriteTo.File($"{fileName}.log")
                    .WriteTo.ColoredConsole(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} {Level:u3} ({ThreadId}) {Message:lj}{NewLine}{Exception}");
            }
            else
            {
                //TODO Need to add serilog skins for sending errors Yandex MQ
                //https://gitlab.com/pokerstat/pokerstat/issues/29
            }

            return loggerConfiguration;
        }
    }
}