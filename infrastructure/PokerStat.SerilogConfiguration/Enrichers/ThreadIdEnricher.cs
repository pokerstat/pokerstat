using System;
using Serilog.Core;
using Serilog.Events;

namespace PokerStat.SerilogConfiguration.Enrichers
{
    sealed class ThreadIdEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("ThreadId", Environment.CurrentManagedThreadId));
        }
    }
}