using System.Collections.Generic;
using PokerStat.Model;

namespace PokerStat.Serialization
{
    public interface ISerializationHistory
    {
        IEnumerable<IHand> GetHands();
    }
}
