﻿using System.IO;
using PokerStat.Serialization.SerializationProvider.PokerStars;

namespace PokerStat.Serialization
{
    public sealed class FactorySerialization : IFactorySerialization
    {
        const string _pokerstars = "PokerStars";
       
        public ISerializationHistory GetSerializator(Stream stream)
        {
            var str= stream.ReadStream().TrimStart((char)65279);

            if (string.IsNullOrEmpty(str))
                return null;
            if (str.Substring(0, _pokerstars.Length+1) == "PokerStars " && str[_pokerstars.Length + 1] >= 'A' && str[_pokerstars.Length + 1] <= 'Z')
            {
                return new SerializationPokerStarsHistory(str);
            }
            if (str.Substring(8, _pokerstars.Length+1) == "PokerStars " && str[0] >= 'А' && str[0] <= 'Я')
            {
               return new SerializationPokerStarsHistoryRu(str);
            }
            return null;
        }
    }
}
