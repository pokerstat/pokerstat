﻿using System.IO;

namespace PokerStat.Serialization
{
    public interface IFactorySerialization
    {
        ISerializationHistory GetSerializator(Stream stream);
    }
}
