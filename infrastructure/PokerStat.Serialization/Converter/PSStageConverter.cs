﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using PokerStat.Model;
using PokerStat.Serialization.Model;

namespace PokerStat.Serialization.Converter
{
    internal class PSStageConverter : IStageConverter
    {
        readonly ActionBuilder ab;
        private readonly IPlayerHand[] _playerHands;
        public PSStageConverter(IConverterWord converterSetting, IPlayerHand[] playerHands)
        {
            ab = new ActionBuilder(converterSetting);
            _playerHands = playerHands;
        }

        public IHandStage AnteStage(IEnumerable<string> raw)
        {
            HandHistorySer stage = new HandHistorySer();
            stage.Stage = Stages.Ante;
            Collection<IStageAction> colhand = new Collection<IStageAction>();
            byte _order = 0;
            foreach (string item in raw)
            {
                SerStageAction actions = new SerStageAction {Order = _order,PlayerHand = CreatePlayerHand(item)};
                if (actions.PlayerHand == null)
                    continue;
                PokerStat.Model.Action act = ab.CreateActionAnte(item.Replace(actions.PlayerHand.Player.Nick, ""));
                if (act == PokerStat.Model.Action.NotFound)
                    continue;
                actions.Action = act;
                actions.Count = GetCount(item, actions.PlayerHand.Player);
                if (actions.Count > actions.PlayerHand.CountChips)
                    throw new SerializationException();
                _order++;
                colhand.Add(actions);
            }
            stage.Actions = colhand;
            return stage;
        }

        public IHandStage FlopStage(IEnumerable<string> raw)
        {
            return CreateHandsByStreet(raw, Stages.Flop);
        }

        public IHandStage RiverStage(IEnumerable<string> raw)
        {
            return CreateHandsByStreet(raw, Stages.River);
        }

        public IHandStage ShowDownStage(IEnumerable<string> raw)
        {
            HandHistorySer stage = new HandHistorySer();
            stage.Stage = Stages.ShowDown;
            Collection<IStageAction> colhand = new Collection<IStageAction>();
            byte _order = 0;
            foreach (string item in raw)
            {
                if (!item.Contains(": "))
                {
                    SerStageAction history = new SerStageAction {Order = _order, PlayerHand = CreatePlayerHand(item)};
                    if (history.PlayerHand == null)
                        continue;
                    PokerStat.Model.Action act = ab.CreateActionShowDown(item.Replace(history.PlayerHand.Player.Nick, ""));
                    if (act == PokerStat.Model.Action.NotFound)
                        continue;
                    history.Action = act;
                    history.Count = GetCount(item, history.PlayerHand.Player);
                    if (history.Count > _playerHands.Sum(x=>x.CountChips))
                        throw new SerializationException();
                    _order++;
                    colhand.Add(history);
                }
            }
            stage.Actions = colhand;
            return stage;
        }

        public IHandStage TurnStage(IEnumerable<string> raw)
        {
            return CreateHandsByStreet(raw, Stages.Turn);
        }

        public IHandStage PreFlopStage(IEnumerable<string> raw)
        {
            HandHistorySer hhs = new HandHistorySer();
            hhs.Stage = Stages.PreFlop;
            CreateStage(hhs, raw, ab.CreateActionPreFlop);
            return hhs;
        }
        private void CreateStage(HandHistorySer handstage, IEnumerable<string> raw, Func<string, PokerStat.Model.Action> createaction)
        {
            Collection<IStageAction> colhand = new Collection<IStageAction>();
            byte _order = 0;
            foreach (string item in raw)
            {
                SerStageAction history = new SerStageAction { Order = _order, PlayerHand = CreatePlayerHand(item)};
                if (history.PlayerHand == null)
                    continue;
                PokerStat.Model.Action act;
                if (item.Contains(": "))
                {
                    act = createaction(item.Replace(history.PlayerHand.Player.Nick, ""));
                    if (act == PokerStat.Model.Action.NotFound)
                        continue;
                    if (act != PokerStat.Model.Action.Folds && act != PokerStat.Model.Action.Checks)
                    {
                        history.Count = GetCount(item,history.PlayerHand.Player);
                        if (act == PokerStat.Model.Action.Collected)
                        {
                            if (history.Count > _playerHands.Sum(x => x.CountChips))
                                throw new SerializationException();
                        }
                        else
                        {
                            if (history.Count > history.PlayerHand.CountChips)
                                throw new SerializationException();
                        }
                    }
                }
                else
                {
                    act = ab.CreateActionShowDown(item);
                    if (act == PokerStat.Model.Action.NotFound)
                        continue;
                    history.Count = GetCount(item, history.PlayerHand.Player);
                    if (history.Count > _playerHands.Sum(x => x.CountChips))
                        throw new SerializationException();
                }
                history.Action = act;
                _order++;
                colhand.Add(history);
            }
            handstage.Actions = colhand;
        }
        private IHandStage CreateHandsByStreet(IEnumerable<string> raw, Stages stage)
        {
            HandHistorySer hhs = new HandHistorySer();
            hhs.Stage = stage;
            CreateStage(hhs, raw, ab.CreateAction);
            return hhs;
        }

        private decimal GetCount(string raw, IPlayer player)
        {
            MatchCollection reg = Regex.Matches(raw.Replace(player.Nick,""), @".?([0-9]+\.?[0-9]*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            return reg[0].Groups[1].Value.ParseCount();
        }
        private IPlayerHand CreatePlayerHand(string str)
        {
            return _playerHands.FirstOrDefault(x => str.Length>x.Player.Nick.Length && str.Substring(0, x.Player.Nick.Length) == x.Player.Nick);
        }

    }
}
