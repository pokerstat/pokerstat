﻿namespace PokerStat.Serialization.Converter
{
    public interface IConverterWord
    {
        string PostsSmallBlind { get; }
        string PostsAnte { get; }
        string PostsBigBlind { get; }
        string Folds { get; }
        string Raises { get; }
        string Collected { get; }
        string Checks { get; }
        string Calls { get; }
        string Bets { get; }
        string All_In { get; }
        string Zoom { get; }
    }
}
