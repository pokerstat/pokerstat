﻿using System;
using System.Globalization;

namespace PokerStat.Serialization.Converter
{
    public sealed class ConverterSettingRU:IConverterSetting
    {
        public string Bets => "бет";
        public string PostsBigBlind => "ставит большой блайнд";
        public string PostsSmallBlind => "ставит малый блайнд";
        public string PostsAnte => "ставит анте";
        public string Collected => "получил";
        public string Calls => "колл";
        public string Checks => "чек";
        public string Folds => "фолд";
        public string Raises => "рейз";
        public string All_In => "олл-ин";

        public IFormatProvider FormatProvider => CultureInfo.CreateSpecificCulture("ru-RU");

        public string Zoom => "Zoom";
    }
}
