﻿using System;
namespace PokerStat.Serialization.Converter
{
    public interface IConverterSetting:IConverterWord
    {
        IFormatProvider FormatProvider { get; }
    }
}
