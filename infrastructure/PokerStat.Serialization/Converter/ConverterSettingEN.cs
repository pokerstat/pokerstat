﻿
using System;
using System.Globalization;

namespace PokerStat.Serialization.Converter
{
    public sealed class ConverterSettingEN:IConverterSetting
    {
        public string PostsSmallBlind => "posts small blind";
        public string PostsAnte => "posts the ante";
        public string PostsBigBlind => "posts big blind";
        public string Folds => "folds";
        public string Raises => "raises";
        public string Collected => "collected";
        public string Checks => "checks";
        public string Calls => "calls";
        public string Bets => "bets";
        public string All_In => "all-in";
        public string Zoom => "Zoom";

        public IFormatProvider FormatProvider => CultureInfo.CreateSpecificCulture("en-US");
    }
}
