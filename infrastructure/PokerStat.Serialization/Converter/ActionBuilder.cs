﻿
namespace PokerStat.Serialization.Converter
{
    internal sealed class ActionBuilder
    {
        readonly IConverterWord converterSetting;
        public ActionBuilder(IConverterWord setting)
        {
            converterSetting = setting;
        }
        public PokerStat.Model.Action CreateActionAnte(string str)
        {
            if (str.Contains(converterSetting.PostsSmallBlind))
                return PokerStat.Model.Action.Posts| PokerStat.Model.Action.SmallBlind;
            if (str.Contains(converterSetting.PostsBigBlind))
                return PokerStat.Model.Action.Posts | PokerStat.Model.Action.BigBlind;
            if (str.Contains(converterSetting.PostsAnte))
                return PokerStat.Model.Action.Posts | PokerStat.Model.Action.Ante;
            return PokerStat.Model.Action.NotFound;
        }

        private bool HasAll_In(string str)
        {
            return str.Contains(converterSetting.All_In);
        }

        public PokerStat.Model.Action CreateActionPreFlop(string str)
        {
            if (str.Contains(converterSetting.Folds))
                return PokerStat.Model.Action.Folds;
            if (str.Contains(converterSetting.Checks))
                return PokerStat.Model.Action.Checks;
            if (str.Contains(converterSetting.Collected))
                return PokerStat.Model.Action.Collected;
            if (str.Contains(converterSetting.Raises))
            {
                if (HasAll_In(str))
                    return PokerStat.Model.Action.Raises | PokerStat.Model.Action.All_In;
                return PokerStat.Model.Action.Raises;
            }
            if (str.Contains(converterSetting.Calls))
            {
                if (HasAll_In(str))
                    return PokerStat.Model.Action.Calls | PokerStat.Model.Action.All_In;
                return PokerStat.Model.Action.Calls;
            }
            return PokerStat.Model.Action.NotFound;
        }

        public PokerStat.Model.Action CreateActionShowDown(string str)
        {
            if (str.Contains(converterSetting.Collected))
                return PokerStat.Model.Action.Collected;
            return PokerStat.Model.Action.NotFound;
        }

        public PokerStat.Model.Action CreateAction(string str)
        {
            if (str.Contains(converterSetting.Bets))
            {
                if (HasAll_In(str))
                    return PokerStat.Model.Action.Posts | PokerStat.Model.Action.All_In;
                return PokerStat.Model.Action.Posts;
            }
            return CreateActionPreFlop(str);
        }
    }
}
