﻿using System.Collections.Generic;
using PokerStat.Model;

namespace PokerStat.Serialization.Converter
{
    internal interface IStageConverter
    {
        IHandStage AnteStage(IEnumerable<string> raw);

        IHandStage FlopStage(IEnumerable<string> raw);

        IHandStage TurnStage(IEnumerable<string> raw);

        IHandStage RiverStage(IEnumerable<string> raw);

        IHandStage PreFlopStage(IEnumerable<string> raw);

        IHandStage ShowDownStage(IEnumerable<string> raw);
    }
}
