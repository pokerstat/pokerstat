﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using PokerStat.Model;
using PokerStat.Serialization.Model;

namespace PokerStat.Serialization.Converter
{
    public static class ConverterCard
    {
        const string _regex_pair_card = @"\[.{2,}?\]";
        const string _regex_card = @"\b[0-9TAKJQ][sdch]\b";
       
        public static IEnumerable<ICard> GetCards(string raw)
        {
            var crds = new Collection<ICard>();
            foreach (string r in FoundString(raw, _regex_pair_card))
            {
                foreach (string r1 in FoundString(r, _regex_card))
                {
                    crds.Add(CreateCard(r1));
                }
            }
            return crds;
        }

        private static IEnumerable<string> FoundString(string raw, string pattern)
        {
            return Regex.Matches(raw, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase).Select(x => x.Value);
        }
        private static ICard CreateCard(string card)
        {
            return new SerCard { ShortName = card[0], SuitCode = card[1] };
        }
    }
}
