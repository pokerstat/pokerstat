﻿namespace PokerStat.Serialization.SerializationProvider.PokerStars
{
    // ReSharper disable once InconsistentNaming
    internal sealed class PokerStarsSettingWordRU : ISettingWord
    {
        public char NumberSymbol => '№';
        public string Seat => "Место";
        public string HoleCard => "*** ЗАКРЫТЫЕ КАРТЫ ***";
        public string FLOP => "*** ФЛОП *** ";
        public string TURN => "*** ТЕРН ***";
        public string RIVER => "*** РИВЕР *** ";
        public string ShowDown => "*** ВСКРЫТИЕ КАРТ ***";
        public string EndOfFile => "*** ИТОГ ***";
    }
}