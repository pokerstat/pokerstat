﻿namespace PokerStat.Serialization.SerializationProvider.PokerStars
{
    internal sealed class PokerStarsSettingWord : ISettingWord
    {
        public char NumberSymbol => '#';
        public string Seat => "Seat";
        public string HoleCard => "*** HOLE CARDS ***";
        public string FLOP => "*** FLOP ***";
        public string TURN => "*** TURN ***";
        public string RIVER => "*** RIVER ***";
        public string ShowDown => "*** SHOW DOWN ***";
        public string EndOfFile => "*** SUMMARY ***";
    }
}