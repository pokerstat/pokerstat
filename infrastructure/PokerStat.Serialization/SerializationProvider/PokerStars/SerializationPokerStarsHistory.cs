using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PokerStat.Model;
using PokerStat.Serialization.Converter;
using PokerStat.Serialization.Model;
using Action = PokerStat.Model.Action;

namespace PokerStat.Serialization.SerializationProvider.PokerStars
{
    internal class SerializationPokerStarsHistory : ISerializationHistory
    {
        private readonly ConcurrentBag<IHand> _dic = new ConcurrentBag<IHand>();

        private readonly IConverterSetting _converterSetting;

        private readonly string _source;

        readonly ISettingWord _settingword;

        public SerializationPokerStarsHistory(string source) : this(new ConverterSettingEN(), new PokerStarsSettingWord(), source)
        {
        }

        protected SerializationPokerStarsHistory(IConverterSetting converter, ISettingWord settingWord, string source)
        {
            _converterSetting = converter;
            _settingword = settingWord;
            _source = source.Replace("\r", ""); // for not Unix systems, we need to replace too
        }

        public IEnumerable<IHand> GetHands()
        {
            string[] hands = _source.Split("\n\n\n");
            Parallel.For(0, hands.LongLength, (i) =>
                {
                    if (!string.IsNullOrEmpty(hands[i]))
                    {
                        var text = CreateTextParse(hands[i]);
                        if (text != null)
                            _dic.Add(Fill(text));
                    }
                }
            );

            return _dic.OrderBy(x => x.Date);
        }

        private IHand Fill(TextParse history)
        {
            var _hand = GetHand(history.TitleString);

            var _playerhand = GetPlayers(history, _hand.Table.PokerRoom);

            var _col = new List<IHandStage>();

            var _sc = new PSStageConverter(_converterSetting, _playerhand);
            var _delegatedic = CreateDictionary(_sc);
            string[] _stages =
            {
                _settingword.HoleCard, _settingword.FLOP, _settingword.TURN, _settingword.RIVER,
                _settingword.ShowDown, _settingword.EndOfFile
            };

            int count = 0;
            int index = 0;

            var tasks = new Collection<Task<IHandStage>>();

            var _body = history.BodyString;

            do
            {
                var preflopstring = CreateStageString(_stages[index], ref _body, ref count);
                var deleg = _delegatedic[_stages[index]];
                tasks.Add(Task.Run(() =>
                {
                    var stageshand = deleg(preflopstring);
                    var str = preflopstring.First();
                    if (str.Contains(_settingword.FLOP) || str.Contains(_settingword.TURN) ||
                        str.Contains(_settingword.RIVER))
                    {
                        foreach (var card in ConverterCard.GetCards(str))
                            stageshand.Cards.Add(card);
                    }

                    return stageshand;
                }));
                index++;
            } while (index < _stages.Length && count < _body.Length);

            _col.AddRange(tasks.Select(x=>x.Result));
            Validate(_col);
            _hand.StageHands = _col;
            return _hand;
        }

        private IDictionary<string, Func<IEnumerable<string>, IHandStage>> CreateDictionary(PSStageConverter _sc)
        {
            IDictionary<string, Func<IEnumerable<string>, IHandStage>> _delegatedic =
                new Dictionary<string, Func<IEnumerable<string>, IHandStage>>();
            _delegatedic.Add(_settingword.HoleCard, _sc.AnteStage);
            _delegatedic.Add(_settingword.FLOP, _sc.PreFlopStage);
            _delegatedic.Add(_settingword.TURN, _sc.FlopStage);
            _delegatedic.Add(_settingword.RIVER, _sc.TurnStage);
            _delegatedic.Add(_settingword.ShowDown, _sc.RiverStage);
            _delegatedic.Add(_settingword.EndOfFile, _sc.ShowDownStage);
            return _delegatedic;
        }

        private void Validate(List<IHandStage> _col)
        {
            if (_col.Count < 2 || !(_col.Any(x => x.Stage == Stages.Ante) && _col.Any(x => x.Stage == Stages.PreFlop)))
                throw new SerializationException();
            var ante = _col.Single(x => x.Stage == Stages.Ante);
            if (!ante.Actions.Any(x => x.PlayerHand.Position == Position.BigBlind && x.Action.HasFlag(Action.BigBlind)))
                throw new SerializationException();
            if (!ante.Actions.Any(x => x.PlayerHand.Position == Position.SmallBlind && x.Action.HasFlag(Action.SmallBlind)))
                throw new SerializationException();
        }

        private IPlayerHand[] GetPlayers(TextParse textParse, IPokerRoom pokerRoom)
        {
            var pstr = textParse.PlayerString;
            SerPlayerHand[] players = new SerPlayerHand[pstr.Length];
            for (byte i = 0; i < pstr.Length; i++)
            {
                players[i] = GetPlayerHand(pstr[i], textParse.BodyString, pokerRoom);
            }

            return players.SortPlayer(textParse.TitleString[1].Split(_settingword.NumberSymbol)[1].Substring(0, 1))
                .FillPosition();
        }

        private SerPlayerHand GetPlayerHand(string raw, string[] bodystr, IPokerRoom pokerRoom)
        {
            MatchCollection reg = Regex.Matches(raw, @"(\d): (.+) \(\$?([0-9]+\.?[0-9]*)",
                RegexOptions.Singleline | RegexOptions.IgnoreCase);
            var ph = new SerPlayerHand
            {
                Player = new SerPlayer
                {
                    Nick = reg[0].Groups[2].Value,
                    PokerRoom = pokerRoom
                },
                CountChips = reg[0].Groups[3].Value.ParseCount(),
                NumberPosition = reg[0].Groups[1].Value
            };
            for (int j = bodystr.Length - 1; j > 0; j--)
            {
                if (bodystr[j].Contains(ph.Player.Nick))
                {
                    var crds = ConverterCard.GetCards(bodystr[j]);
                    if (crds.Count() > 2)
                        throw new SerializationException();
                    foreach (var card in crds)
                    {
                        ph.HandCard.Add(card);
                    }

                    if (crds.Any())
                        break;
                }
            }

            return ph;
        }

        private Collection<string> CreateStageString(string EndWrods, ref string[] body, ref int count)
        {
            var strstage = new Collection<string>();
            for (int i = count; i < body.Length; i++)
            {
                if (!string.IsNullOrEmpty(EndWrods) && SerializationHelper.CompareString(body[i], EndWrods))
                {
                    break;
                }

                strstage.Add(body[i]);
            }

            count += strstage.Count;
            return strstage;
        }

        protected virtual TextParse CreateTextParse(string raw)
        {
            return TextParse.CreateTextParse(raw, new PokerStarsSettingWord());
        }


        private TableSerModel GetTable(string[] raw, string name)
        {
            var table = new TableSerModel
            {
                Identifier = name,
                PokerRoom = CreatePokerRoom(raw)
            };

            if (raw[1] == _converterSetting.Zoom)
            {
                if (raw[5][0] == _settingword.NumberSymbol)
                {
                    table.Tournament = new TournamentSerModel
                    {
                        Identifier = raw[5].TrimStart(_settingword.NumberSymbol).TrimEnd(','),
                        Buy_in = CalcBuy_in(raw[6])
                    };
                }

                table.TypeOfGame = TypeOfGame.Zoom;
            }
            else
            {
                if (raw[4][0] == _settingword.NumberSymbol)
                {
                    table.Tournament = new TournamentSerModel
                    {
                        Identifier = raw[4].TrimStart(_settingword.NumberSymbol).TrimEnd(','),
                        Buy_in = CalcBuy_in(raw[5])
                    };
                }

                table.TypeOfGame = TypeOfGame.Cash;
            }

            table.TypeOfCurrency = GetCurencyType(raw);
            return table;
        }

        private HandSerModel GetHand(string[] raw)
        {
            var str = raw[0].TrimStart().Split(" ");

            var hand = new HandSerModel
            {
                Date = CreateDate(raw[0]),
                Table = GetTable(str, raw[1].Split('\'')[1]),
                Identifier = str[2].TrimStart(_settingword.NumberSymbol).TrimEnd(':')
            };
            return hand;
        }

        private decimal CalcBuy_in(string calcstring)
        {
            var splt = calcstring.Split('+');
            decimal sum = 0;
            for (int i = 0; i < splt.Length; i++)
            {
                MatchCollection reg = Regex.Matches(splt[i], @".?([0-9]+\.?[0-9]*)",
                    RegexOptions.Singleline | RegexOptions.IgnoreCase);
                sum += reg[0].Groups[1].Value.ParseCount();
            }

            return sum;
        }

        private TypeOfCurrency GetCurencyType(string[] raw)
        {
            foreach (var str in raw.Where(x => x.Length > 2))
            {
                switch (str)
                {
                    case "USD":
                    case "USD)":
                        return TypeOfCurrency.USD;
                    case "EURO":
                    case "EURO)":
                        return TypeOfCurrency.Euro;
                }
            }

            return TypeOfCurrency.Chips;
        }

        protected virtual PokerRoomSerModel CreatePokerRoom(string[] raw) => new PokerRoomSerModel(raw[0]);

        DateTime CreateDate(string raw)
        {
            var date = raw.Replace("[", "").Split(" ").Reverse().ToArray();
            return SerializationHelper.CreateDate(date[2] + " " + date[1], _converterSetting.FormatProvider);
        }
    }
}