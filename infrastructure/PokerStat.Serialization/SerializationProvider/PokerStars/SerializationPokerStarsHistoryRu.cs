﻿using PokerStat.Serialization.Converter;
using PokerStat.Serialization.Model;

namespace PokerStat.Serialization.SerializationProvider.PokerStars
{
    internal sealed class SerializationPokerStarsHistoryRu : SerializationPokerStarsHistory
    {
        public SerializationPokerStarsHistoryRu(string source) : base(new ConverterSettingRU(), new PokerStarsSettingWordRU(), source)
        {
        }

        protected override TextParse CreateTextParse(string raw)
        {
            return TextParse.CreateTextParse(raw, new PokerStarsSettingWordRU(), " делает ");
        }

        protected override PokerRoomSerModel CreatePokerRoom(string[] raw) => new PokerRoomSerModel(raw[1]);
    }
}