﻿using System.Collections.ObjectModel;
using System.Linq;

namespace PokerStat.Serialization.SerializationProvider
{
    internal sealed class TextParse
    {
        readonly string[] _raw;
        readonly string[] _removestrings;

        public string[] TitleString => new[] { _raw[0], _raw[1] };
        public string[] PlayerString { get; private set; }
        public string[] BodyString { get; private set; }

        private TextParse(string[] raw, string[] removestring)
        {
            _raw = raw;
            _removestrings = removestring;
        }

        public static TextParse CreateTextParse(string raw, ISettingWord settingWord, params string[] removestring)
        {
            if (string.IsNullOrEmpty(raw) ||
                string.IsNullOrEmpty(settingWord.Seat) ||
                string.IsNullOrEmpty(settingWord.HoleCard) ||
                string.IsNullOrEmpty(settingWord.EndOfFile))
                return null;

            var spl = raw.Split("\n").Where(x => !string.IsNullOrEmpty(x));
            var arrspl = spl.ToArray();
            if (arrspl.Length > 3)
            {
                var tp =  new TextParse(arrspl, removestring);
                if (!tp.CreatePlayerHandString(settingWord.Seat, settingWord.HoleCard) || !tp.CreateBody(settingWord.Seat,settingWord.EndOfFile))
                    return null;
                return tp;
            }
            return null;
        }
        private bool CreatePlayerHandString(string seat,string holecard)
        {
            var body = new Collection<string>();
            bool _wordfound = false;
            for (int i = 2; i < _raw.Length; i++)
            {
                if (SerializationHelper.CompareString(_raw[i], seat))
                    body.Add(Remove(_raw[i]));
                if (SerializationHelper.CompareString(_raw[i], holecard))
                { _wordfound = true; break; }
            }
            if (body.Count == 0 || !_wordfound)
                return false;
            PlayerString = body.ToArray();
            return true;
        }

        private string Remove(string str)
        {
            for (int i = 0; i < _removestrings.Length; i++)
            {
                if (str.Contains(_removestrings[i]))
                    str = str.Replace(_removestrings[i].TrimEnd(), "");
            }
            return str;
        }

        private bool CreateBody(string seat,string endoffile)
        {
            var body = new Collection<string>();
            bool wordfound = false;
            for (int i = 2; i < _raw.Length; i++)
            {
                if (SerializationHelper.CompareString(_raw[i], seat))
                    continue;
                if (SerializationHelper.CompareString(_raw[i], endoffile))
                { wordfound = true; break; }
                body.Add(Remove(_raw[i]));
            }
            if (body.Count == 0 || !wordfound)
                return false;
            BodyString = body.ToArray();
            return true;
        }
    }
}
