﻿namespace PokerStat.Serialization.SerializationProvider
{
    public interface ISettingWord
    {
        char NumberSymbol { get; }
        string Seat {get;}
        string HoleCard { get; }
        string FLOP { get; }
        string TURN { get; }
        string RIVER { get; }
        string ShowDown { get; }
        string EndOfFile { get; }
    }
}
