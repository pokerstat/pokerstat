﻿using System;
using System.Linq;
using PokerStat.Model;
using PokerStat.Serialization.Model;

namespace PokerStat.Serialization.SerializationProvider
{
    internal static class PlayerHelper
    {
        internal static SerPlayerHand[] SortPlayer(this SerPlayerHand[] players, string startposition)
        {
            bool _issortwitherror = true;
            int start;
            for (start = 0; start < players.Length; start++)
            {
                if (players[start].NumberPosition == startposition)
                {
                    _issortwitherror = false;
                    break;
                }
            }
            if (_issortwitherror)
                throw new SerializationException();
            if (players.Length == 2) //на таких столах начальная позиция = малому блайнду
                return ShortTable(players, startposition);
            var _players = new SerPlayerHand[players.Length];
            Array.Copy(players, start+1, _players, 0, players.Length-start-1);
            Array.Copy(players, 0, _players, players.Length - start - 1, start+1);
            return _players;
        }
        internal static IPlayerHand[] FillPosition(this SerPlayerHand[] players)
        {
            players[0].Position = Position.SmallBlind;
            players[1].Position = Position.BigBlind;
            for (byte i = 0; i < players.Length - 2; i++)
            {
                players[players.Length - 1 - i].Position = (Position)(8 - i);//всего позиций 10 и 2 из них мы уже заюзали (23-24 строчка)
            }
            return players;
        }

        static SerPlayerHand[] ShortTable(SerPlayerHand[] players, string startposition)
        {
            var _players = new SerPlayerHand[2];
            _players[0] = players.Single(x => x.NumberPosition == startposition);
            _players[1] = players.Single(x => x.NumberPosition != startposition);
            return _players;
        }
    }
}
