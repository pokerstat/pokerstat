﻿using System;
using System.Globalization;
using System.IO;

namespace PokerStat.Serialization
{
    internal static class SerializationHelper
    {

        public static string ReadStream(this Stream stream)
        {
            stream.Position = 0;
            using(var sr = new StreamReader(stream))
                return sr.ReadToEnd();
        }
        public static bool CompareString(string str1, string str2)
        {
            return str1.Length >= str2.Length && str1.Substring(0, str2.Length) == str2;
        }

        public static DateTime CreateDate(string raw, IFormatProvider format)
        {
            if (DateTime.TryParse(raw, format, DateTimeStyles.None, out DateTime result))
            {
                return result;
            }

            return DateTime.UtcNow;
        }
        public static decimal ParseCount(this string value)
        {
            return decimal.Parse(value, CultureInfo.InvariantCulture);
        }

    }
}
