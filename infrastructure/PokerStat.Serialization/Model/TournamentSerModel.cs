using System;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class TournamentSerModel : ITournament
    {
        public Guid Id => Guid.Empty;
        public string Identifier { get ; set ; }
        
        public decimal Buy_in { get; set; }
    }
}
