﻿using System;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class SerStageAction : IStageAction
    {
        public IPlayerHand PlayerHand { get; set; }

        public PokerStat.Model.Action Action { get; set; }
        public byte Order { get; set; }

        public decimal? Count { get; set; }

        public Guid Id => Guid.Empty;
    }
}
