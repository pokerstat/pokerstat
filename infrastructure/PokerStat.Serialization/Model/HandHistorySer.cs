﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class HandHistorySer : IHandStage
    {
        public Stages Stage { get; set; }

        public ICollection<ICard> Cards { get; } = new Collection<ICard>();

        public IEnumerable<IStageAction> Actions { get; set; }

        public Guid Id => Guid.Empty;
    }
}
