using System;
using System.Collections.Generic;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class HandSerModel : IHand
    {
        public HandSerModel(IHand hand)
        {
            Identifier = hand.Identifier;
            Date = hand.Date;
            Table = hand.Table;
        }
        public HandSerModel()
        {

        }
        public string Identifier { get; set; }

        public DateTime Date { get; set; }
        
        public ITable Table { get; set; }

        public IEnumerable<IHandStage> StageHands { get; set; }

        public Guid Id => Guid.Empty;
        
    }
}
