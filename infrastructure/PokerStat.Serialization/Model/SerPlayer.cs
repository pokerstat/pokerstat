﻿using System;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class SerPlayer : IPlayer
    {
        public string Nick {get; set;}

        public IPokerRoom PokerRoom { get; set; }

        public Guid Id =>Guid.Empty;
    }
}
