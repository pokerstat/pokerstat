﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class SerPlayerHand : IPlayerHand
    {
        public Position Position { get; set; }

        public decimal CountChips { get; set; }

        public IPlayer Player { get; set; }

        public ICollection<ICard> HandCard { get; } = new Collection<ICard>();

        public Guid Id => Guid.Empty;

        public string NumberPosition { get; set; } 
    }
}
