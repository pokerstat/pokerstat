using System;
using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class TableSerModel:ITable
    {
        public ITournament Tournament { get; set; }
        
        public TypeOfCurrency TypeOfCurrency { get; set; }
        public TypeOfGame TypeOfGame { get; set; }
        public IPokerRoom PokerRoom { get; set; }
        public Guid Id => Guid.Empty;
        public string Identifier { get; set; }
    }
}
