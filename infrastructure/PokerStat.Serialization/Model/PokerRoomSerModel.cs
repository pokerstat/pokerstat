using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class PokerRoomSerModel:IPokerRoom
    {
        public PokerRoomSerModel(string name)
        {
            Name = name;
        }
        public short Id => default(short);
        public string Name { get; set; }
    }
}
