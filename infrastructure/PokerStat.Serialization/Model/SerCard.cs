﻿using PokerStat.Model;

namespace PokerStat.Serialization.Model
{
    internal sealed class SerCard: ICard
    {
        public byte Id => 0;

        public string Name { get; set; }

        public char ShortName { get; set; }

        public char SuitCode { get; set; }

        public string SuitName { get; set; }
        public Color Color { get; }
        public Color AlternativeColor { get; }
    }
}
