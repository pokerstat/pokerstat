using System;

namespace PokerStat.Serialization
{
    public class SerializationException : Exception
    {
        public SerializationException() : base("string is corrupted")
        {
        }
    }
}