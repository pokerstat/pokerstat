using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Results;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.FluentValidation.Extensions
{
    public static class FluentValidationExtensions
    {
        public static void ThrowValidationException(this ValidationResult validationResult)
        {
            if (validationResult.IsValid)
            {
                return;
            }

            throw new PokerStatException(validationResult.ToError());
        }

        
        
        private static PokerStatError ToError(this ValidationResult validationResult)
        {
            var error = new PokerStatError(PokerStatErrorCodes.Validation);

            error.Messages = validationResult.Errors.Select(e => new ErrorMessage()
            {
                Key = e.PropertyName,
                Value = e.ErrorMessage
            }).ToArray();

            return error;
        }
        
        public static void AddRuleToValidateDateForClickHouse<T>(this AbstractValidator<T> validator, Expression<Func<T, string>> expression)
        {
            validator.RuleFor(expression).Must(ValidateDateStringForClickHouse).WithMessage("Field has incorrect format");
        }
        
        //click house хавает дату только в таком формате '2000-04-18 00:00:00' или '2000-04-18'
        private static bool ValidateDateStringForClickHouse(string date)
        {
            var _d = date.Trim();
            if (!DateTime.TryParse(_d, out var d))
            {
                return false;
            }
            if (_d.Length == 10)
            {
                Regex r = new Regex("^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$", RegexOptions.Singleline|RegexOptions.IgnoreCase);
                return r.IsMatch(_d);
            }

            if (_d.Length == 19)
            {
                Regex r = new Regex(@"^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01]) ([0-1]\d|2[0-3])(:[0-5]\d){2}$", RegexOptions.Singleline|RegexOptions.IgnoreCase);
                return r.IsMatch(_d);
            }
            return false;
        }
    
        public static void AddRuleOnEmpty<T>(this AbstractValidator<T> validator, Expression<Func<T, string>> expression)
        {
            validator.RuleFor(expression).NotNull().WithMessage("Field can't be empty");
            validator.RuleFor(expression).NotEmpty().WithMessage("Field can't be empty");
        }
    }
}