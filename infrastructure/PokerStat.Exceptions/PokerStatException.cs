using System;

namespace PokerStat.Exceptions
{
    public sealed class PokerStatException : Exception
    {
        public PokerStatException(PokerStatError error) : base(error.Description)
        {
            Error = error;
        }

        public PokerStatException(PokerStatError error, Exception innerException) : base(error.Description, innerException)
        {
            Error = error;
        }

        public PokerStatError Error { get; }
    }
}