namespace PokerStat.Exceptions
{
    public class ErrorMessage
    {
        public string Key { get; set; }

        public string Value { get; set; }

    }
}