using Microsoft.Extensions.Logging;
using PokerStat.Exceptions.Attributes;

namespace PokerStat.Exceptions.ErrorCodes
{
    public enum PokerStatErrorCodes
    {
        [PokerStatErrorCodeDescription("Common error", LogLevel.Information)]
        Common,
        [PokerStatErrorCodeDescription("An error occurred during database initialization", LogLevel.Critical)]
        DataBaseInitializer,
        [PokerStatErrorCodeDescription("The request parameters contain errors", LogLevel.Error)]
        Validation,
        [PokerStatErrorCodeDescription("The request finished with error", LogLevel.Error)]
        ClickHouse,
        [PokerStatErrorCodeDescription("The microservice answered with error", LogLevel.Error)]
        Microservices,

    }
}