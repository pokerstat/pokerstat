using System.ComponentModel;
using Microsoft.Extensions.Logging;

namespace PokerStat.Exceptions.Attributes
{
    public sealed class PokerStatErrorCodeDescriptionAttribute : DescriptionAttribute
    {
        public PokerStatErrorCodeDescriptionAttribute(string description, LogLevel defaultLogLevel = LogLevel.Debug) : base(description)
        {
            DefaultLogLevel = defaultLogLevel;
        }
        
        public LogLevel DefaultLogLevel { get; }
    }
}