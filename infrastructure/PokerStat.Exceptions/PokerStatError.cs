using System.Runtime.Serialization;
using Microsoft.Extensions.Logging;
using PokerStat.Exceptions.ErrorCodes;
using PokerStat.Exceptions.Extensions;

namespace PokerStat.Exceptions
{
    public sealed class PokerStatError
    {
        public PokerStatError(PokerStatErrorCodes code, LogLevel? logLevel = null)
        {
            Code = code;
            LogLevel = logLevel ?? code.GetDefaultLogLevel();
        }

        public PokerStatErrorCodes Code { get; }

        [IgnoreDataMember] public LogLevel LogLevel { get; }

        public string Description => $"{Code.GetDescription()}";

        public ErrorMessage[] Messages { get; set; }
    }
}