using System.Linq;
using Microsoft.Extensions.Logging;
using PokerStat.Exceptions.Attributes;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.Exceptions.Extensions
{
    public static class PSCodesExtensions
    {
        public static string GetDescription(this PokerStatErrorCodes pokerStatErrorCode)
        {
            var type = pokerStatErrorCode.GetType();
            var memInfo = type.GetMember(pokerStatErrorCode.ToString());
            var attribute = memInfo[0]
                .GetCustomAttributes(typeof(PokerStatErrorCodeDescriptionAttribute), false)
                .Cast<PokerStatErrorCodeDescriptionAttribute>()
                .FirstOrDefault();
            
            return attribute?.Description;
        }

        public static LogLevel GetDefaultLogLevel(this PokerStatErrorCodes pokerStatErrorCode)
        {
            var type = pokerStatErrorCode.GetType();
            var memInfo = type.GetMember(pokerStatErrorCode.ToString());
            var attribute = memInfo[0]
                .GetCustomAttributes(typeof(PokerStatErrorCodeDescriptionAttribute), false)
                .Cast<PokerStatErrorCodeDescriptionAttribute>()
                .FirstOrDefault();
            
            return attribute?.DefaultLogLevel ?? LogLevel.Debug;
        }
    }
}