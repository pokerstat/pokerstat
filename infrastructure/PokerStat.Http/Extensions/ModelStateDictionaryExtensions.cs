using System.Linq;
using FluentValidation;
using FluentValidation.Results;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.HttpContracts.Extensions
{
    public static class ValidationResultExtensions
    {
        public static Response ToResponse(this ValidationResult validationResult)
        {
            var response = new Response();
            
            if (validationResult.IsValid)
                return response;

            response.Errors.Add(new PokerStatError(PokerStatErrorCodes.Validation)
            {
                Messages = validationResult.Errors.Select(er => new ErrorMessage()
                {
                    Key = er.PropertyName,
                    Value = er.ErrorMessage
                }).ToArray()
            });

            return response;
        }
        public static bool IsValid(this IValidator validator, object request, out Response errorResponse)
        {
            var validationResult = validator.Validate(request);
            errorResponse = new Response();

            if (validationResult.IsValid) 
                return true;
            errorResponse.Errors.Add(new PokerStatError(PokerStatErrorCodes.Validation)
            {
                Messages = validationResult.Errors.Select(er => new ErrorMessage()
                {
                    Key = er.PropertyName,
                    Value = er.ErrorMessage
                }).ToArray()
            });

            return false;

        }

    }
}