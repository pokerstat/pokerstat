using System.Collections.Generic;
using PokerStat.Exceptions;

namespace PokerStat.HttpContracts
{
    public class Response
    {
        public List<PokerStatError> Errors { get; set; } = new List<PokerStatError>();

        public bool Success => Errors == null || Errors.Count == 0;
    }
    
    public class Response<T>: Response
    {
        private T _data;

        public T Data
        {
            get => Success ? _data : default(T);
            set => _data = value;
        }
        
    }
}