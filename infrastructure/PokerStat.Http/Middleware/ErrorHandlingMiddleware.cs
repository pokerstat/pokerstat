using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.HttpContracts.Middleware
{
    public class ErrorHandlingMiddleware
    { private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
      {
         ContractResolver = new CamelCasePropertyNamesContractResolver(),
         NullValueHandling = NullValueHandling.Ignore
      };

      private readonly RequestDelegate next;
      private readonly ILogger logger;

      public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
      {
         this.next = next;

         logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
      }

      public async Task Invoke(HttpContext context)
      {
         try
         {
            await next(context);
         }
         catch (Exception ex)
         {
            if (ex is OperationCanceledException && context.RequestAborted.IsCancellationRequested)
            {
               logger.LogInformation("Request was cancelled");
               return;
            }

            if (context.Request.Path.HasValue)
            {
               throw;
            }

            var pokerStatError = new PokerStatError(PokerStatErrorCodes.Common);

            if (ex is PokerStatException pokerStatException)
            {
               pokerStatError = pokerStatException.Error;
               //TODO need to add logging
            }
            else
            {
               //TODO need to add logging
            }

            var responseModel = new Response();
            responseModel.Errors.Add(pokerStatError);

            var statusCode = HttpStatusCode.OK;
            if (pokerStatError.Code == PokerStatErrorCodes.Validation)
            {
               statusCode = HttpStatusCode.BadRequest;
            }

            await WriteErrorResponseAsync(context, responseModel, statusCode);
         }
      }

      private Task WriteErrorResponseAsync<TResponseModel>(HttpContext context, TResponseModel responseModel, HttpStatusCode statusCode)
      {
         try
         {
            var responseModelJson = JsonConvert.SerializeObject(responseModel, JsonSettings);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;

            return context.Response.WriteAsync(responseModelJson);
         }
         catch (Exception ex)
         {
            //TODO need to add logging
            return Task.CompletedTask;
         }
      }
    }
}