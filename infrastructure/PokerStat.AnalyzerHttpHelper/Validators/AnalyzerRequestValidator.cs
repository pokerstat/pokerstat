using System;
using FluentValidation;
using FluentValidation.Attributes;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.FluentValidation.Extensions;

namespace PokerStat.AnalyzerHttpHelper.Validators
{
    [Validator(typeof(AnalyzerRequestValidator))]
    public class AnalyzerRequestValidator : AbstractValidator<AnalyzerRequest>
    {
        public AnalyzerRequestValidator()
        {
            //player_id
            RuleFor(x => x.Player_id).Must(value => value != Guid.Empty.ToString()).WithMessage("Player_id is empty id");
            RuleFor(x => x.Player_id).Must(value => Guid.TryParse(value, out var guid)).WithMessage("Player_id has incorrect format");
            
            //pokerroom
            this.AddRuleOnEmpty(x=>x.PokerRoom);
            RuleFor(x => x.PokerRoom).Must(value =>ushort.TryParse(value, out var typ)).WithMessage("PokerRoom has incorrect format");
            
            //typeofgame
            this.AddRuleOnEmpty(x=>x.TypeOfGame);
            RuleFor(x => x.TypeOfGame).Must(value => byte.TryParse(value, out var typ)).WithMessage("TypeOfGame has incorrect format");
            
            //istournament
            this.AddRuleOnEmpty(x=>x.IsTournamnet);
            RuleFor(x => x.IsTournamnet).Must(value => value=="0"||value=="1").WithMessage("IsTournamnet has incorrect format");
        }
    }
}