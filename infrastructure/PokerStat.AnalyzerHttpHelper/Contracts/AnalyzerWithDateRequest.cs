
namespace PokerStat.AnalyzerHttpHelper.Contracts
{
    public sealed class AnalyzerWithDateRequest:AnalyzerRequest
    {
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
    }
}