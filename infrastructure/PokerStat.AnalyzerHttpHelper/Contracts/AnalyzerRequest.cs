using System;

namespace PokerStat.AnalyzerHttpHelper.Contracts
{
    public class AnalyzerRequest
    {
        public string Player_id { get; set; }
        public string IsTournamnet { get; set; }
        public string TypeOfGame { get; set; }
        public string PokerRoom { get; set; }
    }
}