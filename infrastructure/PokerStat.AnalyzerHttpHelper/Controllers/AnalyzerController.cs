using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.AnalyzerHttpHelper.Validators;
using PokerStat.HttpContracts;
using PokerStat.HttpContracts.Extensions;

namespace PokerStat.AnalyzerHttpHelper.Controllers
{
    [ApiController]
    [Route("api/analyzer")]
    [Produces("application/json")]
    public abstract class AnalyzerController : ControllerBase, IAnalyzerController
    {
        private readonly IValidatorFactory _validatorFactory;

        protected AnalyzerController(
            IValidatorFactory validatorFactory)
        {
            _validatorFactory = validatorFactory;
        }
        //[HttpGet(t)]
        [HttpGet("room/{PokerRoom}/typeofgame/{TypeOfGame}/istournament/{IsTournamnet}/player/{player_id}/count")]
        [Produces("application/json", Type = typeof(AnalyzerResponse))]
        public async Task<IActionResult> GetAsync([FromRoute] AnalyzerRequest request)
        {
            if (!_validatorFactory.GetValidator(typeof(AnalyzerRequestValidator)).IsValid(request, out var errorResponse))
            {
                return BadRequest(errorResponse);
            }

            var value = await InvokeAsync(request);

            return Ok(new AnalyzerResponse
            {
                Data = value
            });

        }
        [HttpGet("room/{PokerRoom}/typeofgame/{TypeOfGame}/istournament/{IsTournamnet}/player/{player_id}/DateStart/{DateStart}/DateEnd/{DateEnd}/count")]
        [Produces("application/json", Type = typeof(AnalyzerResponse))]
        public async Task<IActionResult> GetWithDateAsync([FromRoute] AnalyzerWithDateRequest request)
        {
            if (!_validatorFactory.GetValidator(typeof(AnalyzerWithDateRequestValidator)).IsValid(request, out var errorResponse))
            {
                return BadRequest(errorResponse);
            }

            var value = await InvokeAsync(request);

            return Ok(new AnalyzerResponse
            {
                Data = value
            });
        }
        protected abstract Task<decimal> InvokeAsync(AnalyzerRequest request);

        protected abstract Task<decimal> InvokeAsync(AnalyzerWithDateRequest request);
    }
}