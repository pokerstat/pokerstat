using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PokerStat.AnalyzerHttpHelper.Contracts;

namespace PokerStat.AnalyzerHttpHelper.Interfaces
{
    public interface IAnalyzerController
    {
        Task<IActionResult> GetAsync([FromRoute] AnalyzerRequest request);
        Task<IActionResult> GetWithDateAsync([FromRoute] AnalyzerWithDateRequest request);
    }
}