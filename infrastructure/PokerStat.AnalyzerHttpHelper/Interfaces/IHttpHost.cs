﻿using System;
namespace PokerStat.AnalyzerHttpHelper.Interfaces
{
    public interface IHttpHost
    {
        string Host { get; set; }
    }
}
