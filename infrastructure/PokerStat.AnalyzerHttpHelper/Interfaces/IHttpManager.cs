﻿
using System.Threading.Tasks;
using PokerStat.AnalyzerHttpHelper.Contracts;

namespace PokerStat.AnalyzerHttpHelper.Interfaces
{
    public interface IHttpManager
    {
        Task<decimal> SendQueryAsync(AnalyzerWithDateRequest request);
        Task<decimal> SendQueryAsync(AnalyzerRequest request);
    }
}
