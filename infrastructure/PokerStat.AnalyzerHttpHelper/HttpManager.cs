﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.Exceptions;

namespace PokerStat.AnalyzerHttpHelper
{
    public class HttpManager : IHttpManager
    {
        readonly IHttpHost _httpHost;
        readonly IHttpClientFactory _clientFactory;
        public HttpManager(IHttpClientFactory clientFactory, IHttpHost httpHost)
        {
            _httpHost = httpHost;
            _clientFactory = clientFactory;
        }

        public async Task<decimal> SendQueryAsync(AnalyzerWithDateRequest request)
        {
            //"room/{PokerRoom}/typeofgame/{TypeOfGame}/istournament/{IsTournamnet}/player/{player_id}/DateStart/{DateStart}/DateEnd/{DateEnd}/count"
            var sb = new StringBuilder();
            GenerateCommonUri(request, sb)
                .Append("DateStart").Append(request.DateStart)
                .Append("DateEnd").Append(request.DateStart)
                            .Append("count");

            return await CalculateAsync(sb.ToString());
        }
        public async Task<decimal> SendQueryAsync(AnalyzerRequest request)
        {
            // "room/{PokerRoom}/typeofgame/{TypeOfGame}/istournament/{IsTournamnet}/player/{player_id}/count";
            var sb = new StringBuilder();
            GenerateCommonUri(request, sb)
                            .Append("count");

            return await CalculateAsync(sb.ToString());
        }
        private HttpRequestMessage CreateQuery(string query)
        {
            if (string.IsNullOrEmpty(query))
                throw new ArgumentException("query is empty", nameof(query));

            return new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get
            };
        }

        private async Task<HttpContent> ExecuteAsync(HttpRequestMessage requestMessage)
        {
            var httpClient = _clientFactory.CreateClient(_httpHost.Host);

            var responseMessage = await httpClient.SendAsync(requestMessage);

            if (responseMessage.IsSuccessStatusCode)
            {
                return responseMessage.Content;
            }

            var pokerStatError = new PokerStatError(Exceptions.ErrorCodes.PokerStatErrorCodes.Microservices);
            pokerStatError.Messages = new[]
            {
                new ErrorMessage
                {
                    Key = responseMessage.StatusCode.ToString(),
                    Value = responseMessage.ReasonPhrase
                }
            };
            throw new PokerStatException(pokerStatError);
        }



        private async Task<decimal> CalculateAsync(string sb)
        {
            var content = await ExecuteAsync(CreateQuery(sb));
            var res_string = await content.ReadAsStringAsync();
            PokerStatError pokerStatError;
            if (string.IsNullOrEmpty(res_string))
            {
                pokerStatError = new PokerStatError(Exceptions.ErrorCodes.PokerStatErrorCodes.Microservices);
                pokerStatError.Messages = new[]
                {
                new ErrorMessage
                {
                    Key = nameof(res_string),
                    Value = "Value is empty"
                }
            };
                throw new PokerStatException(pokerStatError);
            }
            if (decimal.TryParse(res_string, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal i))
                return i;

            pokerStatError = new PokerStatError(Exceptions.ErrorCodes.PokerStatErrorCodes.Microservices);
            pokerStatError.Messages = new[]
            {
                new ErrorMessage
                {
                    Key = nameof(res_string),
                    Value = "Value is not digital"
                }
            };
            throw new PokerStatException(pokerStatError);


        }

        private StringBuilder GenerateCommonUri(AnalyzerRequest request, StringBuilder sb)
        {
            return sb.Append(_httpHost.Host)
                            .Append("api/analyzer")
                            .Append("room").Append(request.PokerRoom)
                            .Append("typeofgame").Append(request.TypeOfGame)
                            .Append("istournament").Append(request.IsTournamnet)
                            .Append("player").Append(request.Player_id);
        }


    }
}
