using System;
using System.Data;

namespace PokerStat.DataBaseHelpers
{
    public sealed class PokerStatDataBaseParameter
    {
        private PokerStatDataBaseParameter(string name, object value, DbType dbType)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name), "Name of parameter is empty");
            Value = value ?? throw new ArgumentNullException(nameof(value), "Value is null");
            ParameterName = $"{name.Trim().ToLower()}";
            DbType = dbType;
        }

        public static PokerStatDataBaseParameter Create(string name, object value)
        {
            switch (value)
            {
                case Guid _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Guid);
                case string _:
                    return new PokerStatDataBaseParameter(name, value, DbType.String);
                case byte _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Byte);
                case decimal _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Decimal);
                case short _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Int16);
                case int _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Int32);
                case DateTime _:
                    return new PokerStatDataBaseParameter(name, value, DbType.DateTime2);
                case byte[] _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Binary);
                case bool _:
                    return new PokerStatDataBaseParameter(name, value, DbType.Boolean);
                default:
                    throw new NotSupportedException($"Type of value not supported (parameter name is {name})");
            }
        }

        public DbType DbType { get; }
        public string ParameterName { get; }
        public object Value { get; }
    }
}