﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace PokerStat.DataBaseHelpers
{
    public static class ProviderHelper
    {
        public static async Task<T> ExecuteScalarAsync<T>(this IDbConnection dbConnection, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbConnection, nameprocedure, parameters))
            {
                return await Task.FromResult((T) Convert.ChangeType(com.ExecuteScalar(), typeof(T)));
            }
        }

        public static T ExecuteScalar<T>(this IDbConnection dbConnection, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbConnection, nameprocedure, parameters))
            {
                return (T) Convert.ChangeType(com.ExecuteScalar(), typeof(T));
            }
        }

        public static async Task<T> ExecuteScalarAsync<T>(this IDbTransaction dbTransaction, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbTransaction, nameprocedure, parameters))
            {
                return await Task.FromResult((T) Convert.ChangeType(com.ExecuteScalar(), typeof(T)));
            }
        }

        public static T ExecuteScalar<T>(this IDbTransaction dbTransaction, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbTransaction, nameprocedure, parameters))
            {
                return (T) Convert.ChangeType(com.ExecuteScalar(), typeof(T));
            }
        }


        public static async Task ExecuteNonQueryAsync(this IDbConnection dbConnection, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbConnection, nameprocedure, parameters))
            {
                await Task.FromResult(com.ExecuteNonQuery());
            }
        }

        public static async Task ExecuteNonQueryAsync(this IDbTransaction dbTransaction, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbTransaction, nameprocedure, parameters))
            {
                await Task.FromResult(com.ExecuteNonQuery());
            }
        }

        public static async Task<IDataReader> ExecuteReaderAsync(this IDbConnection dbConnection, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            using (var com = CreateCommand(dbConnection, nameprocedure, parameters))
            {
                return await Task.FromResult(com.ExecuteReader(CommandBehavior.SingleResult));
            }
        }

        public static IDataReader ExecuteReader(this IDbConnection dbConnection, string nameprocedure)
        {
            using (var com = CreateCommand(dbConnection, nameprocedure, new PokerStatDataBaseParameter[0]))
            {
                return com.ExecuteReader(CommandBehavior.SingleResult);
            }
        }

        #region private

        private static IDbCommand CreateCommand(IDbConnection dbConnection, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            var com = dbConnection.CreateCommand();
            FillCommand(nameprocedure, parameters, com);
            return com;
        }

        private static void FillCommand(string query, PokerStatDataBaseParameter[] parameters, IDbCommand com)
        {
            if (string.IsNullOrEmpty(query))
                throw new ArgumentNullException(nameof(query), "is empty");
            com.CommandText = query;
            com.CommandType = CommandType.StoredProcedure;
            for (ushort i = 0; i < parameters.Length; i++)
            {
                com.Parameters.Add(com.FillParam(parameters[i]));
            }

            com.Prepare();
        }

        private static IDbCommand CreateCommand(IDbTransaction dbTransaction, string nameprocedure,
            PokerStatDataBaseParameter[] parameters)
        {
            var com = dbTransaction.Connection.CreateCommand();
            com.Transaction = dbTransaction;
            FillCommand(nameprocedure, parameters, com);
            return com;
        }

        private static IDataParameter FillParam(this IDbCommand dbCommand, PokerStatDataBaseParameter psparameter)
        {
            if (psparameter == null)
                throw new ArgumentNullException(nameof(psparameter));
            var param = dbCommand.CreateParameter();
            param.DbType = psparameter.DbType;
            param.ParameterName = psparameter.ParameterName;
            param.Value = psparameter.Value;
            return param;
        }

        #endregion
    }
}