namespace PokerStat.Configuration.ConfigSection
{
    public sealed class IdentityServerConfiguration
    {
        public string ConnectionString { get; set; }
        public IdentityServerClient[] Clients { get; set; }
        public bool ForceMigrate { get; set; }
        public string AdminLogin { get; set; }
        public string AdminPassword { get; set; }
        public string BaseRedirectUrl { get; set; }
    }

    public class IdentityServerClient
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Uris { get; set; }
    }
}