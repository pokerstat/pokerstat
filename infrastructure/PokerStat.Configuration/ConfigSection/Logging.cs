﻿using Serilog.Events;

namespace PokerStat.Configuration.ConfigSection
{
    public sealed class Logging
    {
        public LogEventLevel Level { get; set; } = LogEventLevel.Information;
    }
}