namespace PokerStat.Configuration.ConfigSection
{
    public sealed class ConnectionStrings
    {
        public string PokerStatDataBase { get; set; }
    }
}