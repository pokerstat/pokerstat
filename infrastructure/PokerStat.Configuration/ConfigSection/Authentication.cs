namespace PokerStat.Configuration.ConfigSection
{
    public sealed class Authentication
    {
        public string[] AllowedCorsOrigins { get; set; }
        public bool HttpsEnable { get; set; }
    }
}