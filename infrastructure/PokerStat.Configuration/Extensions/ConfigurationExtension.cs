using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace PokerStat.Configuration.Extensions
{
    public static class ConfigurationExtension
    {
        private static readonly Lazy<Dictionary<Type, object>> CacheOfConfigurations = new Lazy<Dictionary<Type, object>>(new Dictionary<Type, object>());

        public static T BindTo<T>(this IConfiguration configuration, string key = null) where T : class, new()
        {
            var type = typeof(T);

            if (CacheOfConfigurations.Value.TryGetValue(type, out var value))
            {
                return value as T;
            }

            key = string.IsNullOrEmpty(key) ? null : $"{key}:";
            var newInstance = new T();
            CacheOfConfigurations.Value.Add(type, newInstance);
            configuration.GetSection($"{key}{type.Name}").Bind(newInstance);

            return newInstance;
        }

        public static string GetFrom<TFrom>(this IConfiguration configuration, string section) where TFrom : class, new()
        {
            return configuration.GetSection(typeof(TFrom).Name)[section];
        }

        public static bool Contains(this IConfiguration configuration, string section)
        {
            return configuration[section] != null;
        }
    }
}