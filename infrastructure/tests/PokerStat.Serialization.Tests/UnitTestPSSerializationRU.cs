﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PokerStat.Model;
using Action = PokerStat.Model.Action;

namespace PokerStat.Serialization.Tests
{
    [TestFixture]
    public class UnitTestPSSerializationRU
    {
        string text =
            "Раздача PokerStars №197665788820: Турнир №2562617404, $1.29+$0.21 USD Холдем Безлимитный - уровень I (10/20) - 16.03.2019 16:54:46 MSK [16.03.2019 9:54:46 ВВ]" +
            Environment.NewLine + "Стол '2562617404 1' 9-max Баттон на месте №1" + Environment.NewLine +
            "Место 1: radzy777 (1502.24 фишек) " + Environment.NewLine +
            "Место 2: anniekipps2 (1500 фишек) " + Environment.NewLine +
            "Место 3: SLAVAMOZG (1500 фишек) " + Environment.NewLine +
            "Место 4: Bootylek (1500 фишек) " + Environment.NewLine +
            "Место 5: Pascal181 (1500 фишек) " + Environment.NewLine + "Место 6: hunteroneeye (1500 фишек) " +
            Environment.NewLine + "Место 7: MrWahnsinn (1500 фишек) " + Environment.NewLine +
            "Место 8: SerGomel (1500 фишек) " + Environment.NewLine + "Место 9: Natalya-Ber (1500 фишек) " +
            Environment.NewLine + "radzy777: ставит анте 3" + Environment.NewLine + "anniekipps2: ставит анте 3" +
            Environment.NewLine + "SLAVAMOZG: ставит анте 3" + Environment.NewLine + "Bootylek: ставит анте 3" +
            Environment.NewLine + "Pascal181: ставит анте 3" + Environment.NewLine + "hunteroneeye: ставит анте 3" +
            Environment.NewLine + "MrWahnsinn: ставит анте 3" + Environment.NewLine + "SerGomel: ставит анте 3" +
            Environment.NewLine + "Natalya-Ber: ставит анте 3" + Environment.NewLine +
            "anniekipps2: ставит малый блайнд 10" + Environment.NewLine + "SLAVAMOZG: ставит большой блайнд 20" +
            Environment.NewLine + "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine + "Карты Bootylek [Ad Kc]" +
            Environment.NewLine + "Bootylek: делает рейз 40 60" + Environment.NewLine + "Pascal181: делает фолд " +
            Environment.NewLine + "hunteroneeye: делает колл 60" + Environment.NewLine + "MrWahnsinn: делает фолд " +
            Environment.NewLine + "SerGomel: делает колл 60" + Environment.NewLine + "Natalya-Ber: делает фолд " +
            Environment.NewLine + "radzy777: делает фолд " + Environment.NewLine + "anniekipps2: делает фолд " +
            Environment.NewLine + "SLAVAMOZG: делает колл 40" + Environment.NewLine + "*** ФЛОП *** [Td 2s 3h]" +
            Environment.NewLine + "SLAVAMOZG: делает бет 180" + Environment.NewLine + "Bootylek: делает фолд " +
            Environment.NewLine + "hunteroneeye: делает колл 180" + Environment.NewLine + "SerGomel: делает фолд " +
            Environment.NewLine + "*** ТЕРН *** [Td 2s 3h] [Qc]" + Environment.NewLine + "SLAVAMOZG: делает бет 100" +
            Environment.NewLine + "hunteroneeye: делает колл 100" + Environment.NewLine +
            "*** РИВЕР *** [Td 2s 3h Qc] [3d]" + Environment.NewLine + "SLAVAMOZG: делает бет 80" +
            Environment.NewLine + "hunteroneeye: делает колл 80" + Environment.NewLine + "*** ВСКРЫТИЕ КАРТ ***" +
            Environment.NewLine + "SLAVAMOZG: открывает [Th 9d] (две пары [десятки и тройки])" + Environment.NewLine +
            "hunteroneeye: открывает [Ac Ts] (две пары [десятки и тройки] - туз (кикер))" + Environment.NewLine +
            "hunteroneeye получил 997 ( банк)" + Environment.NewLine + "*** ИТОГ ***" + Environment.NewLine +
            "Общий банк 997 | Доля 0 " + Environment.NewLine + "Борд [Td 2s 3h Qc 3d]" + Environment.NewLine +
            "Место 1: radzy777 (баттон) сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 2: anniekipps2 (малый блайнд) сделал фолд до Флоп" + Environment.NewLine +
            "Место 3: SLAVAMOZG (большой блайнд) открыл [Th 9d] и проиграл , собрав две пары [десятки и тройки]" +
            Environment.NewLine + "Место 4: Bootylek сделал фолд на Флоп" + Environment.NewLine +
            "Место 5: Pascal181 сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 6: hunteroneeye открыл [Ac Ts] и выиграл (997) , собрав две пары [десятки и тройки]" +
            Environment.NewLine + "Место 7: MrWahnsinn сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 8: SerGomel сделал фолд на Флоп" + Environment.NewLine +
            "Место 9: Natalya-Ber сделал фолд до Флоп (не ставил)" + Environment.NewLine + "" + Environment.NewLine +
            "" + Environment.NewLine + "" + Environment.NewLine +
            "Раздача PokerStars №198193725508: Турнир №2562617404, $1.29+$0.21 USD Холдем Безлимитный - уровень I (10/20) - 16.03.2019 16:55:58 MSK [16.03.2019 9:55:58 ВВ]" +
            Environment.NewLine + "Стол '2562617404 1' 9-max Баттон на месте №2" + Environment.NewLine +
            "Место 1: radzy777 (1497 фишек) " + Environment.NewLine + "Место 2: anniekipps2 (1487 фишек) " +
            Environment.NewLine + "Место 3: SLAVAMOZG (1077 фишек) " + Environment.NewLine +
            "Место 4: Bootylek (1437 фишек) " + Environment.NewLine + "Место 5: Pascal181 (1497 фишек) " +
            Environment.NewLine + "Место 6: hunteroneeye (2074 фишек) " + Environment.NewLine +
            "Место 7: MrWahnsinn (1497 фишек) " + Environment.NewLine + "Место 8: SerGomel (1437 фишек) " +
            Environment.NewLine + "Место 9: Natalya-Ber (1497 фишек) " + Environment.NewLine +
            "radzy777: ставит анте 3" + Environment.NewLine + "anniekipps2: ставит анте 3" + Environment.NewLine +
            "SLAVAMOZG: ставит анте 3" + Environment.NewLine + "Bootylek: ставит анте 3" + Environment.NewLine +
            "Pascal181: ставит анте 3" + Environment.NewLine + "hunteroneeye: ставит анте 3" + Environment.NewLine +
            "MrWahnsinn: ставит анте 3" + Environment.NewLine + "SerGomel: ставит анте 3" + Environment.NewLine +
            "Natalya-Ber: ставит анте 3" + Environment.NewLine + "SLAVAMOZG: ставит малый блайнд 10" +
            Environment.NewLine + "Bootylek: ставит большой блайнд 20" + Environment.NewLine +
            "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine + "Карты Bootylek [Ts 7d]" + Environment.NewLine +
            "Pascal181: делает колл 20" + Environment.NewLine + "hunteroneeye: делает фолд " + Environment.NewLine +
            "MrWahnsinn: делает колл 20" + Environment.NewLine + "SerGomel: делает колл 20" + Environment.NewLine +
            "Natalya-Ber: делает колл 20" + Environment.NewLine + "radzy777: делает фолд " + Environment.NewLine +
            "anniekipps2: делает фолд " + Environment.NewLine + "SLAVAMOZG: делает колл 10" + Environment.NewLine +
            "Bootylek: делает чек " + Environment.NewLine + "*** ФЛОП *** [3d 6s 7h]" + Environment.NewLine +
            "SLAVAMOZG: делает чек " + Environment.NewLine + "Bootylek: делает чек " + Environment.NewLine +
            "Pascal181: делает чек " + Environment.NewLine + "MrWahnsinn: делает чек " + Environment.NewLine +
            "SerGomel: делает бет 40" + Environment.NewLine + "Natalya-Ber: делает фолд " + Environment.NewLine +
            "SLAVAMOZG: делает колл 40" + Environment.NewLine + "Bootylek: делает колл 40" + Environment.NewLine +
            "Pascal181: делает фолд " + Environment.NewLine + "MrWahnsinn: делает фолд " + Environment.NewLine +
            "*** ТЕРН *** [3d 6s 7h] [9s]" + Environment.NewLine + "SLAVAMOZG: делает чек " + Environment.NewLine +
            "Bootylek: делает чек " + Environment.NewLine + "SerGomel: делает чек " + Environment.NewLine +
            "*** РИВЕР *** [3d 6s 7h 9s] [Ac]" + Environment.NewLine + "SLAVAMOZG: делает чек " + Environment.NewLine +
            "Bootylek: делает бет 20" + Environment.NewLine + "SerGomel: делает фолд " + Environment.NewLine +
            "SLAVAMOZG: делает колл 20" + Environment.NewLine + "*** ВСКРЫТИЕ КАРТ ***" + Environment.NewLine +
            "Bootylek: открывает [Ts 7d] (пару [семерки])" + Environment.NewLine + "SLAVAMOZG: сбрасывает руку " +
            Environment.NewLine + "Bootylek получил 307 ( банк)" + Environment.NewLine + "*** ИТОГ ***" +
            Environment.NewLine + "Общий банк 307 | Доля 0 " + Environment.NewLine + "Борд [3d 6s 7h 9s Ac]" +
            Environment.NewLine + "Место 1: radzy777 сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 2: anniekipps2 (баттон) сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 3: SLAVAMOZG (малый блайнд) сбросил [2d 6c]" + Environment.NewLine +
            "Место 4: Bootylek (большой блайнд) открыл [Ts 7d] и выиграл (307) , собрав пару [семерки]" +
            Environment.NewLine + "Место 5: Pascal181 сделал фолд на Флоп" + Environment.NewLine +
            "Место 6: hunteroneeye сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 7: MrWahnsinn сделал фолд на Флоп" + Environment.NewLine + "Место 8: SerGomel сделал фолд на Ривер" +
            Environment.NewLine + "Место 9: Natalya-Ber сделал фолд на Флоп" + Environment.NewLine + "" +
            Environment.NewLine + "" + Environment.NewLine + "" + Environment.NewLine +
            "Раздача PokerStars №198193755445: Турнир №2562617404, $1.29+$0.21 USD Холдем Безлимитный - уровень I (10/20) - 16.03.2019 16:57:10 MSK [16.03.2019 9:57:10 ВВ]" +
            Environment.NewLine + "Стол '2562617404 1' 9-max Баттон на месте №3" + Environment.NewLine +
            "Место 1: radzy777 (1494 фишек) " + Environment.NewLine + "Место 2: anniekipps2 (1484 фишек) " +
            Environment.NewLine + "Место 3: SLAVAMOZG (994 фишек) " + Environment.NewLine +
            "Место 4: Bootylek (1661 фишек) " + Environment.NewLine + "Место 5: Pascal181 (1474 фишек) " +
            Environment.NewLine + "Место 6: hunteroneeye (2071 фишек) " + Environment.NewLine +
            "Место 7: MrWahnsinn (1474 фишек) " + Environment.NewLine + "Место 8: SerGomel (1374 фишек) " +
            Environment.NewLine + "Место 9: Natalya-Ber (1474 фишек) " + Environment.NewLine +
            "radzy777: ставит анте 3" + Environment.NewLine + "anniekipps2: ставит анте 3" + Environment.NewLine +
            "SLAVAMOZG: ставит анте 3" + Environment.NewLine + "Bootylek: ставит анте 3" + Environment.NewLine +
            "Pascal181: ставит анте 3" + Environment.NewLine + "hunteroneeye: ставит анте 3" + Environment.NewLine +
            "MrWahnsinn: ставит анте 3" + Environment.NewLine + "SerGomel: ставит анте 3" + Environment.NewLine +
            "Natalya-Ber: ставит анте 3" + Environment.NewLine + "Bootylek: ставит малый блайнд 10" +
            Environment.NewLine + "Pascal181: ставит большой блайнд 20" + Environment.NewLine +
            "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine + "Карты Bootylek [4c 5c]" + Environment.NewLine +
            "hunteroneeye: делает фолд " + Environment.NewLine + "MrWahnsinn: делает фолд " + Environment.NewLine +
            "SerGomel: делает фолд " + Environment.NewLine + "Natalya-Ber: делает рейз 40 60" + Environment.NewLine +
            "radzy777: делает фолд " + Environment.NewLine + "anniekipps2: делает колл 60" + Environment.NewLine +
            "SLAVAMOZG: делает колл 60" + Environment.NewLine + "Bootylek: делает фолд " + Environment.NewLine +
            "Pascal181: делает фолд " + Environment.NewLine + "*** ФЛОП *** [3c 9h Kd]" + Environment.NewLine +
            "Natalya-Ber: делает бет 60" + Environment.NewLine + "anniekipps2: делает колл 60" + Environment.NewLine +
            "SLAVAMOZG: делает рейз 140 200" + Environment.NewLine + "Natalya-Ber: делает колл 140" +
            Environment.NewLine + "anniekipps2: делает колл 140" + Environment.NewLine +
            "*** ТЕРН *** [3c 9h Kd] [Ac]" + Environment.NewLine + "Natalya-Ber: делает чек " + Environment.NewLine +
            "anniekipps2: делает бет 837" + Environment.NewLine + "SLAVAMOZG: делает колл 731 и олл-ин" +
            Environment.NewLine + "Natalya-Ber: делает фолд " + Environment.NewLine +
            "Неуравненная ставка (106) возвращается игроку anniekipps2" + Environment.NewLine +
            "*** РИВЕР *** [3c 9h Kd Ac] [9d]" + Environment.NewLine + "*** ВСКРЫТИЕ КАРТ ***" + Environment.NewLine +
            "anniekipps2: открывает [9c Ah] (фулл-хаус [девятки и тузы])" + Environment.NewLine +
            "SLAVAMOZG: открывает [Qh Jc] (пару [девятки])" + Environment.NewLine + "anniekipps2 получил 2299 ( банк)" +
            Environment.NewLine + "игрок SLAVAMOZG занял в турнире 9 место" + Environment.NewLine + "*** ИТОГ ***" +
            Environment.NewLine + "Общий банк 2299 | Доля 0 " + Environment.NewLine + "Борд [3c 9h Kd Ac 9d]" +
            Environment.NewLine + "Место 1: radzy777 сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 2: anniekipps2 открыл [9c Ah] и выиграл (2299) , собрав фулл-хаус [девятки и тузы]" +
            Environment.NewLine + "Место 3: SLAVAMOZG (баттон) открыл [Qh Jc] и проиграл , собрав пару [девятки]" +
            Environment.NewLine + "Место 4: Bootylek (малый блайнд) сделал фолд до Флоп" + Environment.NewLine +
            "Место 5: Pascal181 (большой блайнд) сделал фолд до Флоп" + Environment.NewLine +
            "Место 6: hunteroneeye сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 7: MrWahnsinn сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 8: SerGomel сделал фолд до Флоп (не ставил)" + Environment.NewLine +
            "Место 9: Natalya-Ber сделал фолд на Терн" + Environment.NewLine + "" + Environment.NewLine + "" +
            Environment.NewLine + "";

        [Test]
        public void TestGetHistoryHands()
        {
            ISerializationHistory serialization =
                new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text)));
            Assert.IsNotNull(serialization);
            var hands = serialization.GetHands();
            Assert.IsTrue(hands.Count(x => x.Table.Identifier == "2562617404 1") == 3);
            Assert.IsTrue(hands.Count(x => x.Table.PokerRoom.Name == "PokerStars") == 3);
            Assert.IsTrue(hands.ToArray()[0].Date == new DateTime(2019, 03, 16, 9, 54, 46));
            Assert.IsTrue(!hands.Any(x => x.Table.Tournament == null));
            var hand = hands.ToArray()[0];
            Assert.IsNotNull(hand.Table.Tournament);
            var _playershand = hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand);
            Assert.IsTrue(_playershand.Select(x => x.Player.Nick).Distinct().Count() == 9);
            var players = _playershand.Where(x => x.Position == Position.SmallBlind).Select(x => x.Player);
            var st = hand.StageHands.First(x => x.Stage == Stages.Ante);
            Assert.IsNotNull(st.Actions.Single(actions =>
                actions.PlayerHand.Position == Position.SmallBlind &&
                actions.Action == (Action.Posts | Action.SmallBlind)));
            Assert.IsNotNull(st.Actions.Single(actions =>
                actions.PlayerHand.Position == Position.BigBlind &&
                actions.Action == (Action.Posts | Action.BigBlind)));

            Assert.IsNotNull(hand.Table.Tournament);
            Assert.AreEqual(hand.Table.Tournament.Buy_in, 1.50);
            Assert.AreEqual(hand.Table.TypeOfCurrency, TypeOfCurrency.USD);
            Assert.IsTrue(players.Select(x => x.Nick).Distinct().Count() == 1);
            Assert.IsTrue(players.Where(x => x.Nick == "anniekipps2").Distinct().Count() == 1);
            Assert.IsFalse(_playershand.Any(x => x.Position == 0));
            Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Flop).Cards.Count == 3);
            Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Turn).Cards.Count == 4);
            Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.River).Cards.Count == 5);
            Assert.IsFalse(_playershand.Any(x => x.Player.PokerRoom == null));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard.Count == 2);
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard
                .Any(x => x.ShortName == 'A' && x.SuitCode == 'd'));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard
                .Any(x => x.ShortName == 'K' && x.SuitCode == 'c'));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard.Count == 2);
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard
                .Any(x => x.ShortName == 'T' && x.SuitCode == 'h'));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard
                .Any(x => x.ShortName == '9' && x.SuitCode == 'd'));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard.Count == 2);
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard
                .Any(x => x.ShortName == 'A' && x.SuitCode == 'c'));
            Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard
                .Any(x => x.ShortName == 'T' && x.SuitCode == 's'));
            Assert.IsTrue(hand.StageHands.SelectMany(x => x.Actions).Count() == 29);
            Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.ShowDown).Actions.Any(x =>
                x.Action == PokerStat.Model.Action.Collected && x.Count == 997 &&
                x.PlayerHand.Player.Nick == "hunteroneeye"));
            Assert.IsFalse(hand.StageHands.SelectMany(x => x.Actions).Any(x => x.Action == PokerStat.Model.Action.NotFound));
        }

        [Test]
        public void TestGetHistoryHandsWithAllin()
        {
            string text2 =
                "Раздача PokerStars №197665788820: Турнир №2562617404, $1.29+$0.21 USD Холдем Безлимитный - уровень I (10/20) - 16.03.2019 16:54:46 MSK [16.03.2019 9:54:46 ВВ]" +
                Environment.NewLine + "Стол '2562617404 1' 9-max Баттон на месте №9" + Environment.NewLine +
                "Место 1: radzy777 (1500.24 фишек) " + Environment.NewLine +
                "Место 2: anniekipps2 (1500 фишек) " + Environment.NewLine +
                "Место 3: SLAVAMOZG (1500 фишек) " + Environment.NewLine +
                "Место 4: Bootylek (1500 фишек) " + Environment.NewLine +
                "Место 5: pedro esmeralda (1500 фишек) " + Environment.NewLine +
                "Место 6: hunteroneeye (1500 фишек) " + Environment.NewLine +
                //"Место 7: MrWahnsinn (1500 фишек) " + Environment.NewLine + 
                "Место 8: SerGomel (1500 фишек) " + Environment.NewLine +
                "Место 9: Natalya-Ber (1500 фишек) " + Environment.NewLine +
                "radzy777: ставит анте 3" + Environment.NewLine +
                "anniekipps2: ставит анте 3" + Environment.NewLine +
                "SLAVAMOZG: ставит анте 3" + Environment.NewLine +
                "Bootylek: ставит анте 3" + Environment.NewLine +
                "Pascal181: ставит анте 3" + Environment.NewLine +
                "hunteroneeye: ставит анте 3" + Environment.NewLine +
                "MrWahnsinn: ставит анте 3" + Environment.NewLine +
                "SerGomel: ставит анте 3" + Environment.NewLine +
                "Natalya-Ber: ставит анте 3" + Environment.NewLine +
                "radzy777: ставит малый блайнд 10" + Environment.NewLine +
                "anniekipps2: ставит большой блайнд 20" + Environment.NewLine +
                "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine +
                "Карты Bootylek [Ad Kc]" + Environment.NewLine +
                "Bootylek: делает рейз 40 60" + Environment.NewLine +
                "pedro esmeralda: делает фолд " + Environment.NewLine +
                "hunteroneeye: делает колл 60" + Environment.NewLine +
                //"MrWahnsinn: делает фолд " + Environment.NewLine + 
                "SerGomel: делает колл 60" + Environment.NewLine +
                "Natalya-Ber: делает фолд " + Environment.NewLine +
                "radzy777: делает фолд " + Environment.NewLine +
                "anniekipps2: делает фолд " + Environment.NewLine +
                "SLAVAMOZG: делает колл 40" + Environment.NewLine +
                "*** ФЛОП *** [Td 2s 3h]" + Environment.NewLine +
                "SLAVAMOZG: делает бет 180" + Environment.NewLine +
                "Bootylek: делает фолд " + Environment.NewLine +
                "hunteroneeye: делает колл 180" + Environment.NewLine +
                "SerGomel: делает фолд " + Environment.NewLine +
                "*** ТЕРН *** [Td 2s 3h] [Qc]" + Environment.NewLine +
                "SLAVAMOZG: делает бет 100" + Environment.NewLine +
                "hunteroneeye: делает колл 100" + Environment.NewLine +
                "*** РИВЕР *** [Td 2s 3h Qc] [3d]" + Environment.NewLine +
                "SLAVAMOZG: делает бет 80 и олл-ин" + Environment.NewLine +
                "hunteroneeye: делает колл 80" + Environment.NewLine +
                "*** ВСКРЫТИЕ КАРТ ***" + Environment.NewLine +
                "SLAVAMOZG: открывает [Th 9d] (две пары [десятки и тройки])" + Environment.NewLine +
                "hunteroneeye: открывает [Ac Ts] (две пары [десятки и тройки] - туз (кикер))" + Environment.NewLine +
                "hunteroneeye получил 997 ( банк)" + Environment.NewLine + "*** ИТОГ ***" + Environment.NewLine +
                "Общий банк 997 | Доля 0 " + Environment.NewLine + "Борд [Td 2s 3h Qc 3d]" + Environment.NewLine +
                "Место 1: radzy777 (баттон) сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 2: anniekipps2 (малый блайнд) сделал фолд до Флоп" + Environment.NewLine +
                "Место 3: SLAVAMOZG (большой блайнд) открыл [Th 9d] и проиграл , собрав две пары [десятки и тройки]" +
                Environment.NewLine + "Место 4: Bootylek сделал фолд на Флоп" + Environment.NewLine +
                "Место 5: Pascal181 сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 6: hunteroneeye открыл [Ac Ts] и выиграл (997) , собрав две пары [десятки и тройки]" +
                Environment.NewLine + "Место 7: MrWahnsinn сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 8: SerGomel сделал фолд на Флоп" + Environment.NewLine +
                "Место 9: Natalya-Ber сделал фолд до Флоп (не ставил)";

            ISerializationHistory serialization =
                new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text2)));
            Assert.IsNotNull(serialization);
            var hands = serialization.GetHands();
            var hand = hands.ToArray()[0];
            Assert.IsNotNull(hand.Table.Tournament);
            var _playershand = hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand);
            //Assert.IsTrue(_playershand.Select(x=>x.Player.Nick).Distinct().Count()== 8); //хз почему эта строчка не работает
            Assert.Multiple(() =>
            {
                var players = _playershand.Where(x => x.Position == Position.SmallBlind).Select(x => x.Player);
                Assert.IsTrue(players.Where(x => x.Nick == "radzy777").Distinct().Count() == 1);
                players = _playershand.Where(x => x.Position == Position.BigBlind).Select(x => x.Player);
                Assert.IsTrue(players.Where(x => x.Nick == "anniekipps2").Distinct().Count() == 1);
                players = _playershand.Where(x => x.Position == Position.CutOff).Select(x => x.Player);
                Assert.IsTrue(players.Where(x => x.Nick == "SerGomel").Distinct().Count() == 1);
                players = _playershand.Where(x => x.Position == Position.Button).Select(x => x.Player);
                Assert.IsTrue(players.Where(x => x.Nick == "Natalya-Ber").Distinct().Count() == 1);
                var st = hand.StageHands.First(x => x.Stage == Stages.Ante);
                Assert.IsNotNull(st.Actions.Single(actions =>
                    actions.PlayerHand.Position == Position.SmallBlind &&
                    actions.Action == (Action.Posts | Action.SmallBlind)));
                Assert.IsNotNull(st.Actions.Single(actions =>
                    actions.PlayerHand.Position == Position.BigBlind &&
                    actions.Action == (Action.Posts | Action.BigBlind)));

                Assert.AreEqual(_playershand.First(x => x.Position == Position.Button).Player.Nick, "Natalya-Ber");
                Assert.IsNotNull(hand.Table.Tournament);
                Assert.AreEqual(hand.Table.Tournament.Buy_in, 1.50);
                Assert.AreEqual(hand.Table.TypeOfCurrency, TypeOfCurrency.USD);
                Assert.AreEqual((decimal) 20,
                    hand.StageHands.First(x => x.Stage == Stages.Ante).Actions.First(y =>
                        y.Action == (PokerStat.Model.Action.Posts | PokerStat.Model.Action.BigBlind)).Count);
                Assert.AreEqual((decimal) 10,
                    hand.StageHands.First(x => x.Stage == Stages.Ante).Actions.First(y =>
                        y.Action == (PokerStat.Model.Action.Posts | PokerStat.Model.Action.SmallBlind)).Count);
                Assert.AreEqual(hand.Table.TypeOfCurrency, TypeOfCurrency.USD);
                Assert.IsFalse(_playershand.Any(x => x.Position == 0));
                Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Flop).Cards.Count == 3);
                Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Turn).Cards.Count == 4);
                Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.River).Cards.Count == 5);
                Assert.IsFalse(_playershand.Any(x => x.Player.PokerRoom == null));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard.Count == 2);
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard
                    .Any(x => x.ShortName == 'A' && x.SuitCode == 'd'));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard
                    .Any(x => x.ShortName == 'K' && x.SuitCode == 'c'));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard.Count == 2);
                Assert.IsTrue(
                    hand.StageHands.First(x => x.Stage == Stages.River).Actions
                        .First(x => x.PlayerHand.Player.Nick == "SLAVAMOZG").Action ==
                    (PokerStat.Model.Action.All_In | PokerStat.Model.Action.Posts));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard
                    .Any(x => x.ShortName == 'T' && x.SuitCode == 'h'));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "SLAVAMOZG").HandCard
                    .Any(x => x.ShortName == '9' && x.SuitCode == 'd'));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard.Count == 2);
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard
                    .Any(x => x.ShortName == 'A' && x.SuitCode == 'c'));
                Assert.IsTrue(_playershand.First(x => x.Player.Nick == "hunteroneeye").HandCard
                    .Any(x => x.ShortName == 'T' && x.SuitCode == 's'));
                Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.ShowDown).Actions.Any(x =>
                    x.Action == PokerStat.Model.Action.Collected && x.Count == 997 &&
                    x.PlayerHand.Player.Nick == "hunteroneeye"));
                Assert.IsFalse(hand.StageHands.SelectMany(x => x.Actions)
                    .Any(x => x.Action == PokerStat.Model.Action.NotFound));
            });
        }

        [Test]
        public void TestGetHistoryHandsWithNumberInNick()
        {
            string text2 =
                "Раздача PokerStars №199436273201:  Холдем Безлимитный ($0.01/$0.02 USD) - 18.04.2019 20:47:12 MSK [18.04.2019 13:47:12 ВВ]" +
                Environment.NewLine +
                "Стол 'Taurinensis II' 9-max Баттон на месте №4" + Environment.NewLine +
                "Место 1: 1tillit1 ($2.14 фишек)" + Environment.NewLine +
                "Место 2: Demokhrit ($2.05 фишек)" + Environment.NewLine +
                "Место 3: makkrik ($2.01 фишек)" + Environment.NewLine +
                "Место 4: juNglef1ShH! ($2.90 фишек)" + Environment.NewLine +
                "Место 5: MiltonMartin ($2.03 фишек)" + Environment.NewLine +
                "Место 7: Happy we:-) ($0.80 фишек)" + Environment.NewLine +
                "Место 8: Bootylek ($1.58 фишек)" + Environment.NewLine +
                "Место 9: Region36rus ($2.24 фишек)" + Environment.NewLine +
                "MiltonMartin: ставит малый блайнд $0.01" + Environment.NewLine +
                "Alex23-RnD: пропускает" + Environment.NewLine +
                "Happy we:-): ставит большой блайнд $0.02" + Environment.NewLine +
                "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine +
                "Карты Bootylek [Kc 5h]" + Environment.NewLine +
                "Bootylek: делает фолд" + Environment.NewLine +
                "Region36rus: делает рейз $0.04 $0.06" + Environment.NewLine +
                "1tillit1: делает фолд" + Environment.NewLine +
                "Demokhrit: делает фолд" + Environment.NewLine +
                "makkrik: делает фолд" + Environment.NewLine +
                "juNglef1ShH!: делает фолд" + Environment.NewLine +
                "Alex23-RnD покидает стол" + Environment.NewLine +
                "MiltonMartin: делает фолд" + Environment.NewLine +
                "Happy we:-): делает фолд" + Environment.NewLine +
                "Неуравненная ставка ($0.04) возвращается игроку Region36rus" + Environment.NewLine +
                "Region36rus получил $0.05 ( банк)" + Environment.NewLine +
                "Region36rus: не показывает руку " + Environment.NewLine +
                "*** ИТОГ ***" + Environment.NewLine +
                "Общий банк $0.05 | Доля $0 " + Environment.NewLine +
                "Место 1: 1tillit1 сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 2: Demokhrit сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 3: makkrik сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 4: juNglef1ShH! (баттон) сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 5: MiltonMartin (малый блайнд) сделал фолд до Флоп" + Environment.NewLine +
                "Место 7: Happy we:-) (большой блайнд) сделал фолд до Флоп" + Environment.NewLine +
                "Место 8: Bootylek сделал фолд до Флоп (не ставил)" + Environment.NewLine +
                "Место 9: Region36rus собрал ($0.05)";
            ISerializationHistory serialization =
                new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text2)));
            Assert.IsNotNull(serialization);
            var hands = serialization.GetHands();
            var hand = hands.ToArray()[0];
            Assert.IsTrue(hand.Table.TypeOfGame == TypeOfGame.Cash);
            Assert.IsNull(hand.Table.Tournament);
            var _playershand = hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand);
            //Assert.IsTrue(_playershand.Select(x=>x.Player.Nick).Distinct().Count()== 8); //хз почему эта строчка не работает
            var st = hand.StageHands.First(x => x.Stage == Stages.Ante);
            Assert.IsNotNull(st.Actions.Single(actions =>
                actions.PlayerHand.Position == Position.SmallBlind &&
                actions.Action == (Action.Posts | Action.SmallBlind)));
            Assert.IsNotNull(st.Actions.Single(actions =>
                actions.PlayerHand.Position == Position.BigBlind &&
                actions.Action == (Action.Posts | Action.BigBlind)));
            Assert.Multiple(() =>
            {
                Assert.IsNull(_playershand.FirstOrDefault(x => x.Player.Nick == "Alex23-RnD"));
                Assert.AreEqual(_playershand.First(x => x.Position == Position.Button).Player.Nick, "juNglef1ShH!");
                Assert.AreEqual(_playershand.First(x => x.Position == Position.SmallBlind).Player.Nick, "MiltonMartin");
                Assert.AreEqual(_playershand.First(x => x.Position == Position.BigBlind).Player.Nick, "Happy we:-)");
                Assert.AreEqual(_playershand.First(x => x.Position == Position.CutOff).Player.Nick, "makkrik");
                Assert.AreEqual(hand.Table.TypeOfCurrency, TypeOfCurrency.USD);
                Assert.IsFalse(_playershand.Any(x => x.Position == 0));
                Assert.AreEqual(9, hand.StageHands.First(x => x.Stage == Stages.PreFlop).Actions.Count());
                Assert.AreEqual(2, hand.StageHands.First(x => x.Stage == Stages.Ante).Actions.Count());
                Assert.AreEqual((decimal) 0.05,
                    hand.StageHands.First(x => x.Stage == Stages.PreFlop).Actions
                        .First(y => y.Action == PokerStat.Model.Action.Collected).Count);
                Assert.AreEqual((decimal) 0.02,
                    hand.StageHands.First(x => x.Stage == Stages.Ante).Actions.First(y =>
                        y.Action == (PokerStat.Model.Action.Posts | PokerStat.Model.Action.BigBlind)).Count);
                Assert.IsFalse(hand.StageHands.SelectMany(x => x.Actions)
                    .Any(x => x.Action == PokerStat.Model.Action.NotFound));
            });
        }

        [Test]
        public void TestGetCorrectPosition()
        {
            string text = "Раздача PokerStars №198193910502: Турнир №2562625880, $0.23+$0.02 USD Холдем Безлимитный - уровень I (10/20) - 16.03.2019 17:02:55 MSK [16.03.2019 10:02:55 ВВ]" + Environment.NewLine +
                          "Стол '2562625880 1' 3-max Баттон на месте №3" + Environment.NewLine +
                          "Место 2: Bootylek (400 фишек) " + Environment.NewLine +
                          "Место 3: zeng1111 (1100 фишек) " + Environment.NewLine +
                          "zeng1111: ставит малый блайнд 10" + Environment.NewLine +
                          "Bootylek: ставит большой блайнд 20" + Environment.NewLine +
                          "*** ЗАКРЫТЫЕ КАРТЫ ***" + Environment.NewLine +
                          "Карты Bootylek [Th Qh]" + Environment.NewLine +
                          "zeng1111: делает рейз 40 60" + Environment.NewLine +
                          "Bootylek: делает колл 40" + Environment.NewLine +
                          "*** ФЛОП *** [8s Jh Qs]" + Environment.NewLine +
                          "Bootylek: делает бет 60" + Environment.NewLine +
                          "zeng1111: делает колл 60" + Environment.NewLine +
            "*** ТЕРН *** [8s Jh Qs] [3s]" + Environment.NewLine +
                "Bootylek: делает бет 120" + Environment.NewLine +
                "zeng1111: делает рейз 860 980 и олл-ин" + Environment.NewLine +
                "Bootylek: делает колл 160 и олл-ин" + Environment.NewLine +
                "Неуравненная ставка (700) возвращается игроку zeng1111" + Environment.NewLine +
                "*** РИВЕР *** [8s Jh Qs 3s] [Jc]" + Environment.NewLine +
                "*** ВСКРЫТИЕ КАРТ ***" + Environment.NewLine +
                "Bootylek: открывает [Th Qh] (две пары [дамы и валеты])" + Environment.NewLine +
                "zeng1111: открывает [Kc Ks] (две пары [короли и валеты])" + Environment.NewLine +
                "zeng1111 получил 800 ( банк)" + Environment.NewLine +
                "игрок Bootylek занял в турнире 2 место" + Environment.NewLine +
                "Игрок zeng1111 побеждает в турнире и получает $1.00 - поздравляем!" + Environment.NewLine +
                "*** ИТОГ ***" + Environment.NewLine +
                "Общий банк 800 | Доля 0 " + Environment.NewLine +
                "Борд [8s Jh Qs 3s Jc]" + Environment.NewLine +
                "Место 2: Bootylek (большой блайнд) открыл [Th Qh] и проиграл , собрав две пары [дамы и валеты]" + Environment.NewLine +
                "Место 3: zeng1111 (баттон) (малый блайнд) открыл [Kc Ks] и выиграл (800) , собрав две пары [короли и валеты]";
            ISerializationHistory serialization =
                new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text)));
            Assert.IsNotNull(serialization);
            var hand = serialization.GetHands().Single();
            var _playershand = hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand);
            var st = hand.StageHands.First(x => x.Stage == Stages.Ante);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(hand.Table.TypeOfCurrency, TypeOfCurrency.USD);
                Assert.IsFalse(_playershand.Any(x => x.Position == 0));
                Assert.IsNotNull(st.Actions.Single(actions =>
                    actions.PlayerHand.Position == Position.SmallBlind &&
                    actions.Action == (Action.Posts | Action.SmallBlind)));
                Assert.IsNotNull(st.Actions.Single(actions =>
                    actions.PlayerHand.Position == Position.BigBlind &&
                    actions.Action == (Action.Posts | Action.BigBlind)));
                Assert.IsTrue(hand.Table.TypeOfGame == TypeOfGame.Cash);
            });
            
        }
    }
}