using System;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PokerStat.Model;
using Action = PokerStat.Model.Action;

namespace PokerStat.Serialization.Tests
{
    [TestFixture]
    public class UnitTestPSSerialization
    {

        string text = "PokerStars Hand #198226684287:  Hold'em No Limit ($0.01/$0.02 USD) - 2019/03/17 13:02:58 MSK [2019/03/17 6:02:58 ET]"+Environment.NewLine+"Table 'Bethgea' 6-max Seat #6 is the button"+Environment.NewLine+"Seat 1: kowalowski89 ($5.31 in chips) "+Environment.NewLine+"Seat 2: Payatzo ($2.14 in chips) "+Environment.NewLine+"Seat 3: melblueeyes ($1.25 in chips) "+Environment.NewLine+"Seat 4: goongoon11 ($2.41 in chips) "+Environment.NewLine+"Seat 5: Bootylek ($1.60 in chips) "+Environment.NewLine+"Seat 6: alonesoul853 ($2.27 in chips) "+Environment.NewLine+"kowalowski89: posts small blind $0.01"+Environment.NewLine+"Payatzo: posts big blind $0.02"+Environment.NewLine+"Bootylek: posts big blind $0.02"+Environment.NewLine+"*** HOLE CARDS ***"+Environment.NewLine+"Dealt to Bootylek [Td 2d]"+Environment.NewLine+"melblueeyes: folds "+Environment.NewLine+"goongoon11: raises $0.04 to $0.06"+Environment.NewLine+"Bootylek: folds "+Environment.NewLine+"alonesoul853: folds "+Environment.NewLine+"kowalowski89: folds "+Environment.NewLine+"Payatzo: folds "+Environment.NewLine+"Uncalled bet ($0.04) returned to goongoon11"+Environment.NewLine+"goongoon11 collected $0.07 from pot"+Environment.NewLine+"goongoon11: doesn't show hand "+Environment.NewLine+"*** SUMMARY ***"+Environment.NewLine+"Total pot $0.07 | Rake $0 "+Environment.NewLine+"Seat 1: kowalowski89 (small blind) folded before Flop"+Environment.NewLine+"Seat 2: Payatzo (big blind) folded before Flop"+Environment.NewLine+"Seat 3: melblueeyes folded before Flop (didn't bet)"+Environment.NewLine+"Seat 4: goongoon11 collected ($0.07)"+Environment.NewLine+"Seat 5: Bootylek folded before Flop (didn't bet)"+Environment.NewLine+"Seat 6: alonesoul853 (button) folded before Flop (didn't bet)"+Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+"PokerStars Hand #198226688990:  Hold'em No Limit ($0.01/$0.02 USD) - 2019/03/17 13:03:12 MSK [2019/03/17 6:03:12 ET]"+Environment.NewLine+"Table 'Bethgea' 6-max Seat #1 is the button"+Environment.NewLine+"Seat 1: kowalowski89 ($5.30 in chips) "+Environment.NewLine+"Seat 2: Payatzo ($2.12 in chips) "+Environment.NewLine+"Seat 3: melblueeyes ($1.25 in chips) "+Environment.NewLine+"Seat 4: goongoon11 ($2.46 in chips) "+Environment.NewLine+"Seat 5: Bootylek ($1.58 in chips) "+Environment.NewLine+"Seat 6: alonesoul853 ($2.27 in chips) "+Environment.NewLine+"Payatzo: posts small blind $0.01"+Environment.NewLine+"melblueeyes: posts big blind $0.02"+Environment.NewLine+"*** HOLE CARDS ***"+Environment.NewLine+"Dealt to Bootylek [Ah Jd]"+Environment.NewLine+"goongoon11: folds "+Environment.NewLine+"Bootylek: raises $0.06 to $0.08"+Environment.NewLine+"alonesoul853: folds "+Environment.NewLine+"kowalowski89: folds "+Environment.NewLine+"Payatzo: folds "+Environment.NewLine+"melblueeyes: folds "+Environment.NewLine+"Uncalled bet ($0.06) returned to Bootylek"+Environment.NewLine+"Bootylek collected $0.05 from pot"+Environment.NewLine+"Bootylek: doesn't show hand "+Environment.NewLine+"*** SUMMARY ***"+Environment.NewLine+"Total pot $0.05 | Rake $0 "+Environment.NewLine+"Seat 1: kowalowski89 (button) folded before Flop (didn't bet)"+Environment.NewLine+"Seat 2: Payatzo (small blind) folded before Flop"+Environment.NewLine+"Seat 3: melblueeyes (big blind) folded before Flop"+Environment.NewLine+"Seat 4: goongoon11 folded before Flop (didn't bet)"+Environment.NewLine+"Seat 5: Bootylek collected ($0.05)"+Environment.NewLine+"Seat 6: alonesoul853 folded before Flop (didn't bet)"+Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+
            "PokerStars Hand #198226697235:  Hold'em No Limit ($0.01/$0.02 USD) - 2019/03/17 13:03:36 MSK [2019/03/17 6:03:36 ET]"+Environment.NewLine+"Table 'Bethgea' 6-max Seat #2 is the button"+Environment.NewLine+"Seat 1: kowalowski89 ($5.30 in chips) "+Environment.NewLine+"Seat 2: Payatzo ($2.11 in chips) "+Environment.NewLine+"Seat 3: melblueeyes ($1.23 in chips) "+Environment.NewLine+"Seat 5: Bootylek ($1.61 in chips) "+Environment.NewLine+"Seat 6: alonesoul853 ($2.27 in chips) "+Environment.NewLine+"melblueeyes: posts small blind $0.01"+Environment.NewLine+"goongoon11: is sitting out "+Environment.NewLine+"goongoon11 leaves the table"+Environment.NewLine+"Bootylek: posts big blind $0.02"+Environment.NewLine+"*** HOLE CARDS ***"+Environment.NewLine+"Dealt to Bootylek [Qs 8d]"+Environment.NewLine+"Bom001 joins the table at seat #4 "+Environment.NewLine+"alonesoul853: folds "+Environment.NewLine+"kowalowski89: folds "+Environment.NewLine+"Payatzo: folds "+Environment.NewLine+"melblueeyes: calls $0.01"+Environment.NewLine+"Bootylek: checks "+Environment.NewLine+"*** FLOP *** [6s Td Tc]"+Environment.NewLine+"melblueeyes: checks "+Environment.NewLine+"Bootylek: bets $0.02"+Environment.NewLine+"melblueeyes: calls $0.02"+Environment.NewLine+"*** TURN *** [6s Td Tc] [4d]"+Environment.NewLine+"Bom001 leaves the table"+Environment.NewLine+"melblueeyes: checks "+Environment.NewLine+"Bootylek: bets $0.04"+Environment.NewLine+"melblueeyes: calls $0.04"+Environment.NewLine+"*** RIVER *** [6s Td Tc 4d] [Jh]"+Environment.NewLine+"melblueeyes: checks "+Environment.NewLine+"Bootylek: checks "+Environment.NewLine+"*** SHOW DOWN ***"+Environment.NewLine+"melblueeyes: shows [8h 9h] (a pair of Tens)"+Environment.NewLine+"Bootylek: shows [Qs 8d] (a pair of Tens - Queen kicker)"+Environment.NewLine+"Bootylek collected $0.15 from pot"+Environment.NewLine+"*** SUMMARY ***"+Environment.NewLine+"Total pot $0.16 | Rake $0.01 "+Environment.NewLine+"Board [6s Td Tc 4d Jh]"+Environment.NewLine+"Seat 1: kowalowski89 folded before Flop (didn't bet)"+Environment.NewLine+"Seat 2: Payatzo (button) folded before Flop (didn't bet)"+Environment.NewLine+"Seat 3: melblueeyes (small blind) showed [8h 9h] and lost with a pair of Tens"+Environment.NewLine+"Seat 5: Bootylek (big blind) showed [Qs 8d] and won ($0.15) with a pair of Tens"+Environment.NewLine+"Seat 6: alonesoul853 folded before Flop (didn't bet)" +Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+""+Environment.NewLine+
            "PokerStars Zoom Hand #199534858436:  Hold'em No Limit ($0.01/$0.02) - 2019/04/21 18:01:38 CUST [2019/04/21 8:01:38 ET]"+Environment.NewLine+"Table 'Bethgea' 9-max Seat #1 is the button"+Environment.NewLine+"Seat 1: PaYkaH ($3.40 in chips)"+Environment.NewLine+"Seat 2: vas_ururu ($1.44 in chips)" +Environment.NewLine+"Seat 3: FoterIS ($2 in chips)" +Environment.NewLine+"Seat 4: SellYourBody ($2 in chips)" +Environment.NewLine+"Seat 5: $Andreev8899 ($0.76 in chips)" +Environment.NewLine+"Seat 6: Fess911 ($0.93 in chips)" +Environment.NewLine+"Seat 7: vcf2013 ($3.98 in chips)" +Environment.NewLine+"Seat 8: nazmeew ($3.96 in chips)" +Environment.NewLine+"Seat 9: Kronprinz79 ($2.70 in chips)" +Environment.NewLine+"vas_ururu: posts small blind $0.01"+Environment.NewLine+"FoterIS: posts big blind $0.02"+Environment.NewLine+"*** HOLE CARDS ***"+Environment.NewLine+"Dealt to FoterIS [4s 9c]"+Environment.NewLine+"SellYourBody: folds"+Environment.NewLine+"$Andreev8899: folds" +Environment.NewLine+"Fess911: raises $0.02 to $0.04"+Environment.NewLine+"vcf2013: folds" +Environment.NewLine+"nazmeew: calls $0.04"+Environment.NewLine+"Kronprinz79: folds" +Environment.NewLine+"PaYkaH: calls $0.04"+Environment.NewLine+"vas_ururu: folds" +Environment.NewLine+"FoterIS: folds" +Environment.NewLine+"*** FLOP *** [4h Jd 3h]"+Environment.NewLine+"Fess911: bets $0.06"+Environment.NewLine+"nazmeew: folds" +Environment.NewLine+"PaYkaH: calls $0.06"+Environment.NewLine+"*** TURN *** [4h Jd 3h] [Kc]"+Environment.NewLine+"Fess911: bets $0.10"+Environment.NewLine+"PaYkaH: folds" +Environment.NewLine+"Uncalled bet ($0.10) returned to Fess911"+Environment.NewLine+"Fess911 collected $0.26 from pot"+Environment.NewLine+"Fess911: doesn't show hand" +Environment.NewLine+"*** SUMMARY ***"+Environment.NewLine+"Total pot $0.27 | Rake $0.01" +Environment.NewLine+"Board [4h Jd 3h Kc]"+Environment.NewLine+"Seat 1: PaYkaH (button) folded on the Turn"+Environment.NewLine+"Seat 2: vas_ururu (small blind) folded before Flop"+Environment.NewLine+"Seat 3: FoterIS (big blind) folded before Flop"+Environment.NewLine+"Seat 4: SellYourBody folded before Flop (didn't bet)"+Environment.NewLine+"Seat 5: $Andreev8899 folded before Flop (didn't bet)"+Environment.NewLine+"Seat 6: Fess911 collected ($0.26)"+Environment.NewLine+"Seat 7: vcf2013 folded before Flop (didn't bet)"+Environment.NewLine+"Seat 8: nazmeew folded on the Flop"+Environment.NewLine+"Seat 9: Kronprinz79 folded before Flop (didn't bet)"+Environment.NewLine;
        [Test]
        public void TestGetHands()
        {
            ISerializationHistory serialization = new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text)));
            Assert.IsNotNull(serialization);
            var hands = serialization.GetHands();
            Assert.IsTrue(hands.Count(x => x.Table.Identifier == "Bethgea") == 4);
            Assert.IsTrue(hands.Count(x => x.Table.PokerRoom.Name == "PokerStars") == 4);
            Assert.IsTrue(hands.ToArray()[0].Date == new DateTime(2019, 03, 17, 6, 02, 58));
            Assert.IsTrue(hands.Count(x => x.Table.Tournament == null) == 4);
        }
        [Test]
        public void TestGetHistoryHands()
        {
            ISerializationHistory serialization = new FactorySerialization().GetSerializator(new MemoryStream(Encoding.Default.GetBytes(text)));
            Assert.IsNotNull(serialization);
            var hands = serialization.GetHands();
            var hand = hands.ToArray()[2];
            Assert.IsTrue(hand.Table.TypeOfGame == TypeOfGame.Cash);
            Assert.IsTrue(hands.ToArray()[3].Table.TypeOfGame == TypeOfGame.Zoom);
           // Assert.IsTrue(serialization.FillHand(ref hand));
            var _playershand = hand.StageHands.SelectMany(x => x.Actions).Select(x => x.PlayerHand);
            Assert.IsTrue(_playershand.Select(x=>x.Player.Nick).Distinct().Count()== 5);
            var players = _playershand.Where(x => x.Position == Position.SmallBlind).Select(x=>x.Player);
            var st = hand.StageHands.First(x => x.Stage == Stages.Ante);
            Assert.Multiple(() =>
         {
             
             Assert.IsNull(hand.Table.Tournament);
             Assert.AreEqual(hand.Table.TypeOfCurrency,TypeOfCurrency.USD);
             Assert.IsTrue(players.Select(x => x.Nick).Distinct().Count() == 1);
             Assert.IsTrue(players.Where(x => x.Nick == "melblueeyes").Distinct().Count() == 1);
             Assert.IsFalse(_playershand.Any(x => x.Position == 0));
             Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Flop).Cards.Count == 3);
             Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.Turn).Cards.Count == 4);
             Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.River).Cards.Count == 5);
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard.Count == 2);
             Assert.IsNotNull(st.Actions.Single(actions=>actions.PlayerHand.Position == Position.SmallBlind && actions.Action == (Action.Posts|Action.SmallBlind)));
             Assert.IsNotNull(st.Actions.Single(actions=>actions.PlayerHand.Position == Position.BigBlind && actions.Action == (Action.Posts|Action.BigBlind)));
                //   actions.PlayerHand.Position == Position.SmallBlind && actions.Action != PokerStat.Model.Action.PostsSmallBlind
                Assert.IsFalse(_playershand.Any(x => x.Player.PokerRoom == null));
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard.Any(x => x.ShortName == 'Q' && x.SuitCode == 's'));
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "Bootylek").HandCard.Any(x => x.ShortName == '8' && x.SuitCode == 'd'));
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "melblueeyes").HandCard.Count == 2);
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "melblueeyes").HandCard.Any(x => x.ShortName == '8' && x.SuitCode == 'h'));
             Assert.IsTrue(_playershand.First(x => x.Player.Nick == "melblueeyes").HandCard.Any(x => x.ShortName == '9' && x.SuitCode == 'h'));
             Assert.IsTrue(hand.StageHands.SelectMany(x => x.Actions).Count() == 16);
             Assert.IsTrue(hand.StageHands.First(x => x.Stage == Stages.ShowDown).Actions.Any(x => x.Action == PokerStat.Model.Action.Collected && x.Count == (decimal)0.15 && x.PlayerHand.Player.Nick == "Bootylek"));
             Assert.IsFalse(hand.StageHands.SelectMany(x => x.Actions).Any(x => x.Action == PokerStat.Model.Action.NotFound));
         });
        }
    }

}
