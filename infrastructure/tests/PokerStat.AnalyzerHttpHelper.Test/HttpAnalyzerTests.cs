using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.AnalyzerHttpHelper.Validators;
using PokerStat.Exceptions.ErrorCodes;
using PokerStat.HttpContracts;
using Tests.Fake;

namespace Tests
{
    [TestFixture]
    public class HttpAnalyzerTests
    {
        private IAnalyzerController _analyzerController;
       [SetUp]
        public void Setup()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerRequestValidator)))
                .Returns(new AnalyzerRequestValidator());
            _analyzerController = new FakeController(stub_vf.Object);
        }
        [TestCase("fss", "1", "1", "1")] // неверный  guid
        [TestCase("00000000-0000-0000-0000-000000000000", "1", "1", "1")]// неверный  guid
        [TestCase("", "1", "1", "1")]// неверный  guid
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "sfdf", "1", "1")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "2", "1", "1")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "", "1", "1")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "fsfd", "1")]// неверный  typeofgame
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "", "1")]// неверный  typeofgame
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "")]// неверный  pokerroom
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "hfhf")]//неверный  pokerroom
        [TestCase("fss", "dsd", "sd", "ds")]// все неверно
        public async Task Should_Return_ErrorValidation(string playerid, string istournament, string typeofgame, string pokerrrom)
        {
            var res = await _analyzerController.GetAsync(new AnalyzerRequest
            {
                Player_id = playerid, 
                IsTournamnet = istournament,
                TypeOfGame = typeofgame,
                PokerRoom = pokerrrom
            });
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<BadRequestObjectResult>(res);
                var bad = (BadRequestObjectResult) res;
                Assert.IsInstanceOf<Response>(bad.Value);
                var response = (Response) bad.Value;
                Assert.IsTrue(response.Errors.Any());
                Assert.AreEqual(PokerStatErrorCodes.Validation,response.Errors[0].Code);
            });
        }

        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "0", "1", "1")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "0", "1")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "0", "1", "0")]
        public async Task Should_Return_5(string playerid, string istournament, string typeofgame, string pokerrrom)
        {
            var res = await _analyzerController.GetAsync(new AnalyzerRequest
            {
                Player_id = playerid, 
                IsTournamnet = istournament,
                TypeOfGame = typeofgame,
                PokerRoom = pokerrrom
            });
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult) res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse) ok.Value;
                Assert.AreEqual(5, response.Data);
            });

        }
    }
}