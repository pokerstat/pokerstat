using System.Threading.Tasks;
using FluentValidation;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Controllers;

namespace Tests.Fake
{
    public class FakeController:AnalyzerController
    {
        public FakeController(IValidatorFactory validatorFactory) : base(validatorFactory)
        {
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerRequest request)
        {
            return await Task.FromResult(5);
        }

        protected override async Task<decimal> InvokeAsync(AnalyzerWithDateRequest request)
        {
            return await Task.FromResult(6);
        }
    }
}