using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PokerStat.AnalyzerHttpHelper.Contracts;
using PokerStat.AnalyzerHttpHelper.Interfaces;
using PokerStat.AnalyzerHttpHelper.Validators;
using PokerStat.Exceptions.ErrorCodes;
using PokerStat.HttpContracts;
using Tests.Fake;

namespace Tests
{
    [TestFixture]
    public class HttpAnalyzerWithDateTest
    {
        private IAnalyzerController _analyzerController;
       [SetUp]
        public void Setup()
        {
            Mock<IValidatorFactory> stub_vf = new Mock<IValidatorFactory>();
            stub_vf.Setup(vf => vf.GetValidator(typeof(AnalyzerWithDateRequestValidator)))
                .Returns(new AnalyzerWithDateRequestValidator());
            _analyzerController = new FakeController(stub_vf.Object);
        }

        [TestCase("fss", "1", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")] // неверный  guid
        [TestCase("00000000-0000-0000-0000-000000000000", "1", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  guid
        [TestCase("", "1", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  guid
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "sfdf", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "2", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  istournament
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "fsfd", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  typeofgame
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "-1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  typeofgame
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  typeofgame
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "","2000-04-18 00:00:00","2000-04-18 00:00:00")]// неверный  pokerroom
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "hfhf","2000-04-18 00:00:00","2000-04-18 00:00:00")]//неверный  pokerroom
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "-2","2000-04-18 00:00:00","2000-04-18 00:00:00")]//неверный  pokerroom
        
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1","","2000-04-18 00:00:00")]// неверный  datestart
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1","dsdsd","2000-04-18 00:00:00")]// неверный  datestart

        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1", "2000-04-18 00:00:00", "")]// неверный  dateend
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1", "2000-04-18 00:00:00", "vxvvcv")]// неверный  dateend


        [TestCase("fss", "dsd", "sd", "ds","2000-04-1","2000-00")]// все неверно
        public async Task Should_Return_ErrorValidation(string playerid, string istournament, string typeofgame, string pokerrrom, string datestart, string dateend)
        {
            var res = await _analyzerController.GetWithDateAsync(new AnalyzerWithDateRequest
            {
                Player_id = playerid, 
                IsTournamnet = istournament,
                TypeOfGame = typeofgame,
                PokerRoom = pokerrrom,
                DateStart = datestart,
                DateEnd = dateend
            });
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<BadRequestObjectResult>(res);
                var bad = (BadRequestObjectResult) res;
                Assert.IsInstanceOf<Response>(bad.Value);
                var response = (Response) bad.Value;
                Assert.IsTrue(response.Errors.Any());
                Assert.AreEqual(PokerStatErrorCodes.Validation,response.Errors[0].Code);
            });
        }
        
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "0", "1", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "0", "1","2000-04-18 00:00:00","2000-04-18 00:00:00")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "1", "0","2000-04-18 00:00:00","2000-04-18 00:00:00")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "0", "0", "1","2000-04-18","2000-04-18 00:00:00")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "1", "0", "0","2000-04-18 00:00:00","2000-04-18")]
        [TestCase("936DA01F-9ABD-4d9d-80C7-02AF85C822A8", "0", "1", "0","2000-04-18","2000-04-18")]
        public async Task Should_Return_6(string playerid, string istournament, string typeofgame, string pokerrrom, string datestart, string dateend)
        {
            var res = await _analyzerController.GetWithDateAsync(new AnalyzerWithDateRequest
            {
                Player_id = playerid, 
                IsTournamnet = istournament,
                TypeOfGame = typeofgame,
                PokerRoom = pokerrrom,
                DateStart = datestart,
                DateEnd = dateend
            });
            Assert.Multiple(() =>
            {
                Assert.IsInstanceOf<OkObjectResult>(res);
                var ok = (OkObjectResult) res;
                Assert.IsInstanceOf<AnalyzerResponse>(ok.Value);
                var response = (AnalyzerResponse) ok.Value;
                Assert.AreEqual(6, response.Data);
            });

        }
    }
}