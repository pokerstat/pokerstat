using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PokerStat.Exceptions.ErrorCodes;
using PokerStat.Exceptions.Extensions;

namespace PokerStat.Tests.Exceptions
{
    [TestFixture]
    public class PSCodesExtensionsTests
    {
        [TestCase(PokerStatErrorCodes.Common, LogLevel.Information)]
        public void Should_ReturnDefaultLogLevel_When_LogLevelWasSetting(PokerStatErrorCodes pokerStatErrorCode, LogLevel excepted)
        {
            Assert.AreEqual(excepted, pokerStatErrorCode.GetDefaultLogLevel());
        }
        
        [TestCase(PokerStatErrorCodes.Common, "Common error")]
        public void Should_ReturnDescription_When_DescriptionWasSetting(PokerStatErrorCodes pokerStatErrorCode, string excepted)
        {
            Assert.AreEqual(excepted, pokerStatErrorCode.GetDescription());
        }
    }
}