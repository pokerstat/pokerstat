using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PokerStat.Exceptions;
using PokerStat.Exceptions.ErrorCodes;

namespace PokerStat.Tests.Exceptions
{
    [TestFixture]
    public class PSErrorTests
    {
        [Test]
        public void Should_UseLogLevelFromCode_When_logLevelWasNotInit()
        {
            var psError = new PokerStatError(PokerStatErrorCodes.Common);

            Assert.AreEqual(LogLevel.Information, psError.LogLevel);
        }

        [Test]
        public void Should_UseLogLevelFromConstructor_When_logLevelWasInit()
        {
            var psError = new PokerStatError(PokerStatErrorCodes.Common, LogLevel.Critical);

            Assert.AreEqual(LogLevel.Critical, psError.LogLevel);
        }
    }
}